.. Pumpkin Sphinx Documentation documentation master file, created by
   sphinx-quickstart on Fri Nov  8 09:53:43 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Pumpkin SupMCU Firmware Reference Manual
===================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   pages/module-overview
   pages/scpi-format
   pages/tlm-info
   pages/cal-info
   pages/tlm-cal-format
   pages/supmcu
   pages/aim
   pages/bim
   pages/bm
   pages/bsm
   pages/dasa
   pages/epsm
   pages/dcps
   pages/gpsrm
   pages/leps
   pages/pdb
   pages/pim
   pages/rhm
   pages/sim
   pages/indi


The SupMCU Firmware Reference Manual covers:

* SCPI command usage of SUPERNOVA modules
* Telemetry and calibration binary/ascii formats
* Parsing of Telemetry/Calibration data
* SCPI command definitions for every module
* Telemetry definitions for every module
* Calibration definitions for every module

Revisions
=========

.. highlight:: none

**Version v5.4** ::

   * Fixed BM2 telemetry length values

**Version v5.3.2** ::

   * OEM615 references removed
   * Fixed wording of GPSRM sections
   * Fix title for document
   * Add missing changelog items

**Version v5.3.1** ::

   * Added PDB/LEPS Section
   * Minor corrections to grammar/wording

**Version v5.2.3** ::

   * Changed `External Temp Sensor #`` -> ``Cell Temperature #``

**Version v5.2.2** ::

   * Added in new section for temperature telemetry for the BM2
   * Deprecated TS# Telemetry items for the BM2

**Version v5.2.1** ::

   * Updated EPSM Command reference for batteries, OT trip points and NVM values.

**Version v5.2** ::

   * Reordered changelog to be in decsending order
   * Add in BM2 Command and telemetry section

**Version v5.1** ::

   * Separated DCPS from EPSM section
   * Updated to include new telemetry items on EPSM
   * Deprecated several commands and telemetry items no longer supported or superseded by other commands/telemetry
   * Updated several module telemetry items to reflect new data formats
   * Updated several module commands to reflect new syntax and features
   * Added in register map for EPSM FPGA read/write commands

**Version v5.0** ::

   * Initial web release version created from PDF document
   * Updated to include initial EPSM Command and Telemetry set
   * Added in Calibration values


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. Include substitutions for various modules.

.. include:: ./pages/substitutions.rst
