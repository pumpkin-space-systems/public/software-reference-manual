# SupMCU Firmware Reference Manual

Documentation source for SCPI commands, calibration settings, NVM memory and telemetry for each SupMCU module.