************************
SCPI Commands
************************

.. highlight:: none

The |GPSRM| Command set is responsible for:

* Controlling power to the OEM719.
* Controlling the RESET line to the OEM719.
* Propagating orbit via Vinti7/SGP4 orbit propagators.
* Setting UART communication channel for the OEM719.
* Enabling or disabling UART communication for the OEM719.
* Enabling a subset of the logs available for the OEM719.
* Controlling power to the ADCS.

.. _gps-power:

GPS:POWer
=======================================

Controls the electrical power of the OEM719 unit.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    GPS:POWer <OFF | ON>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turns ON or OFF power to the OEM719 module.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn on power to the OEM719::

    GPS:POW ON

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

None

.. _gps-reset:

GPS:RESet
=======================================

Controls the state of the RESET signal to the OEM719 unit.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    GPS:RESet <OFF | ON>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turns ON or OFF the RESET signal to the OEM719 module.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn on the GPS reset signal::

    GPS:RES ON

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The reset signal is not turned off automatically, the user will need to turn it off with a corresponding ``GPS:RESET OFF`` command.

.. _gps-propagate:

GPS:PROPagate
=======================================

Propagates the given orbital parameters using Vinti7 or SGP orbit propagators.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    GPS:PROPagate <VINTI7>, <t0 in seconds>,<t1 in seconds>, <Rx>, <Ry>, <Rz>, <Vx>, <Vy>, <Vz>
    or
    GPS:PROPagate <SGP4>, <dt in minutes>,<TLE>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Propagate orbit using SGP4 (S) or Vinti7 (V) orbit propagators.
The results of the orbit propagators can be seen via :ref:`GPS:TEL? 2. <tlm-gps-orbit-propagator>`

``Vinti7`` fields are:

* ``t0`` - An initial time in seconds (usually 0).
* ``t1`` - Time of solution in seconds (time since t0).
* ``Rx, Ry, Rz`` - Position vector in kilometers.
* ``Vx, Vy, Vz`` - Velocity vector in kilometers.

While ``SGP4`` works using:

* ``dt`` - Time delta from time in TLE in minutes.
* ``TLE`` - Two-line element exactly 140 characters long. The lines in a TLE are separated by space instead of newline.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Propagate a spacecraft 120 minutes from now with a  positional vector of (-18,982 Km, 25,047 Km, -173 Km) and velocity vector (2.96 Km/s, 0.33 Km/s 0.27 Km/s)::

    GPS:PROP VINTI7, 0, 7200, -18982.0, -25047.0, -173.0, 2.96, 0.33, 0.27

Propagate TELSTAR 19V orbit 30 minutes from TLE time with SGP4::

    GPS:PROP SGP4,30,TELSTAR 19V 1 43562U 18059A   18228.55107419 -.00000304  00000-0  00000+0 0  9993 2 43562   0.0593 269.5551 0001312 309.6754 241.0290  1.00272513   473

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A TLE’s spacing is critical as the code parsing it expects elements to be at exact character locations.

TLEs for current space vehicles can be found at https://www.celestrak.com/NORAD/elements/

.. _gps-led:

GPS:LED
=======================================

Controls the state of the Status LED on the SupMCU module to match the state of the GPS.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    GPS:LED <SUP | PASS | GPS>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Controls the state of the SupMCU status light to match:

* ``SUP`` - Follows the state of the LED as given by the SUP:LED command
* ``PASS`` - The LED will remain ON if GPS passthrough is on, otherwise OFF.
* ``GPS`` - The LED will remain ON if the GPS is ON otherwise OFF.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Make the status light follow the power state of the OEM719::

    GPS:LED GPS

Make the status light follow the pass through state of the OEM719::

    GPS:LED PASS

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

None.

.. _gps-passthrough:

GPS:PASSthrough
=======================================

Controls the state of the UART passthrough

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    GPS:PASSthrough <OFF | ON>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Controls the UART passthrough channel to the GPS receiver.
For UART configuration, see the ASSY REV on the GPSRM data sheet for the particular module.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn OFF UART Passthrough::

    GPS:PASS OFF

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

None.

.. _gps-log:

GPS:LOG
=======================================

Commands the OEM719 to transmit logs via COM1/COM2 port on NovaTel unit.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    GPS:LOG  <OFF | GGA | GSV | GSA | RMC | VTG | ZDA | BESTPOSA | BESTPOSB | BESTXYZA | BESTXYZB>,[COMn]

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Commands the OEM719 to log a subset of the available data from the receiver.
An optional COMn (where n is 1 or 2) parameter can be given to have the GPS log to COM1 or COM2.
If not present, COM1 is chosen.
The log choices are:

* ``OFF/NONE`` - Turns off all logging from the OEM719 via NovaTel UNLOGALL command.
* ``GGA/GSV/GSA/RMC/VTG`` - Logs OEM719 data via “LOG COMn GP<type> ONTIME 1” NovaTel OEM command string (where type is log type given).
* ``ZDA/BESTPOSA/BESTPOSB/BESTXYZA/BESTXYZB`` - Logs OEM719 data via “LOG COMn <type> ONTIME 1” NovaTel OEM command string (where type is log type given).


Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn on logging BESTXYZA logs to COM2 (including powering GPS, enabling PASSthrough)::

    GPS:POW ON
    GPS:PASS ON
    GPS:LOG BESTXYZA,COM2

Turn on logging GPGGA string to COM1::

    GPS:LOG GGA


Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

More logs/command are available via the UART passthrough and commanding the OEM unit directly.
For information about OEM commands see `here <https://docs.novatel.com/OEM7/Content/PDFs/OEM7_Commands_Logs_Manual.pdf>`_

.. _gps-telemetry:

GPS:TELemetry?
=======================================

Requests a given telemetry item specific to the |GPSRM| unit.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    GPS:TELemetry? <N>,[DATA | ASCII | NAME | LENGTH]

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Request telemetry for a specific field N in the given format.
The formats are:

* ``DATA`` - Return telemetry in binary representation. Not available over user debug terminal.
* ``ASCII`` - Return telemetry in ASCII representation.
* ``NAME`` - The name of the telemetry item e.g. ``SupMCU Uptime``
* ``LENGTH`` - Length (in bytes) of telemetry in its binary representation.


Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Query the GPS Power status in binary representation::

    GPS:TEL? 0,DATA

Query the name of the 2nd Telemetry item::

    GPS:TEL? 1,NAME

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If no format representation is specified, ``DATA`` is used as the default.

See GPSRM Telemetry section for more detail.

.. _gps-calibration-query:

GPS:CALibration?
=======================================

Queries |GPSRM| specific calibration values.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    GPSRM:CALibration? LINEAR,<idx>,[ASCII | NAME | DATA]

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Request calibration values for a specific calibration index `idx` in a given format.
The formats are:

* ``DATA`` - Return calibration values in binary format. Not available over user debug terminal.
* ``ASCII`` - Return the calibration values in ASCII representation.
* ``NAME`` - The name of the calibration index. e.g. CTMU

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Query name of idx 0 calibration value::

    GPSRM:CAL? LINEAR, 0, NAME

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``LINEAR`` argument is required.
This is for forward compatibility with future calibration value types (e.g. quadratic).

.. _gps-calibration:

GPS:CALibration
=======================================

Sets |GPSRM| specific calibration values.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    GPSRM:CALibration LINEAR,<idx>,<OFFSET | SCALE_FACTOR>,<value>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Set calibration ``OFFSET`` or ``SCALE_FACTOR`` values for a specific calibration ``idx`` in the Supervisor MCU calibration values.
The calibration values are used internally when calculating the telemetry values for the user, applying a linear correction factor (E.g. ``final = scale_factor*initial + offset``).
Possible value sets are:

* ``OFFSET`` - Set the offset of the calibration factor. ``<value>`` is a number in the range [-32768, 32767]
* ``SCALE_FACTOR`` - Set the scale factor of the calibration factor. ``<value>`` is a floating-point number from [-16.0, 16.0].

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Set the SCALE_FACTOR to .975 and offset to -20 for the `VCC_SYS` calibration value, save to NVM::

    GPS:CAL LINEAR,0,SCALE_FACTOR,0.975
    GPS:CAL LINEAR,0,OFFSET,-20
    SUP:NVM UNLOCK,12345
    SUP:NVM WRITE,1

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``LINEAR`` argument is required.
This is for forward compatibility with future calibration value types (e.g. quadratic).
These values directly impact the output of telemetry from the SupMCU modules.
Calibration values must be saved via :ref:`supervisor-nvm` commands.

See GPSRM Calibration for more details.

.. _gps-adcs-power:

GPS:ADCS:POWer
=======================================

Controls power to the ADCS unit.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    GPS:ADCS:POWer <ON | OFF>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turns the ADCS power ON or OFF.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn the ADCS ON::

    GPS:ADCS:POW ON

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

None.

Deprecated commands
===================

Below is a listing of commands to be removed in a future revision of the firmware:

.. csv-table:: Deprecated Commands
    :header: "Name", "Reason"
    :widths: 30, 70

    "GPS:BESTPOS", "Use ``GPS:LOG`` command instead."

.. highlight:: python
