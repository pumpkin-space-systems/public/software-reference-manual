************************
SCPI Commands
************************

.. highlight:: none

The SIM Command set is responsible for:

* Firing each of the four panel release modules (PRM’s).

.. _sim-prm:

SIM:PRM
===============================

Controls the state of the Panel release modules on the |SIM|.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    SIM:PRM <number>,<ENABle | DISable | ARM | UNARm | FIRE, <burn_time>>

Description
^^^^^^^^^^^^^^^^^^^^^^^^

Controls the state of the given PRM number where number is in the range [1,4].
To burn a PRM, the sequence of commands is ENABLE → ARM → FIRE.
The different parameter strings are:

* ``ENABle`` - Enables the given PRM number, first stage in burning a PRM.
* ``DISable`` - Disables a given PRM number, effectively restarting the firing sequence.
* ``ARM`` - Arms the given PRM number, second stage in burning a PRM.
* ``UNARm`` - Disarms a given PRM number, this does not disable a PRM, it can be rearmed with a ``SIM:PRM <number>, ARM`` command.
* ``FIRE`` - Starts the OS task to burn the PRM for the given ``<burn_time>`` in seconds where ``<burn_time>`` is a value between [1, 1800].

Examples
^^^^^^^^^^^^^^^^^^^^^^^^

Burn PRM 1 & 2: for 30 seconds::

    SIM:PRM 1,ENAB
    SIM:PRM 1,ARM
    SIM:PRM 1,FIRE,30
    Wait 30 seconds...
    SIM:PRM 2,ENAB
    SIM:PRM 2.ARM
    SIM:PRM 2,FIRE,30

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^

Any ``SIM:PRM`` command done out of order (e.g. ``SIM:PRM 1,ENAB`` → ``SIM:PRM 1,FIRE,30``) will reset the PRM back to DISABLED state.
The sequence must restart with SIM:PRM <number>,ENABle.

.. highlight:: python