************************
SCPI Commands
************************

.. highlight:: none

The PDB Command set is responsible for:

* Turning on / off power ports.
* Controlling NVM values.
* Enabling / disabling debug information.
* Requesting telemetry.

.. _PDB-debug:

PDB:DEBug
===============================

Enables or disables debug information specific to the PDB output via the Debug UART port.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    PDB:DEBug <ENable | DISable>,<mask>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^


Enables or disables debug information being printed out over the UART Debug port on the PDB.
Mask is the bits of the debug information to enable.
Possible value combinations are:

* ``ENable, <mask>`` - Enables the bits represented by the mask value. Mask is a bit field and bits that are 1 will enable debug information.
* ``DISable, <mask>`` - Disables the bits represented by the mask value. Mask is a bit field and bits that are 1 will disable debug information.

There are 4 bits used for debug information on the PDB:

* ``0x0001`` - Toggles ports 1-3 on and off every 5 seconds.
* ``0x0002`` - Show raw ADC readings over debug terminal.
* ``0x0004`` - Show hardware overcurrent trip tick calculations.
* ``0x0008`` - Show CTMU Temperatures.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^

Enable all debug information on PDB, except toggling ports::

    PDB:DEB EN,0x000E

Disable debug information on ADC readings::

    PDB:DEB DIS,0x0002

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^

Debug flags are disabled

.. _PDB-nvm:

PDB:NVM
===============================

Controls NVM values specific to the |PDB| module.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    PDB:NVM UNLOCK,<key>
    PDB:NVM WRITE,1
    PDB:NVM SHUNT <port>,<value>
    PDB:NVM I_LIMIT <port>,<mA>
    PDB:NVM DEBUG,<mask>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^

Controls access to and writes parameters to the non-volatile memory.
Order of operations when changing NVM is: UNLOCK, <change value>, WRITE.
Possible value combinations are:

* ``UNLOCK, <key>`` - unlocks the non-volatile memory (must be done first). key is a value consisting of: ``device serial number + 12345``
* ``WRITE, <value>`` - Writes all set NVM values to non-volatile memory, value = 1.
* ``DEBUG, <mask>`` - Sets the debug mask to be set on boot. The current debug mask is also set to the mask value passed. See :ref:`PDB-debug` command to see list of masks.
* ``SHUNT <port>, <value>`` - Sets the shunt on <port> to <value>.
* ``I_LIMIT <port>, <mA>`` - Sets the current limit on <port> to <mA> milliamps.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^

Set current limit for port 1 to 2000 mA::

    PDB:NVM UNLOCK,12345
    PDB:NVM I_LIMIT 1,2000
    PDB:NVM WRITE,1

Set debug mask to show ADC readings upon boot and now::

    PDB:NVM UNLOCK,12345
    PDB:NVM DEBUG,0x0020
    PDB:NVM WRITE,1

.. _PDB-port:

PDB:PORT:POWer
===============================

Turns power to a port on or off. Port 1 is the XLINK, port 2 is the thruster, and port 3 is the payload.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    PDB:PORT:POW [XLINK | THRUSTER | PAYLOAD],[ON|OFF]

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^

Turns specified port on or off.


Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn XLINK port on::

    PDB:PORT:POW XLINK, ON

Turns thruster port off::

    PDB:PORT:POW THRUSTER, OFF


.. _PDB-telemetry:

PDB:TELemetry?
===============================

Requests a given telemetry item specific to the |PDB| unit.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    PDB:TELemetry? <N>,[DATA | ASCII | NAME | LENGTH]

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^

Request telemetry for a specific field N in the given format.
The formats are:

* ``DATA`` - Return telemetry in binary representation. Not available over user debug terminal.
* ``ASCII`` - Return telemetry in ASCII representation.
* ``NAME`` - The name of the telemetry item e.g. ``SupMCU Uptime``
* ``LENGTH`` - Length (in bytes) of telemetry in its binary representation.


Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^

Query PDB port currents in ASCII::

    PDB:TEL? 0,ASCII

Query PDB combined telemetry in binary representation::

    PDB:TEL? 6,DATA

Query the name of the 2nd PDB telemetry item::

    PDB:TEL? 1,NAME

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^

If no format representation is specified, ``DATA`` is used as the default.

See PDB Telemetry section for more detail.
