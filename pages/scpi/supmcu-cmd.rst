********************************
SCPI Commands
********************************

.. highlight:: none

These commands apply to every module in the Pumpkin |SUPERNOVA| ecosystem.
These commands include:

* Resetting the Supervisor Microcontroller (SupMCU)
* Controlling the SupMCU indicator LED function
* Controlling SupMCU Self-test functions
* Turning on or off the SupMCU clock output (output only available on select modules).
* Manipulating SupMCU clock oscillator speed.
* Querying SupMCU Telemetry


.. _supervisor-reset:

SUPervisor:RESet
===============================

Resets the SupMCU or reset the SupMCU’s error flag.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    SUPervisor:RESet <NOW | ERRor>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Resets the SupMCU or reset the SupMCU’s error flag. Choices are:

* NOW: Reset the SupMCU module.
* ERRor: Clears any error on the SupMCU, also causing the status to return to previous state before the erroneous command.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Reset SupMCU Module::

    SUP:RES NOW

Reset the error status flag::

    SUP:RES ERR

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

None.

.. _supervisor-clock:

SUPervisor:CLOCk
===============================

Controls the state of the |SupMCU| reference clock output.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    SUPervisor:CLOCk <OFF | ON, <N>>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Sets the SupMCU’s reference clock output off, or on with a divider applied.
The divider (N) is a power of 2, with a range of ``[0, 15]``, and is only specified when turning the clock output on, e.g., ``SUP:CLOC ON,4``

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn on reference clock output with base clock divided by 2:
SUP:CLOCK ON,1

Turn off reference clock output:
SUP:CLOCK OFF

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This command only controls the output of the reference clock used with the MCU.

.. _supervisor-selftest:

SUPervisor:SELFtest
===============================

Controls the |SupMCU| self-test code used to check the functional behavior of the MCU ALU/CPU/Cache etc...

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    SUPervisor:SELFtest <STOP | START | REStart | ENABLE,[N1,[N2,...[N6]]] | DISABLE,[N1,[N2,...[N6]]]>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Controls SupMCU self tests, choices are listed below:

* ``START`` - Starts or resumes the enabled self-tests.
* ``STOP`` - Stops self-tests
* ``REStart`` - Restarts self-tests.
* ``ENABLE`` - Enables the given test numbers N1-N6.
* ``DISABLE`` - Disables the given test numbers N1-N6

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Enable Self-tests 1,3,5::

    SUP:SELF ENABLE,1,3,5

Disable Self-tests 1,5::

    SUP:SELF DISABLE,1,5

Start Self-test task::

    SUP:SELF START

Self-test typical output to debug console of a test that has succeeded over 2 million times, over the course of 12 days.

.. code-block::

    0)12:09:02:54.13 task_supmcu_selftest: 2128291 passed(#1, 0x694D in 147 cycles), 0 failed(#0, 0x0000).
    0)12:09:02:54.62 task_supmcu_selftest: 2128292 passed(#2, 0x7B77 in 173 cycles), 0 failed(#0, 0x0000).
    0)12:09:02:55.10 task_supmcu_selftest: 2128293 passed(#3, 0xC2AB in 215 cycles), 0 failed(#0, 0x0000).
    0)12:09:02:55.59 task_supmcu_selftest: 2128294 passed(#4, 0x00F8 in 147 cycles), 0 failed(#0, 0x0000).
    0)12:09:02:56.07 task_supmcu_selftest: 2128295 passed(#5, 0x1BD2 in 167 cycles), 0 failed(#0, 0x0000).
    0)12:09:02:56.56 task_supmcu_selftest: 2128296 passed(#6, 0xAE40 in 187 cycles), 0 failed(#0, 0x0000).

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

As of document writing, it has been observed that tests may report false errors.
Their results may not be accurate.

Tests 1-6 are *enabled* by default.

.. _supervisor-i2c-reset:

SUPervisor:I2C:RESet
===============================

Resets the I2C Bus.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    SUPervisor:I2C:RESet <NOW>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Resets the I2C bus, useful if problems are encountered with I2C communications.
``NOW`` is the only accepted argument.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Resetting I2C task::

    SUP:I2C:RES NOW

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

None.

.. _supervisor-debug:

SUPervisor:DEBug
===============================

Enables / disables debug messages via the user debug terminal.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    SUPervisor:DEBug <ENABle | DISABle>, <debug bitfield>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Enables / disables debug messages via the user debug terminal.
``<debug bitfield>`` is a 2-byte hexadecimal number (e.g. ``0xFFFF``)

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Enable all debug messages::

    SUP:DEB ENAB,0xFFFF

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Messages are not tramsitted via I2C and are not useful when communicating via I2C.

.. _supervisor-telemetry:

SUPervisor:TELemetry?
===============================

Request telemetry from the |SupMCU| set of telemetry items present to all modules.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    SUPervisor:TELemetry? <N>,[DATA | ASCII | NAME | LENGTH]

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Request telemetry for a specific field N in the given format.
The formats are:

* DATA: Return telemetry in binary representation. Not available over user debug terminal.
* ASCII: Return telemetry in ASCII representation.
* NAME: The name of the telemetry item e.g. ``SupMCU Uptime``
* LENGTH: Length (in bytes) of telemetry in its binary representation.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Query SupMCU for its Firmware version in ASCII::

    SUP:TEL? 0,ASCII

Query SupMCU for the name of the 3rd telemetry item::

    SUP:TEL? 2,NAME


Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If no format representation is specified, DATA is used as the default.
See SupMCU Telemetry section for more detail.

.. _supervisor-led:

SUPervisor:LED
===============================

Controls status LED behavior.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    SUPervisor:LED <OFF | ON | FLASH | I2C>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Controls status LED behavior.
The states are:

* ``OFF`` - The LED is turned off
* ``ON`` - The LED stays illuminated.
* ``FLASH`` - The default state of the LED, flashing at 2Hz
* ``I2C`` - LED illuminates on I2C activity for the SupMCU module.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn off the status LED::

    SUP:LED OFF

The status LED follows I2C activity::

    SUP:LED I2C

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If there is a pre-existing error (e.g. bad SCPI command sent), then that error must be cleared with ``SUP:RES ERR`` before LED state can be changed.

.. _supervisor-nvm:

SUPervisor:NVM
===============================

Controls the non-volatile memory parameters of the |SupMCU| modules.
Use the ``SUP:CAL`` command to set calibration NVM values.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    SUPervisor:NVM <field>,<value>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Controls access to and writes settings to the non-volatile memory (NVM).
Order of operations when changing NVM is: <change value or values>, UNLOCK, WRITE.
Possible values of ``<field>`` and ``<value>`` are:

* ``UNLOCK`` - Unlocks the non-volatile memory for writing. ``<value>`` is a key consisting of ``device serial number + 12345``
* ``WRITE`` - Writes all set NVM values to non-volatile memory, ``<value>`` = 1.
* ``CRC`` - Sets the CRC value to ``<value>``.
* ``SERIAL`` - Sets the device serial number to value (decimal). `Note: Changing this value changes the key to unlock NVM memory on SupMCU Modules. Add 12345 to serial number for the new key.`
* ``I2C`` - Changes the 7-bit I2C address to ``<value>`` (hex or decimal).
* ``OSCTUN`` - Sets the oscillator tuning parameter to ``<value>``.
* ``RESET_TO`` - Changes the time before -RESET is allowed to reset a SupMCU Module. ``<value>`` is in seconds and is either 0 (-RESET never affects the module) or greater than or equal to 15 seconds. `Note: This is only used on BM2, EPSM, and DCPS currently.`
* ``DEBUG`` - Sets the NVM debug mask to ``<value>``; the current debug mask is also set to ``<value>``.
* ``BC`` - Resets the NVM boot counter.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Change the module's I2C address to 0x5D (assuming serial number is currently 0)::

    SUP:NVM I2C,0x5D
    SUP:NVM UNLOCK,12345
    SUP:NVM WRITE,1
    
Reset the NVM boot counter, change the SupMCU configuration word to 0x7F, and write it to NVM::

    SUP:NVM BC,RESET
    SUP:NVM DEBUG,0x7F
    SUP:NVM UNLOCK,12345
    SUP:NVM WRITE,1

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Most of the NVM commands take effect at startup, when the SupMCU reads the NVM
settings and applies them to the SupMCU application. Some NVM commands also take 
effect immediately.

The NVM is implemented as a block of flash memory. It is advised that when changing
more than one NVM setting at a time, unlock and write to NVM **after** all the NVM
setting commands have executed. 

In order for new NVM settings to become permanent in NVM, an unlock and write to NVM 
is required; failing to unlock and write will result in the NVM reverting to its prior 
settings at the next startup. Limit NVM writes to times when there is adequate 
power to keep the SupMCU alive during the write operation (ca 5ms long); failure to do so
may result in NVM corruption, resulting in the NVM being reset to its default
settings (see below). In order to avoid exceeding the cycle life of the NVM
flash memory, writes to NVM should be occasional and never at high frequencies.

If/when the SupMCU detects that the NVM has become corrupted or its checksum
is no longer valid, it will re-initialize the NVM with firmware-version-specific 
default settings. This can occur when a new firmware version is applied and the 
new firmware version includes supports for new NVM settings.

Each SupMCU has supervisor and module-specific NVM settings, with corresponding
supervisor- and module-specific commands. However, **unlocking and writing the NVM**
is strictly through :ref:`supervisor-nvm` or via ``unlock`` and ``write`` commands 
from inside the NVM directory in the CLI.

.. _supervisor-oscilator:

SUPervisor:OSCilator
===============================

Configure the internal oscillator of the MCU.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    SUPervisor:OSCilator <INTernal>,<pre>,<div>,<post>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Configure the internal oscillator based on the external clock.
A Prescaler, Divider and Postscaler satisfying the internal requirements of the processor are required.
Clock is calculated by: ``(7.23 Mhz*div)/(pre*post)``

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Set clock OSC to 1.8075 Mhz::

    SUP:OSC INT,2,1,2


Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

None.

.. _supervisor-calibration-query:

SUPervisor:CALibration?
===============================

Queries |SupMCU| calibration values present in all modules.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    SUPervisor:CALibration? LINEAR,<idx>,[ASCII | NAME | DATA]

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Request calibration values for a specific calibration index `idx` in a given format.
The formats are:

* ``DATA`` - Return calibration values in binary format. Not available over user debug terminal.
* ``ASCII`` - Return the calibration values in ASCII representation.
* ``NAME`` - The name of the calibration index. e.g. CTMU

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Query CTMU calibration values in ASCII::

    SUP:CAL? LINEAR, 0, ASCII


Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``LINEAR`` argument is required.
This is for forward compatibility with future calibration value types (e.g. quadratic).

.. _supervisor-calibration:

SUPervisor:CALibration
==================================

Sets |SupMCU| calibration values present in all modules.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    SUPervisor:CALibration LINEAR,<idx>,<OFFSET | SCALE_FACTOR>,<value>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Set calibration ``OFFSET`` or ``SCALE_FACTOR`` values for a specific calibration ``idx`` in the Supervisor MCU calibration values.
The calibration values are used internally when calculating the telemetry values for the user, applying a linear correction factor (E.g. ``final = scale_factor*initial + offset``).
Possible value sets are:

* ``OFFSET`` - Set the offset of the calibration factor. ``<value>`` is a number in the range [-32768, 32767]
* ``SCALE_FACTOR`` - Set the scale factor of the calibration factor. ``<value>`` is a floating-point number from [-16.0, 16.0].

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Set the SCALE_FACTOR to .975 and offset to -20 for the CTMU telemetry, save to NVM::

    SUP:CAL LINEAR,0,SCALE_FACTOR,0.975
    SUP:CAL LINEAR,0,OFFSET,-20
    SUP:NVM UNLOCK,12345
    SUP:NVM WRITE,1

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``LINEAR`` argument is required.
This is for forward compatibility with future calibration value types (e.g. quadratic).
These values directly impact the output of telemetry from the SupMCU modules.
Calibration values must be saved via :ref:`supervisor-nvm` commands.

See SupMCU Calibration for more details.

.. highlight:: python