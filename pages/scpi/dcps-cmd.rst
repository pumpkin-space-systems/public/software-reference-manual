.. _dcps-cmds:

************************
SCPI Commands
************************

.. highlight:: none

The DCPS Command set is responsible for:

* Turning on / off 3.3v, 5v, 12v and VBATT converters.

.. _DCPS-bus:

DCPS:BUS
===============================

Controls the state of the voltage buses on the DCPS modules.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    DCPS:BUS <3V3, 5V, 12V, VBATT>, <ON | OFF>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^

Turns the given DCPS bus ON or OFF:

* ``3V3, <ON | OFF>`` - Turns the 3.3v bus ON or OFF.
* ``5V, <ON | OFF>`` - Turns the 5v bus ON or OFF.
* ``12V, <ON | OFF>`` - Turns the 12v bus ON or OFF.
* ``VBATT, <ON | OFF>`` - Turns the VBATT bus ON or OFF.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn the 3.3v and 5v buses off::

    DCPS:BUS 3V3,OFF
    DCPS:BUS 5V,OFF

Turn the VBATT bus on::

    DCPS:BUS VBATT,ON

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^

All buses are enabled by default

.. _DCPS-nvm:

DCPS:NVM
===============================

Controls NVM values specific to the |DCPS| module.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    DCPS:NVM UNLOCK,<key>
    DCPS:NVM WRITE,1
    DCPS:NVM DEBUG,<mask>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^

Controls access to and writes parameters to the non-volatile memory.
Order of operations when changing NVM is: UNLOCK, <change value>, WRITE.
Possible value combinations are:

* ``UNLOCK, <key>`` - unlocks the non-volatile memory (must be done first). key is a value consisting of: ``device serial number + 12345``
* ``WRITE, <value>`` - Writes all set NVM values to non-volatile memory, value = 1.
* ``DEBUG, <mask>`` - Sets the debug mask to be set on boot. There is currently no debug masks for the DCPS.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^

Set debug mask to ``0x0020`` state upon boot and now::

    DCPS:NVM UNLOCK,12345
    DCPS:NVM DEBUG,0x0020
    DCPS:NVM WRITE,1

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^

None.

.. _DCPS-telemetry:

DCPS:TELemetry?
===============================

Requests a given telemetry item specific to the |DCPS| unit.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    DCPS:TELemetry? <N>,[DATA | ASCII | NAME | LENGTH]

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^

Request telemetry for a specific field N in the given format.
The formats are:

* ``DATA`` - Return telemetry in binary representation. Not available over user debug terminal.
* ``ASCII`` - Return telemetry in ASCII representation.
* ``NAME`` - The name of the telemetry item e.g. ``SupMCU Uptime``
* ``LENGTH`` - Length (in bytes) of telemetry in its binary representation.


Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^

Query DCPS 3.3V Converter in binary representation::

    DCPS:TEL? 6,DATA

Query the name of the 2nd DCPS telemetry item::

    DCPS:TEL? 1,NAME

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^

If no format representation is specified, ``DATA`` is used as the default.

See DCPS Telemetry section for more detail.

.. _DCPS-calibration-query:

DCPS:CALibration?
===============================

Queries |DCPS| specific calibration values.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    DCPS:CALibration? LINEAR,<idx>,[ASCII | NAME | DATA]

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^

Request calibration values for a specific calibration index `idx` in a given format.
The formats are:

* ``DATA`` - Return calibration values in binary format. Not available over user debug terminal.
* ``ASCII`` - Return the calibration values in ASCII representation.
* ``NAME`` - The name of the calibration index. e.g. CTMU

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^

Query ``12V`` calibration values in ASCII::

    DCPS:CAL? LINEAR, 0, ASCII

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``LINEAR`` argument is required.
This is for forward compatibility with future calibration value types (e.g. quadratic).

.. _DCPS-calibration:

DCPS:CALibration
===============================

Sets |DCPS| specific calibration values.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    DCPS:CALibration LINEAR,<idx>,<OFFSET | SCALE_FACTOR>,<value>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^

Set calibration ``OFFSET`` or ``SCALE_FACTOR`` values for a specific calibration ``idx`` in the Supervisor MCU calibration values.
The calibration values are used internally when calculating the telemetry values for the user, applying a linear correction factor (E.g. ``final = scale_factor*initial + offset``).
Possible value sets are:

* ``OFFSET`` - Set the offset of the calibration factor. ``<value>`` is a number in the range [-32768, 32767]
* ``SCALE_FACTOR`` - Set the scale factor of the calibration factor. ``<value>`` is a floating-point number from [-16.0, 16.0].

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^

Set the SCALE_FACTOR to .975 and offset to -20 for the ``12V`` calibration value, save to NVM::

    DCPS:CAL LINEAR,0,SCALE_FACTOR,0.975
    DCPS:CAL LINEAR,0,OFFSET,-20
    SUP:NVM UNLOCK,12345
    SUP:NVM WRITE,1

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``LINEAR`` argument is required.
This is for forward compatibility with future calibration value types (e.g. quadratic).
These values directly impact the output of telemetry from the SupMCU modules.
Calibration values must be saved via :ref:`supervisor-nvm` commands.

See DCPS Calibration for more details.

.. highlight:: python