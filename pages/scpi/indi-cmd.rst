***********************
SCPI Commands
***********************

.. highlight:: none

The |INDI| Command set is responsible for:

* Controlling power to the neon lamp
* Setting the heater into automatic set-point control or manual on/off commanding
* Controlling the shutter motor to run, idle or open/close
* Querying INDI specific Telemetry.
* Setting INDI specific NVM values

.. _indi-heater:

INDI:HEATER
==================================

Controls the state of the heater unit on the INDI controller board.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    INDI:HEATER <ON | OFF>
    INDI:HEATER <AUTO>
    INDI:HEATER SET,<VALUE>
    INDI:HEATER GET

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Selects what set-point and mode the INDI heater should be in. There are two different modes:

* ``AUTO`` - Automatically keep the temperature at the setpoint set via either the NVM ``INDI:NVM SET,###`` command or ``INDI:HEATER SET``
* ``<ON | OFF>`` - Force the heater unit ON or OFF

In addition, the set-point can be set and retrieved via:

* ``SET,<VALUE>`` - to store heater setpoint in °K
* ``GET`` - to recall heater setpoint in °K

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Set the heater setpoint to 335K, save to NVM memory to persist and turn the INDI heater to automatic mode::

    INDI:HEATER SET,335
    INDI:NVM UNLOCK,12345
    INDI:NVM WRITE,1
    INDI:HEATER AUTO

Force the INDI heater unit on::

    INDI:HEATER ON

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

None.

.. _indi-lamp:

INDI:LAMP
==================================

Controls the state of the electrical power for the neon lamp.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    INDI:LAMP <ON | OFF>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turns the neon lamp ON or OFF.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn on the INDI neon lamp::

    INDI:LAMP ON

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

None.

.. _indi-shutter:

INDI:SHUTTER
==================================

Controls the state of the camera shutter motor for the INDI controller board.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    INDI:SHUTTER <OPEN | CLOSE>
    INDI:SHUTTER RUN
    INDI:SHUTTER IDLE
    INDI:SHUTTER SET,<ms>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``INDI:SHUTTER`` command can run the shutter either in an ``OPEN | CLOSE`` mode, or a full cycle via the ``INDI:SHUTTER RUN``.

To control the period of the ``RUN`` cycle, the ``INDI:SHUTTER SET,<ms>`` command can be sent to set the period in ``ms``.
The current setting can be recalled over the Debug UART connection via the ``INDI:SHUTTER GET`` command.

In addition, the shutter motor can be completely de-energized via the ``INDI:SHUTTER IDLE`` command.


Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Set the INDI cycle to 100ms, save to NVM to persist across boots, then run one cycle can be done via ::

    INDI:SHUTTER SET,100
    INDI:NVM UNLOCK,12345
    INDI:NVM WRITE,1
    INDI:SHUTTER RUN

To open the shutter, run ::

    INDI:SHUTTER OPEN

To close the shutter and de-energize the circuitry, run ::

    INDI:SHUTTER CLOSE
    INDI:SHUTTER IDLE

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

None.

.. _indi-quasar:

INDI:QUASAR
==================================

Controls the state of the GPIO to the recovery and reset lines on the Quasar TX2 carrier board.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    INDI:QUASAR <RECOVERY_OFF | RECOVERY_ON | RESET>


Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Controls the GPIO pins connected to the recovery and reset lines on the Quasar TX2 carrier board
Choices for controlling GPIO state are:

* ``RECOVERY_ON | RECOVERY_OFF`` - Turns the System Recovery signal on the Quasar GPIO header to either ON (shorted to GND) or OFF (NC).
* ``RESET`` - Pulses the RESET signal to GND on the GPIO header to initiate a system reset.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn on the recovery pin, connecting it to ground ::

    INDI:QUASAR RECOVERY_ON

Reset the TX2 via Quasar RESET line ::

    INDI:QUASAR RESET

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

None.

.. _indi-debug:

INDI:DEBug
==================================

Controls the output of debug messages to the debug UART terminal.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    INDI:DEBug <ENable | DISable>, mask

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Enables or disables debug information being printed out over the UART Debug port on the INDI.
Mask is the bits of the debug information to enable.
Possible value combinations are:

* ``ENable, <mask>`` - Enables the bits represented by the mask value. Mask is a bit field and bits that are 1 will enable debug information.
* ``DISable, <mask>`` - Disables the bits represented by the mask value. Mask is a bit field and bits that are 1 will disable debug information.

There is 1 bit used for debug information on the INDI:

* ``0x0001`` - Prints out the current temperature sensor readings to the debug UART

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Enable temperature sensor output to the Debug UART::

    INDI:DEB EN,0x0001

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

All ``INDI:DEBug`` mask settings are *temporary* unless set with the ``INDI:NVM DEBUG,#`` command then saved to NVM via ``INDI:NVM UNLOCK,12345`` and ``INDI:NVM WRITE,1``.

.. _indi-nvm:

INDI:NVM
==================================

Controls NVM parameters specific to the |INDI| module

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    NVM control commands
    RHM:NVM <UNLOCK, <key> | WRITE, <1> | DEBUG, <mask>>

    NVM values for setting the sensors used for automatic setpoint control
    RHM:NVM SENSOR_MASK,<mask>

    NVM values for setting the LEDs illuminated during operation
    RHM:NVM LED_MASK,<mask>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Controls access to and writes parameters to the non-volatile memory, which affect module operation **across restarts**.
Order of operations when changing NVM is: UNLOCK, <change value>, WRITE.
Possible value combinations are:

* ``UNLOCK`` - Unlocks the non-volatile memory (must be done first). Value is a key consisting of: ``device serial number + 12345``
* ``WRITE`` - Writes all set NVM values to non-volatile memory, value = 1
* ``DEBUG,<mask>`` - Sets the debug flag to use at boot of the module. See ``INDI:DEBug`` command for list of masks.
* ``SENSOR_MASK,<mask>`` - Sets the set of temperature sensors to be used by the ``INDI:HEATER AUTO`` automatic setpoint control. Possible bits are:
    * ``0x0001`` - Use Temperature sensor #1 readings
    * ``0x0002`` - Use Temperature sensor #2 readings
    * ``0x0004`` - Use Temperature sensor #3 readings
    * ``0x0008`` - Use temperature sensor #4 readings
    * Use a binary OR operation to combine the masks together to monitor multiple temperature sensors for automatic setpoint control (e.g. use ``0x0003`` to monitor #1 and #2 temperature sensors).
* ``LED_MASK,<mask>`` - Sets the LEDs that are illuminated during operation. The possible bits for LEDs are:
    * ``0x0001`` - The SupMCU status LED will be illuminated
    * ``0x0002`` - THe LAMP status LED will be illuminated
    * ``0x0004`` - The HEATER status LED will be illuminated
    * ``0x0008`` - The SHUTTER status LED will be illuminated
    * Use a binary OR operation to combine the masks together in order to illuminate zero or more LEDs. To disable all LED illumination, use a mask of ``0x0000``.


Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Set the INDI control board to only monitor Temperature Sensor #1 and #2, then save NVM is....
Assuming serial number of 0::

    INDI:NVM UNLOCK,12345
    INDI:NVM SENSOR_MASK,0x0003
    INDI:NVM WRITE,1

Set the INDI control board to illuminate all LEDs for operation, then save NVM is...
Assuming serial number of 0::

    INDI:NVM UNLOCK,12345
    INDI:NVM LED_MASK,0x000F
    INDI:NVM WRITE,1

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

All changes made via the NVM command are immediately applied (e.g. when the ``INDI:NVM SENSOR_MASK,...`` command is sent), so no restart is required.

All NVM parameters will be reset back to previous values, unless the ``INDI:NVM UNLOCK,12345`` and ``INDI:NVM WRITE,1`` is sent.

.. _indi-telemetry:

INDI:TELemetry?
==================================

Requests a given telemetry item specific to the |INDI| unit.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    INDI:TELemetry? <N>,[DATA | ASCII | NAME | LENGTH]

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Request telemetry for a specific field N in the given format.
The formats are:

* ``DATA`` - Return telemetry in binary representation. Not available over user debug terminal.
* ``ASCII`` - Return telemetry in ASCII representation.
* ``NAME`` - The name of the telemetry item e.g. ``SupMCU Uptime``
* ``LENGTH`` - Length (in bytes) of telemetry in its binary representation.


Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Query INDI for the Temperature Sensor #1 reading over the Debug UART::

    INDI:TEL? 4,ASCII

Query INDI for name of 4th telemetry item::

    INDI:TEL? 3,NAME

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If no format representation is specified, ``DATA`` is used as the default, which is only supported for output over the I2C bus.

See INDI Telemetry section for more detail.

.. _indi-calibration:

INDI:CALibration
===============================

Sets |INDI| specific calibration values.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    INDI:CALibration LINEAR,<idx>,<OFFSET | SCALE_FACTOR>,<value>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^

Set calibration ``OFFSET`` or ``SCALE_FACTOR`` values for a specific calibration ``idx`` in the Supervisor MCU calibration values.
The calibration values are used internally when calculating the telemetry values for the user, applying a linear correction factor (E.g. ``final = scale_factor*initial + offset``).
Possible value sets are:

* ``OFFSET`` - Set the offset of the calibration factor. ``<value>`` is a number in the range [-32768, 32767]
* ``SCALE_FACTOR`` - Set the scale factor of the calibration factor. ``<value>`` is a floating-point number from [-16.0, 16.0].

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^

Set the SCALE_FACTOR to .975 and offset to -20 for the ``TEMP_SENS1`` calibration value, save to NVM::

    EPSM:CAL LINEAR,0,SCALE_FACTOR,0.975
    EPSM:CAL LINEAR,0,OFFSET,-20
    SUP:NVM UNLOCK,12345
    SUP:NVM WRITE,1

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``LINEAR`` argument is required.
This is for forward compatibility with future calibration value types (e.g. quadratic).
These values directly impact the output of telemetry from the SupMCU modules.
Calibration values must be saved via :ref:`supervisor-nvm` commands.

See INDI Calibration for more details.


.. highlight:: python

