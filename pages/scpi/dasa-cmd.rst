***************************
SCPI Commands
***************************

.. highlight:: none

The |DASA| Command set is responsible for:

* Articulating the DASA array clockwise/counter-clockwise a set amount of steps.
* Setting the articulated solar array to an absolute position in steps.
* Homing the articulated solar array to its center position.
* Manipulating motor drive manually.
* Energizing the solar panel release mechanism to deploy the solar array.
* Switching the -1.8v power rail to extend the range of the temperature sensors.

.. _dasa-debug:

DASA:DEBug
============================

Controls the output of debug messages specific to the DASA over the debug UART terminal.

Syntax
^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    DASA:DEBug <ENable | DISable>,<mask>

Description
^^^^^^^^^^^^^^^^^^^^^^

Enables or disables debug messages to be output from the SupMCU Module.
The following masks are supported on the DASA module:

* ``0x0001`` - Control debug messages outputting the state of A+/- and B+/- drive circuity. These messages are displayed when the array is rotating via :ref:`DASA:STEP <dasa-step>` command.
* ``0x0002`` - Shows the current HES sensor readings during a ``DASA:STEP HOME`` move

Examples
^^^^^^^^^^^^^^^^^^^^^^

Enable debug messages for state of the drive circuity::

    DASA:DEB EN,0x0001

Disable debug messages for state of the drive circuity::

    DASA:DEB DIS,0x0001

Remarks
^^^^^^^^^^^^^^^^^^^^^^

Debug messages are only displayed via the Debug Serial connection.
They will not be able to be retrieved via I2C.

.. _dasa-release:

DASA:RELease
============================

Controls the pin-puller release mechanism on the |DASA|.

Syntax
^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    DASA:RELease <DISAble | ENABle | UNARm | ARM | FIRE, <N>>

Description
^^^^^^^^^^^^^^^^^^^^^^

Controls the solar panel release mechanism.
When energizing the release mechanism, the order of operations is ENABLE → ARM → FIRE, N.
Possible value combinations are:

* ``DISAble`` - Disables the solar panel release mechanism, effectively resetting the sequence.
* ``ENABle`` - Enables the solar panel release mechanism, first step in panel deployment.
* ``ARM`` - Arms the solar panel release mechanism, second step in panel deployment.
* ``UNARm`` - Disables the SupMCU task responsible for energizing the Pin Puller. Does not disable the solar panel release mechanism.
* ``FIRE, <Seconds>`` - Energizes the Pin Puller, releasing the deployment mechanism of the solar panels. Will keep energized for N seconds where N is in the range [1, 30].

Examples
^^^^^^^^^^^^^^^^^^^^^^

Energize the solar panel release mechanism for 25 seconds::

    DASA:REL ENAB
    DASA:REL ARM
    DASA:REL FIRE,25

Reset the solar panel release mechanism sequence::

    DASA:REL DISAble

Remarks
^^^^^^^^^^^^^^^^^^^^^^

Any command done out of order will reset the sequence. E.g. if ``DASA:REL ENAB`` → ``DASA:REL FIRE,30`` is done, then ``DASA:REL ENAB`` must be sent again.

.. _dasa-step:

DASA:STEP
============================

Controls the motor drive circuitry to drive a certain amount of steps.

Syntax
^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    DASA:STEP <STOP>
    or
    DASA:STEP <REL | ABS | HOME>,<HALF | FULL>,<rate>,<steps>

Description
^^^^^^^^^^^^^^^^^^^^^^

Controls the position of the articulated solar array.
The command :ref:`DASA:DRIVE ENABLE <dasa-drive>` must be sent before the array will articulate.
Possible value combinations are:

* ``STOP`` - Stops movement of the articulated solar array, canceling any motor movement.
* ``REL, HALF | FULL, rate, steps`` - Drive the motor circuit to rotate the array relative to the current position. The steps argument can be negative, which will rotate the array counter-clockwise, while a positive value rotates the array clockwise. FULL | HALF argument controls whether to drive motor circuit by full steps, or half stepper motor steps. The command will stop movement if the drive circuit detects the rotation limit via hall effect sensor during rotation. Maximum rate is 25 Hz, if a greater rate is given, it is clamped to 25 Hz.
* ``HOME, HALF | FULL, rate, steps`` - Homes the articulated array to its 12 o’clock position by driving the motor circuit to rotate clockwise until either detecting the home position via hall effect sensor, or reaching the maximum steps limit given by steps. The Homing algorithm will attempt to find home via a min/max reading on the Hall-effect sensor, if found, set the position telemetry to 0, and set the homed bit to 1. Otherwise the DASA will stop rotation and leave homed as 0 and position as the current value. Rate is the frequency at which the pulse-width modulation is driven. FULL | HALF argument controls whether to drive motor circuit by full steps, or half stepper motor steps. The homed bit is NOT set if the DASA array DOES NOT detect the homed position via hall effect sensor. Its position is also not set to 0 if it does not detect the rotation limit via hall effect sensor at the limits. Maximum rate is 25 Hz, if a greater rate is given, it is clamped to 25 Hz.
* ``ABS, HALF | FULL, rate, position`` - Sets he array to an absolute position, rotating CW or CCW based on shortest path to position. FULL | HALF argument controls whether to drive motor circuit by full steps, or half stepper motor steps. The command will stop movement if the drive circuit detects the rotation limit via hall effect sensor during rotation. Maximum rate is 25 Hz, if a greater rate is given, it is clamped to 25 Hz.

Examples
^^^^^^^^^^^^^^^^^^^^^^


Turn the DASA array 200 steps counter-clockwise at 10 Hz step rate using FULL steps::

    DASA:DRIVE ENABLE
    DASA:STEP REL,FULL,10,200

Stop movement of the DASA array::

    DASA:STEP STOP

Set the DASA array to a position of 269, moving at 7 Hz, holding the position at a half step::

    DASA:DRIVE ENABLE
    DASA:DRIVE HOLD
    DASA:STEP ABS,HALF,7, 269

Home the DASA array, allowing it to move a maximum of 2000 steps either direction at 15 Hz::

    DASA:DRIVE ENABLE
    DASA:STEP HOME,FULL,15,2000

Remarks
^^^^^^^^^^^^^^^^^^^^^^

None.

.. _dasa-rotate:

DASA:ROTate
============================

Controls the motor drive circuity, commanding the array to move a certain amount of degrees.

Syntax
^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    DASA:ROTate <STOP>
    or
    DASA:ROTate <REL | ABS | HOME>,<HALF | FULL>,<rate>,<angle>

Description
^^^^^^^^^^^^^^^^^^^^^^

Controls the angle of the articulated solar array.
This command converts the angle given to a number of steps to rotate the stepper motor drive.
Possible value combinations are:

* ``STOP`` - Stops movement of the articulated solar array, canceling any motor movement.
* ``REL, HALF | FULL, rate, angle`` - Drive the motor circuit to rotate the array relative to the current angle. The angle argument can be negative, which will rotate the array counter-clockwise, while a positive value rotates the array clockwise. FULL | HALF argument controls whether to drive motor circuit by full steps, or half stepper motor steps. The command will stop movement if the drive circuit detects the rotation limit via hall effect sensor during rotation. Maximum step rate is 25 Hz, if a greater rate is given, it is clamped to 25 Hz.
* ``HOME, HALF | FULL, rate, max_angle`` - Homes the articulated array to its 12 o’clock position by driving the motor circuit to rotate clockwise until either detecting the home position via hall effect sensor, or reaching the maximum angle given by max_angle. The Homing algorithm will attempt to find home via a min/max reading on the Hall-effect sensor, if found, set the position telemetry to 0, and set the homed bit to 1. Otherwise the DASA will stop rotation and leave homed as 0 and position as the current value. Rate is the frequency at which the pulse-width modulation is driven. FULL | HALF argument controls whether to drive motor circuit by full steps, or half stepper motor steps. The homed bit is NOT set if the DASA array DOES NOT detect the homed position via hall effect sensor. Its position is also not set to 0 if it does not detect the rotation limit via hall effect sensor at the limits. Maximum rate is 25 Hz, if a greater rate is given, it is clamped to 25 Hz.
* ``ABS, HALF | FULL, rate, position`` - Sets the array to an absolute position, rotating CW or CCW based on shortest path to position. FULL | HALF argument controls whether to drive motor circuit by full steps, or half stepper motor steps. The command will stop movement if the drive circuit detects the rotation limit via hall effect sensor during rotation. Maximum rate is 25 Hz, if a greater rate is given, it is clamped to 25 Hz.

Examples
^^^^^^^^^^^^^^^^^^^^^^

Turn the DASA array 45.7 degrees counter-clockwise at 1 degree per second using FULL steps::

    DASA:DRIVE ENABLE
    DASA:ROT REL,FULL,1,45.7

Stop movement of the DASA array::

    DASA:ROT STOP

Set the DASA array to an angle of 110 degrees, moving at 1.5 degrees per second, using half-steps::

    DASA:DRIVE ENABLE
    DASA:ROT ABS,HALF,1.5, 110

Home the DASA array, allowing it to move a maximum of 360 degrees in either direction at 2 degrees per second in FULL steps::

    DASA:DRIVE ENABLE
    DASA:ROT HOME,FULL,2,360

Remarks
^^^^^^^^^^^^^^^^^^^^^^

None.

.. _dasa-hes:

DASA:HES
============================

Controls the electrical power to the hall-effect sensor on the DASA.

Syntax
^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    DASA:HES <ENABle | DISAble>

Description
^^^^^^^^^^^^^^^^^^^^^^

Enables or disables the DASA hall-effect sensor used to home the articulated array.
Possible value combinations are:

* ``ENABle`` - Enables the HES. It’s value can be read via ``DASA:TEL? 6``.
* ``DISAble`` - Disables the HES.

Examples
^^^^^^^^^^^^^^^^^^^^^^

Enable the hall-effect sensor::

    DASA:HES ENABLE

Disable the hall-effect sensor::

    DASA:HES DISABLE


Remarks
^^^^^^^^^^^^^^^^^^^^^^

Disabling the HES during a homing move WILL cause the home to fail. The HES is automatically enabled during a HOME move.

.. _dasa-ctrl:

DASA:CTRL
============================

Controls the state of the motor drive circuitry manually. **Not recommended for use, use** :ref:`DASA:STEP <dasa-step>` **instead**.

Syntax
^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    DASA:CTRL <OPEN | CLOSE | AOFF | APOS | ANEG | BOFF | BPOS | BNEG | SENSE>

Description
^^^^^^^^^^^^^^^^^^^^^^

Manually controls the Motor A and B drive circuits.
The drive circuit must be opened via ``DASA:CTRL OPEN`` before sending any ``DASA:CTRL`` commands.
Note to drive the array CW, the sequence is A+ ON → B+ ON → A- ON → B- ON, and to drive the array CCW, the sequence is B+ ON → A+ ON → B- ON → A- ON.
Possible value combinations are:

* ``OPEN`` - Opens the drive circuit, first step before manually controlling the DASA motor.
* ``CLOSE`` - Closes the drive circuit, last step to stop movement of controlling the DASA motor.
* ``AOFF`` - Turns off the A+/- motor drive.
* ``BOFF`` - Turns off the B+/- motor drive.
* ``APOS`` - Turns on A+ motor drive.
* ``BPOS`` - Turns on B+ motor drive.
* ``ANEG`` - Turns on A- motor drive.
* ``BNEG`` - Turns on B- motor drive.
* ``SENSE`` - Outputs current driven through A and B motor drive circuits to debug UART.

Examples
^^^^^^^^^^^^^^^^^^^^^^

Turn the DASA array one step clockwise (CW)::

    DASA:CTRL OPEN
    DASA:CTRL APOS
    DASA:CTRL BPOS
    DASA:CTRL ANEG
    DASA:CTRL BNEG
    DASA:CTRL AOFF
    DASA:CTRL BOFF
    DASA:CTRL CLOSE

Turn the DASA array one step counter-clockwise (CCW)::

    DASA:CTRL OPEN
    DASA:CTRL BPOS
    DASA:CTRL APOS
    DASA:CTRL BNEG
    DASA:CTRL ANEG
    DASA:CTRL BOFF
    DASA:CTRL AOFF
    DASA:CTRL CLOSE

Output current flowing through A and B drive circuits::

    DASA:CTRL SENSE

Remarks
^^^^^^^^^^^^^^^^^^^^^^

Use with caution; can cause damage to the motor drive circuitry or DASA module its self through improper use.

.. _dasa-npr:

DASA:NPR
============================

Controls the power to the voltage reference for the temperature sensors on the DASA array.

Syntax
^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    DASA:NPR <ENable | DISable>

Description
^^^^^^^^^^^^^^^^^^^^^^

Turns the -1.8v power rail ON or OFF to the temperature sensing circuitry to increase the temperature ranges on the sensors.

Examples
^^^^^^^^^^^^^^^^^^^^^^

Enable the NPR::

    DASA:NPR ENABLE

Read Temperature sensor 2 via serial debug::

    DASA:NPR ENABLE
    DASA:TEL? 7,ASCII

Remarks
^^^^^^^^^^^^^^^^^^^^^^

In order to read the temperature sensors on the DASA, the NPR must be enabled via ``DASA:NPR ENable`` command.

.. _dasa-drive:

DASA:DRIVE
============================

Controls the current state of the drive circuitry on the DASA array.
Used to enable/disable motors, set drive position, set drive hold/release mode.

Syntax
^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    DASA:DRIVE <ENable | DISable | STORe | RECALL | HOLD | RELease | ANGle, <value> | POSition, <value>>

Description
^^^^^^^^^^^^^^^^^^^^^^

Sets the state of the drive circuitry on the DASA.
Possible value combinations are:

* ``ENable``/``DISable`` - Enables or disables the drive circuitry on the DASA. **Required before any STEP/ROTate commands**.
* ``STORe``/``RECALL`` - Stores or recalls the last known step position from NVM memory.
* ``HOLD``/``RELease`` - Sets the drive motors to HOLD position (e.g. apply power to last step) or release.
* ``ANGle, <value>``, ``POSition, <value>`` - Sets the current data angle (``ANGle``) or step position (``POSition``) to the given ``<value>``

Examples
^^^^^^^^^^^^^^^^^^^^^^

Enable the DASA drive::

    DASA:DRIVE ENABLE

Set the motor position to 70 degrees::

    DASA:DRIVE ANG,70

Remarks
^^^^^^^^^^^^^^^^^^^^^^

In order to use the articulation feature of the DASA, the drive must be enabled via ``DASA:DRIVE ENable``

.. _dasa-telemetry:

DASA:TELemetry?
============================

Requests a given telemetry item specific to the |DASA| unit.

Syntax
^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    DASA:TELemetry? <N>,[DATA | ASCII | NAME | LENGTH]

Description
^^^^^^^^^^^^^^^^^^^^^^

Request telemetry for a specific field N in the given format.
The formats are:

* ``DATA`` - Return telemetry in binary representation. Not available over user debug terminal.
* ``ASCII`` - Return telemetry in ASCII representation.
* ``NAME`` - The name of the telemetry item e.g. ``SupMCU Uptime``
* ``LENGTH`` - Length (in bytes) of telemetry in its binary representation.


Examples
^^^^^^^^^^^^^^^^^^^^^^

Query DASA for Drive Motor Status in ASCII format::

    DASA:TEL? 1,ASCII

Query DASA for drive motor position in binary format::

    DASA:TEL? 0

Query DASA for name of third telemetry item::

    DASA:TEL? 2,NAME

Remarks
^^^^^^^^^^^^^^^^^^^^^^

If no format representation is specified, ``DATA`` is used as the default.

See DASA Telemetry section for more detail.

.. _dasa-calibration-query:

DASA:CALibration?
============================

Queries |DASA| specific calibration values.

Syntax
^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    DASA:CALibration? LINEAR,<idx>,[ASCII | NAME | DATA]

Description
^^^^^^^^^^^^^^^^^^^^^^

Request calibration values for a specific calibration index `idx` in a given format.
The formats are:

* ``DATA`` - Return calibration values in binary format. Not available over user debug terminal.
* ``ASCII`` - Return the calibration values in ASCII representation.
* ``NAME`` - The name of the calibration index. e.g. CTMU

Examples
^^^^^^^^^^^^^^^^^^^^^^

Query ``temp. sensor 1`` calibration values in ASCII::

    DASA:CAL? LINEAR, 0, ASCII

Remarks
^^^^^^^^^^^^^^^^^^^^^^

The ``LINEAR`` argument is required.
This is for forward compatibility with future calibration value types (e.g. quadratic).

.. _dasa-calibration:

DASA:CALibration
============================

Sets |DASA| specific calibration values.

Syntax
^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    DASA:CALibration LINEAR,<idx>,<OFFSET | SCALE_FACTOR>,<value>

Description
^^^^^^^^^^^^^^^^^^^^^^

Set calibration ``OFFSET`` or ``SCALE_FACTOR`` values for a specific calibration ``idx`` in the Supervisor MCU calibration values.
The calibration values are used internally when calculating the telemetry values for the user, applying a linear correction factor (E.g. ``final = scale_factor*initial + offset``).
Possible value sets are:

* ``OFFSET`` - Set the offset of the calibration factor. ``<value>`` is a number in the range [-32768, 32767]
* ``SCALE_FACTOR`` - Set the scale factor of the calibration factor. ``<value>`` is a floating-point number from [-16.0, 16.0].

Examples
^^^^^^^^^^^^^^^^^^^^^^

Set the SCALE_FACTOR to .975 and offset to -20 for the `temp. sensor 1` calibration value, save to NVM::

    DASA:CAL LINEAR,0,SCALE_FACTOR,0.975
    DASA:CAL LINEAR,0,OFFSET,-20
    SUP:NVM UNLOCK,12345
    SUP:NVM WRITE,1


Remarks
^^^^^^^^^^^^^^^^^^^^^^

The ``LINEAR`` argument is required.
This is for forward compatibility with future calibration value types (e.g. quadratic).
These values directly impact the output of telemetry from the SupMCU modules.
Calibration values must be saved via :ref:`SUP:NVM <supervisor-nvm>` commands.

See DASA Calibration for more details.

.. highlight:: python
