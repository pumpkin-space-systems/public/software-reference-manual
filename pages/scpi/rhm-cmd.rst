***********************
SCPI Commands
***********************

.. highlight:: none

.. note:: Items marked as **RHM1 Only** are not present on **RHM2** modules.

The |RHM| Command set is responsible for:

* Controlling power to GlobalStar Simplex radio
* Controlling UART communication channel to/from GlobalStar Simplex radio.
* Controlling Digital Input (DIN) pins for GlobalStar Simplex radio.
* Enabling or disabling UART communications to/from GlobalStar Simplex radio.
* RHM1 Only – Controlling power to Astrodev Lithium radio.
* RHM1 Only – Controlling UART communication channel to/from Astrodev Lithium radio.
* RHM1 Only – Enabling or disabling UART communications to/from Astrodev Lithium radio.
* Querying RHM specific Telemetry.
* Setting RHM specific NVM values

.. _rhm-gstar-comm:

RHM:GStar:COMM
==================================

Controls the state of the hardware UART Mux for the GlobalStar UART communication line.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    RHM:GStar:COMM <NONE | UART0 | UART1 | UART2 | UART3 | UART4 | I2C>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Selects what CSK UART channel the GlobalStar Simplex radio should communicate on.
UART choices are:

* ``NONE`` - Disables UART communication to GlobalStar Simplex.
* ``UART0-UART4`` - Sets UART communication for GlobalStar Simplex to CSK UART 0, 1, 2, 3 or 4. `See MBM2 Datasheet for mapping of CSK UART’s to BBB UART’s.` CSK UART4 is a TX-only UART.
* ``I2C`` - Sets UART communication for GlobalStar Simplex to one of the two available internal SupMCU UART communication channels. `Port only accessible by SupMCU.`

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Set GlobalStar Simplex to communicate on CSK UART3 (BBB UART4) and powering it on::

    RHM:GS:COMM UART3
    RHM:GS:POW ON
    RHM:GS:PASS ON

Turn off UART communication to GlobalStar Simplex::

    RHM:GS:COMM NONE
    RHM:GS:PASS OFF

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Communication must still be passed through via a :ref:`RHM:GS:PASS ON <rhm-gstar-passthrough>` command.

.. _rhm-gstar-power:

RHM:GStar:POWer
==================================

Controls the state of the electrical power for the GlobalStar Simplex.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    RHM:GStar:POWer <ON | OFF>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turns the power supply to the GlobalStar radio ON or OFF

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn on the GlobalStar Simplex::

    RHM:GS:POW ON

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

None.

.. _rhm-gstar-passthrough:

RHM:GStar:PASSthrough
==================================

Controls the state of the UART passthrough for the GlobalStar Simplex radio.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    RHM:GStar:PASSthrough <ON | OFF>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Sets the Globalstar's UART passthrough ON or OFF

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn off UART Passthrough for GlobalStar Simplex::

    RHM:GS:PASS OFF

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

None

.. _rhm-gstar-din:

RHM:GStar:DIN
==================================

Controls the state of the GPIO to the DIN ports on the GlobalStar Simplex radio.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    RHM:GS:DIN <ON | OFF>,<pin>
    RHM:GS:DIN <ENABLE | DISABLE>


Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Controls the Digital input pins on the GlobalStar radio.
Choices for controlling DIN state are:

* ``ON | OFF, PIN`` - Turns the given digital input PIN ON or OFF. PIN is either 1 or 2.
* ``ENABLE | DISABLE`` - Enables or disables the GlobalStar Simplex Digital Input.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn on Digital Input pin #2 and enable digital input::

    RHM:GS:DIN ON,2
    RHM:GS:DIN ENABLE

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

None.

.. _rhm-lithium-comm:

RHM:LIthium:COMM
==================================

Controls the state of the hardware UART Mux to the Astrodev Lithium radio.
**Only on RHM1**

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    RHM:LIthium:COMM <NONE | UART0 | UART1 | UART2 | UART3 | I2C>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Selects what CSK UART channel the Lithium radio should communicate on.
UART choices are:

* ``NONE`` - Disables UART communication to Lithium.
* ``UART0-UART3`` - Sets UART communication for Lithium to CSK UART 0, 1, 2, or 3. `See MBM2 Datasheet for mapping of CSK UART’s to BBB UART’s.`
* ``I2C`` - Sets UART communication for Lithium to one of the two available internal SupMCU UART communication channels. `Ports only accessible by SupMCU.`

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Set UART Communication to Lithium radio to CSK UART1 (BBB UART1) then power and enable communications::

    RHM:LI:COMM UART1
    RHM:LI:POW ON
    RHM:LI:PASS ON

Disable UART communication to Lithium radio and disable passthrough::

    RHM:LI:COMM NONE
    RHM:LI:PASS OFF

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Communication must still be passed through via a :ref:`RHM:LI:PASS ON <rhm-lithium-passthrough>` command.

.. _rhm-lithium-passthrough:

RHM:LIthium:PASSthrough
==================================

Controls the state of the UART passthrough for the Astrodev Lithium radio.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    RHM:LIthium:PASSthrough <ON | OFF>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Sets the Lithium's UART passthrough ON or OFF

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn off UART Passthrough for Lithium Radio::

    RHM:LI:PASS OFF

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

None

.. _rhm-lithium-config:

RHM:LIthium:CONFig
==================================

Controls the state of the configure pin on the Astrodev Lithium radio.
**RHM1 Only command**

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    RHM:LIthium:CONFig <1 | 2>, <ON | OFF>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Sets the configure pin number (1 or 2) of the Lithium Radio ON or OFF.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn off configure pin #1::

    RHM:LI:CONF 1,OFF

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

None

.. _rhm-lithium-mcu:

RHM:LIthium:MCU
==================================

Controls the electrical power state of the MCU for the Astrodev Lithium radio.
**RHM1 Only command**

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    RHM:LIthium:MCU <ON | OFF>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn the Lithium's MCU power ON or OFF.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn on Lithium MCU::

    RHM:LI:MCU ON

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

None.

.. _rhm-lithium-pa:

RHM:LIthium:PA
==================================

Controls the state of the electrical power to the power amplifier of the Astrodev Lithium Radio.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    RHM:LIthium:PA <ON | OFF>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn the Lithium's power amplifier (PA) power ON or OFF.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn on Lithium Power-amplifier::

    RHM:LI:PA ON

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

None.

.. _rhm-lithium-power:

RHM:LIthium:POWer
==================================

Controls the state of the power to the Astrodev Lithium Radio PA and MCU.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    RHM:LIthium:POWer <ON | OFF>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn the Lithium's MCU and PA power ON or OFF

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn on Lithium MCU and PA::

    RHM:LI:POW ON

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

None.

.. _rhm-debug:

RHM:DEBug
==================================

Controls the output of debug messages to the debug UART terminal.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    RHM:DEBug <ENable | DISable>, mask

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Enables or disables debug information being printed out over the UART Debug port on the RHM.
Mask is the bits of the debug information to enable.
Possible value combinations are:

* ``ENable, <mask>`` - Enables the bits represented by the mask value. Mask is a bit field and bits that are 1 will enable debug information.
* ``DISable, <mask>`` - Disables the bits represented by the mask value. Mask is a bit field and bits that are 1 will disable debug information.

There are 4 bits used for debug information on the RHM:

* ``0x0001`` - Prints out configuration of GlobalStar Simplex Radio when it is powered on.
* ``0x0002`` - Prints out raw bytes being sent to Simplex Radio from RHM:SEND command.
* ``0x0004`` - Prints out raw bytes read from Simplex radio from RHM:SEND command.
* ``0x0008`` - Prints out state of BUSY Line during RHM:SEND command.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Enable all debug messages from the ``RHM:GS:SEND`` command::

    RHM:DEB EN,0x000E

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

None.

.. _rhm-nvm:

RHM:NVM
==================================

Controls NVM parameters specific to the |RHM| module

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    NVM control commands
    RHM:NVM <UNLOCK, <key> | WRITE, <1> | DEBUG, <mask>>

    NVM values for GlobalStar Simplex
    RHM:NVM <GS>,<COMM,  <CHAN>  | <UART>, <ENABLE | DISABLE> | <POW>, <ON | OFF>>

    NVM values for Lithium (RHM1 only)
    RHM:NVM <LI>, <COMM, <CHAN> | <POW>, <ON | OFF> | <UART>, <ON | OFF> | <PAPOW>, <ON | OFF> | <MCUPOW>, <ON | OFF>>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Controls access to and writes parameters to the non-volatile memory.
Order of operations when changing NVM is: UNLOCK, <change value>, WRITE.
Possible value combinations are:

* ``UNLOCK`` - Unlocks the non-volatile memory (must be done first). Value is a key consisting of: ``device serial number + 12345``
* ``WRITE`` - Writes all set NVM values to non-volatile memory, value = 1
* ``DEBUG,<mask>`` - Sets the debug flag to use at boot of the module. See ``RHM:DEBug`` command for list of masks.
* ``GS,COMM,<CHAN>`` - Sets the UART communications channel the GlobalStar Simplex is on upon startup of the RHM. CHAN can be in the set {NONE, UART0, UART1, UART2, UART3, UART4, I2C}. Default is NONE.
* ``GS,UART,<ENABLE | DISABLE>`` - Sets if UART passthrough should be enabled or disabled to the GlobalStar Simplex upon startup of the RHM. Default is disabled.
* ``GS,POW,<ON | OFF>`` - Sets if the GlobalStar Simplex should power ON or OFF upon startup of the RHM.  Default is off
* ``LI,COMM,<CHAN>`` - **(RHM1 Only)** Sets the UART communications channel the Lithium radio is on upon startup of the RHM. CHAN can be in the set  {NONE, UART0, UART1, UART2, UART3, UART4, I2C}.  Default is NONE.
* ``LI,UART, <ENABLE | DISABLE>`` - **(RHM1 Only)** Sets if UART pass through for the Lithium should be ENABLEd or DISABLEd upon startup of the RHM.  Default is disabled.
* ``LI,POW, <ON | OFF>`` - **(RHM1 Only)** Sets if the Lithium radio should be ON or OFF upon startup of the RHM (Both PA and MCU).  Default is OFF.
* ``LI,PAPOW, <ON | OFF>`` - **(RHM1 Only)** Sets if the Lithium radio’s power-amplifier should be ON or OFF upon startup of the RHM.  Default is OFF.
* ``LI,MCUPOW,<ON | OFF>`` - **(RHM1 Only)** Sets if the Lithium radio’s microcontroller unit (MCU) should be ON or OFF upon startup of RHM. Default is OFF.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Set GlobalStar Simplex to be powered ON, and enable UART communications on CSK UART2 (BBB UART2) on startup.
Assuming serial number of 0::

    RHM:NVM UNLOCK,12345
    RHM:NVM GS,COMM,UART2
    RHM:NVM GS,UART,ENABLE
    RHM:NVM GS,POW,ON
    RHM:NVM WRITE,1

Set Lithium radio to be OFF, and enable UART Communications on CSK UART1 (BBB UART1) on startup.
Assuming serial number of 0::

    RHM:NVM UNLOCK,12345
    RHM:NVM LI,COMM,UART1
    RHM:NVM LI,POW,OFF
    RHM:NVM LI,UART,ENABLE
    RHM:NVM WRITE,1

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

None

.. _rhm-gs-send:

RHM:GS:SEND
==================================

Sends a message through the GlobalStar simplex up to 35 bytes.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    RHM:GS:SEND <hexstr>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Sends a ‘hex’ string (eg. ``50756D706B696E`` for Pumpkin) (up to a max of 70 hex characters) to the GlobalStar radio.
These requirements must be met before using this command:

#. The UART communication port must be set to I2C. (``RHM:GS:COMM I2C``).
#. Radio must be powered. (``RHM:GS:POW ON``)
#. UART pass through must be ENABLED. (``RHM:GS:PASS ON``).
#. GS Busy line must not be HIGH for more than 20 seconds AFTER the command is sent.
#. GS must NOT be transmitting currently.

This command will correctly wrap the packet for the GS Radio so wrapping does not need to be included.
This will increment the :ref:`GS Transmission Sent <tlm-rhm-gs-transmissions-sent>` if the GS Radio sends an ACK packet back, otherwise :ref:`GS Transmission Failed <tlm-rhm-gs-transmissions-failed>` is incremented.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Send “Hello World” through the GS radio::

    RHM:GS:POW ON
    RHM:GS:PASS ON
    RHM:GS:COMM I2C
    RHM:GS:SEND 48656c6c6f20576f726c64

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

None.

.. _rhm-telemetry:

RHM:TELemetry?
==================================

Requests a given telemetry item specific to the |RHM| unit.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    RHM:TELemetry? <N>,[DATA | ASCII | NAME | LENGTH]

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Request telemetry for a specific field N in the given format.
The formats are:

* ``DATA`` - Return telemetry in binary representation. Not available over user debug terminal.
* ``ASCII`` - Return telemetry in ASCII representation.
* ``NAME`` - The name of the telemetry item e.g. ``SupMCU Uptime``
* ``LENGTH`` - Length (in bytes) of telemetry in its binary representation.


Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Query RHM for GlobalStar UART Status in ASCII::

    RHM:TEL? 4,ASCII

Query RHM for name of 4th telemetry item::

    RHM:TEL? 3,NAME

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If no format representation is specified, ``DATA`` is used as the default.

See RHM Telemetry section for more detail.

Deprecated Commands
====================

Below is a listing of commands to be removed in a future revision of firmware:

.. csv-table:: Deprecated Commands
    :header: "Name","Reason"
    :widths: 30,70

    "RHM:GS:CONFigure", "Required commands not available on consumer models of Simplex radio."
    "RHM:WDT", "Features not available in latest revision."


.. highlight:: python

