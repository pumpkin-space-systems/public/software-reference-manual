************************
SCPI Commands
************************

.. highlight:: none

The |PIM| Command set is responsible for:

* Controlling the four assembly configurable power ports.
* Controlling the four port Ethernet switch on the |PIM|.
* Illuminating the Blue LED on the |PIM|.
* Querying the |PIM|’s Telemetry.
* Setting Non-volatile memory values specific to the |PIM|.

.. _pim-port-power:

PIM:PORT:POWer
===============================

Controls the state of the four power ports on the PIM.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    PIM:PORT:POWer <OFF | ON>,<N>

Description
^^^^^^^^^^^^^^^^^^^^^^^^

Power port ``<N>`` ``ON`` or ``OFF``. ``<N>`` can be in the range [1, 4].

Examples
^^^^^^^^^^^^^^^^^^^^^^^^

Power Port #2 on:

    PIM:PORT:POW ON,2

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^

None.

.. _pim-ethernet:

PIM:ETHernet
===============================

Controls the state of the ethernet switch on the PIM.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    PIM:ETHernet <ON | OFF | RESET | UNRESET>

Description
^^^^^^^^^^^^^^^^^^^^^^^^

Command the Ethernet switch to one of four different states:

* ``ON`` - The PIM Ethernet switch is ON
* ``OFF`` - The PIM Ethernet switch is OFF
* ``RESET`` - Pulls the PIM Ethernet -RESET Low. Must be restored with ``UNRESET`` subcommand.
* ``UNRESET`` - The PIM Ethernet switch RESET line is not driven high.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^

Turn off the PIM’s Ethernet::

    PIM:ETH OFF

Reset the PIM’s Ethernet switch::

    PIM:ETH RESET

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^

The RESET state is not turned off automatically, must be turned off with corresponding UNRESET command.

.. _pim-led:

PIM:LED
===============================

Controls the extra LEDs on the PIM module.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    PIM:LED <BLUE | RED>, <ON | OFF>

Description
^^^^^^^^^^^^^^^^^^^^^^^^

Command the BLUE or RED LED to turn ON or OFF

Examples
^^^^^^^^^^^^^^^^^^^^^^^^

Turn the RED LED ON::

    PIM:LED RED,ON

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^

None.

.. _pim-telemetry:

PIM:TELemetry?
===============================

Requests a given telemetry item specific to the |PIM| unit.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    PIM:TELemetry? <N>,[DATA | ASCII | NAME | LENGTH]

Description
^^^^^^^^^^^^^^^^^^^^^^^^

Request telemetry for a specific field N in the given format.
The formats are:

* ``DATA`` - Return telemetry in binary representation. Not available over user debug terminal.
* ``ASCII`` - Return telemetry in ASCII representation.
* ``NAME`` - The name of the telemetry item e.g. ``SupMCU Uptime``
* ``LENGTH`` - Length (in bytes) of telemetry in its binary representation.


Examples
^^^^^^^^^^^^^^^^^^^^^^^^

Query PIM port status in ASCII format::

    PIM:TEL? 5,ASCII

Query PIM channel voltages in binary format::

    PIM:TEL? 7

Query telemetry item #4’s length in bytes::

    PIM:TEL? 3,LENGTH

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^

If no format representation is specified, ``DATA`` is used as the default.

See PIM Telemetry section for more detail.

.. _pim-calibration-query:

PIM:CALibration?
===============================

Queries |PIM| specific calibration values.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    PIM:CALibration? LINEAR,<idx>,[ASCII | NAME | DATA]

Description
^^^^^^^^^^^^^^^^^^^^^^^^

Request calibration values for a specific calibration index `idx` in a given format.
The formats are:

* ``DATA`` - Return calibration values in binary format. Not available over user debug terminal.
* ``ASCII`` - Return the calibration values in ASCII representation.
* ``NAME`` - The name of the calibration index. e.g. CTMU

Examples
^^^^^^^^^^^^^^^^^^^^^^^^

Query ``SNS_CH1_I`` calibration values in ASCII::

    PIM:CAL? LINEAR, 0, ASCII

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^

The `LINEAR` argument is required.
This is for forward compatibility with future calibration value types (e.g. quadratic).

.. _pim-calibration:

PIM:CALibration
===============================

Sets |PIM| specific calibration values.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    PIM:CALibration LINEAR,<idx>,<OFFSET | SCALE_FACTOR>,<value>

Description
^^^^^^^^^^^^^^^^^^^^^^^^

Set calibration ``OFFSET`` or ``SCALE_FACTOR`` values for a specific calibration ``idx`` in the Supervisor MCU calibration values.
The calibration values are used internally when calculating the telemetry values for the user, applying a linear correction factor (E.g. ``final = scale_factor*initial + offset``).
Possible value sets are:

* ``OFFSET`` - Set the offset of the calibration factor. ``<value>`` is a number in the range [-32768, 32767]
* ``SCALE_FACTOR`` - Set the scale factor of the calibration factor. ``<value>`` is a floating-point number from [-16.0, 16.0].

Examples
^^^^^^^^^^^^^^^^^^^^^^^^

Set the SCALE_FACTOR to .975 and offset to -20 for the `SNS_CH1_I` calibration value, save to NVM::

    PIM:CAL LINEAR,0,SCALE_FACTOR,0.975
    PIM:CAL LINEAR,0,OFFSET,-20
    SUP:NVM UNLOCK,12345
    SUP:NVM WRITE,1

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^

The ``LINEAR`` argument is required.
This is for forward compatibility with future calibration value types (e.g. quadratic).
These values directly impact the output of telemetry from the SupMCU modules.
Calibration values must be saved via :ref:`supervisor-nvm` commands.

See PIM Calibration for more details.

.. _pim-nvm:

PIM:NVM
=====================================

Controls non-volatile memory parameters specific to the |PIM|.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    PIM:NVM <field,<PORT>,<VALUE> | UNLOCK,<key> | WRITE,<1>>

    Fields:
    SHUNT, I_LIMIT

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Controls access to and writes parameters to the non-volatile memory.
Order of operations when changing NVM is: UNLOCK, <change value>, WRITE.
Possible value combinations are:

* ``UNLOCK`` - unlocks the non-volatile memory (must be done first). Value is a key consisting of: ``device serial number + 12345``
* ``WRITE`` - Writes all set NVM values to non-volatile memory, value = 1
* ``SHUNT, PORT, VALUE`` - Sets the current shunt resistor value for the PORT given with the integer VALUE in micro-ohms.  Default is 5000 micro-ohms.
* ``I_LIMIT, PORT, VALUE`` - Sets the Current limit for the given PORT with the given integer VALUE in mA, Default is 2000 mA.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Set a current limit for PIM Port #1 & #4 to 2.5A (assuming PIM Serial Number is 0)::

    PIM:NVM UNLOCK,12345
    PIM:NVM I_LIMIT,1,2500
    PIM:NVM I_LIMIT,4,2500
    PIM:NVM WRITE,1

Set shunt for PIM port #3 to 6500 micro-ohms (assuming PIM serial number is 0)::

    PIM:NVM UNLOCK,12345
    PIM:NVM SHUNT,3,6500
    PIM:NVM WRITE,1

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``PIM:NVM UNLOCK`` and ``PIM:NVM WRITE`` provide the same functionality as ``SUP:NVM UNLOCK`` and ``SUP:NVM WRITE``.

It is recommended to leave the ``SHUNT`` values at default unless directed otherwise by Pumpkin.


.. highlight:: python