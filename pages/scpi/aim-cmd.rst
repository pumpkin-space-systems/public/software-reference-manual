***********************
SCPI Commands
***********************

.. highlight:: none

The |AIM| Command set is responsible for:

* Controlling power to the OEM719.
* Controlling the RESET line to the OEM719.
* Propagating orbit via Vinti7/SGP4 orbit propagators.
* Setting UART communication channel for OEM719.
* Enabling or disabling UART communication for the OEM719.
* Enabling a subset of the logs available for the OEM719.
* Controlling power to the MAI-400 ADCS
* Controlling the RESET line to the MAI-400 ADCS
* Setting UART communications channel for MAI-400 ADCS
* Enabling or disabling UART communications to/from MAI-400 ADCS
* Querying AIM-2 and GPSRM Telemetry.

.. note:: The AIM-2 Module supports ALL of the GPSRM commands except GPS:ADCS:POW. GPSRM commands can be prefixed with GPS:… or AIM:GPS:… (AIM:GPS:POW ON is equal to GPS:POW ON)
.. note:: See GPSRM SCPI Commands for GPSRM commands. All AIM-2 Specific commands are listed below.

.. _aim-gps-power:

AIM:GPS:POWer
==================================

Controls the electrical power of the OEM719 unit.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    AIM:GPS:POWer <OFF | ON>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turns ON or OFF power to the OEM719 or OEM719 module.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn on power to the OEM719::

    AIM:GPS:POW ON

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

None

.. _aim-gps-comm:

AIM:GPS:COMM
==================================

Sets the state of the UART port assigned to the OEM719 unit.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    AIM:GPS:COMM <NONE | UART0 | UART1 | UART2 | UART3 | SUP1 | SUP2>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Selects what CSK UART channel the OEM719 COM2 port should communicate on.
UART choices are:

* ``NONE`` - Disables UART communication to OEM719 COM2 port.
* ``UART0-UART3`` - Sets UART communication for OEM719 COM2 port to CSK UART 0, 1, 2 or 3. `See MBM2 Datasheet for mapping of CSK UART’s to BBB UART’s.`
* ``SUP1-SUP2`` - Sets UART communication for OEM719 to one of the two available internal SupMCU UART communication channels. `Ports only accessible by SupMCU.`

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Set OEM719 to communicate on CSK UART0 (BBB UART0) and enable communications::

    GPS:POW ON
    GPS:COMM UART0
    GPS:PASS ON

Set OEM719 to not communicate on any UART::

    GPS:COMM NONE
    GPS:PASS OFF

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Communication must still be passed through via a :ref:`GPS:PASS ON <aim-gps-passthrough>` command

.. _aim-gps-reset:

AIM:GPS:RESet
==================================

Controls the state of the RESET signal to the OEM719 unit.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    AIM:GPS:RESet <OFF | ON>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turns ON or OFF the RESET signal to the OEM719 or OEM719 module.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn on the GPS reset signal::

    AIM:GPS:RES ON

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The reset signal is not turned off automatically, the user will need to turn it off with a corresponding ``AIM:GPS:RESET OFF`` command.

.. _aim-gps-propagate:

AIM:GPS:PROPagate
==================================

Propagates the given orbital parameters using Vinti7 or SGP orbit propagators.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    AIM:GPS:PROPagate <VINTI7>, <t0 in seconds>,<t1 in seconds>, <Rx>, <Ry>, <Rz>, <Vx>, <Vy>, <Vz>
    or
    AIM:GPS:PROPagate <SGP4>, <dt in minutes>,<TLE>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Propagate orbit using SGP4 (S) or Vinti7 (V) orbit propagators.
The results of the orbit propagators can be seen via :ref:`AIM:GPS:TEL? 2 <tlm-aim-orbit-propagator-results>`.

``Vinti7`` fields are:

* ``t0`` - An initial time in seconds (usually 0).
* ``t1`` - Time of solution in seconds (time since t0).
* ``Rx, Ry, Rz`` - Position vector in kilometers.
* ``Vx, Vy, Vz`` - Velocity vector in kilometers.

While ``SGP4`` works using:

* ``dt`` - Time delta from time in TLE in minutes.
* ``TLE`` - Two-line element exactly 140 characters long. The lines in a TLE are separated by space instead of newline.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Propagate a spacecraft 120 minutes from now with a  positional vector of (-18,982 Km, 25,047 Km, -173 Km) and velocity vector (2.96 Km/s, 0.33 Km/s 0.27 Km/s)::

    AIM:GPS:PROP VINTI7, 0, 7200, -18982.0, -25047.0, -173.0, 2.96, 0.33, 0.27

Propagate TELSTAR 19V orbit 30 minutes from TLE time with SGP4::

    AIM:GPS:PROP SGP4,30,TELSTAR 19V 1 43562U 18059A   18228.55107419 -.00000304  00000-0  00000+0 0  9993 2 43562   0.0593 269.5551 0001312 309.6754 241.0290  1.00272513   473

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

TLE’s spacing is critical as the code parsing it expects elements to be at exact character locations.

TLE’s for current space vehicles can be found at `<https://www.celestrak.com/NORAD/elements/>`_

.. _aim-gps-led:

AIM:GPS:LED
==================================

Controls the state of the Status LED on the SupMCU module to match the state of the GPS.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    AIM:GPS:LED <SUP | PASS | GPS>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Controls the state of the SupMCU status light to match:

* ``SUP`` - Follows the state of the LED as given by the SUP:LED command
* ``PASS`` - The LED will remain ON if GPS pass through is on, otherwise OFF.
* ``GPS`` - The LED will remain ON if the GPS is ON otherwise OFF.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Make the status light follow the power state of the OEM719::

    AIM:GPS:LED GPS

Make the status light follow the pass through state of the OEM719::

    AIM:GPS:LED PASS

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

None.

.. _aim-gps-passthrough:

AIM:GPS:PASSthrough
==================================

Controls the state of the UART passthrough

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    AIM:GPS:PASSthrough <OFF | ON>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Controls the UART passthrough channel to the GPS receiver.

Use the :ref:`aim-gps-comm` command to control the CSK UART used for communication.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn OFF UART Passthrough::

    AIM:GPS:PASS OFF

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

None.

.. _aim-gps-log:

AIM:GPS:LOG
==================================

Commands the OEM719 to transmit logs via COM1/COM2 port on NovaTel unit.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    AIM:GPS:LOG  <OFF | GGA | GSV | GSA | RMC | VTG | ZDA | BESTPOSA | BESTPOSB | BESTXYZA | BESTXYZB>,[COMn]

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Commands the OEM719 to log a subset of the available data from the receiver.
An optional COMn (where n is 1 or 2) parameter can be given to have the GPS log to COM1 or COM2.
If not present, COM1 is chosen.
The log choices are:

* ``OFF/NONE`` - Turns off all logging from the OEM719 via NovaTel UNLOGALL command.
* ``GGA/GSV/GSA/RMC/VTG`` - Logs OEM719 data via “LOG COMn GP<type> ONTIME 1” NovaTel OEM command string (where type is log type given).
* ``ZDA/BESTPOSA/BESTPOSB/BESTXYZA/BESTXYZB`` - Logs OEM719 data via “LOG COMn <type> ONTIME 1” NovaTel OEM command string (where type is log type given).


Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn on logging BESTXYZA logs to COM2 (including powering GPS, enabling PASSthrough)::

    AIM:GPS:POW ON
    AIM:GPS:PASS ON
    AIM:GPS:LOG BESTXYZA,COM2

Turn on logging GPGGA string to COM1::

    AIM:GPS:LOG GGA


Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

More logs/command are available via the UART passthrough and commanding the OEM unit directly.
For information about OEM commands see `here <https://docs.novatel.com/OEM7/Content/PDFs/OEM7_Commands_Logs_Manual.pdf>`_

.. _aim-gps-bestpos:

AIM:GPS:BESTPOS
==================================

Logs the ``BESTPOSA`` or ``BESTPOSB`` log from the NovaTel OEM719 over COM2.
**This command is deprecated in favor of the** :ref:`AIM:GPS:LOG <aim-gps-log>` **SCPI Command**.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    AIM:GPS:BESTPOS <OFF | BESTPOSA | BESTXYZA>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Commands the OEM719 to log a subset of the available data from the receiver to COM2 port of the OEM719 unit. The log choices are:

* ``OFF`` - Turns off all logging from the OEM719 via NovaTel UNLOGALL command.
* ``BESTPOSA`` - Logs BESTPOS logs in ASCII format via ``LOG COM2 BESTPOSA ONTIME 1`` command string.
* ``BESTPOSB`` - Logs BESTPOS logs in binary format via ``LOG COM2 BESTPOSB ONTIME 1`` command string.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn on BESTPOSA logging to COM2 port of OEM719::

    AIM:GPS:BESTPOS BESTPOSA

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Command is deprecated in favor of :ref:`AIM:GPS:LOG <aim-gps-log>` command.

.. _aim-adcs-comm:

AIM:ADCS:COMM
==================================

Controls the state of the UART hardware mux for the MAI-400 communication.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    AIM:ADCS:COMM <NONE | UART0 | UART1 | UART2 | UART3 | SUP1 | SUP2>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Selects what CSK UART channel the MAI-400 should communicate on.
UART choices are:

* ``NONE`` - Disables UART communication to MAI-400.
* ``UART0-UART3`` - Sets UART communication for MAI-400 to CSK UART 0, 1, 2 or 3. `See MBM2 Datasheet for mapping of CSK UART’s to BBB UART’s.`
* ``SUP1-SUP2`` - Sets UART communication for MAI-400 to one of the two available internal SupMCU UART communication channels. `Ports only accessible by SupMCU.`

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Set MAI-400 to communicate on CSK UART2 (BBB UART 2) and enable communications::

    AIM:ADCS:POW ON
    AIM:ADCS:COMM UART2
    AIM:ADCS:PASS ON

Set MAI-400 to not communicate on any UART::

    AIM:ADCS:COMM NONE
    AIM:ADCS:PASS OFF

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Communication must still be passed through via a :ref:`AIM:ADCS:PASS ON <aim-adcs-passthrough>` command.

.. _aim-adcs-passthrough:

AIM:ADCS:PASS
==================================

Controls the state of the UART passthrough for the MAI-400 unit.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    AIM:ADCS:PASS <ON | OFF>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn the UART connection to the MAI-400 ADCS ON or OFF.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn on UART passthrough to MAI-400::

    AIM:ADCS:PASS ON

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Use the :ref:`aim-adcs-comm` command to set the CSK UART to communicate with the MAI-400 on.

.. _aim-adcs-power:

AIM:ADCS:POWer
==================================

Controls the state of the electrical power to MAI-400

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    AIM:ADCS:POWer <ON | OFF>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turns power to the MAI-400 ADCS ON or OFF

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn off the MAI-400::

    AIM:ADCS:POW OFF

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

None.

.. _aim-adcs-reset:

AIM:ADCS:RESet
==================================

Controls the RESET signal to the MAI-400

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    AIM:ADCS:RESET <ON | OFF>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turns the RESET signal to the MAI-400 ADCS ON or OFF (assert or revoke).

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn on the RESET signal for the MAI-400::

    AIM:ADCS:RESET ON

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The RESET signal must be turned off with a corresponding ``AIM:ADCS:RESET OFF`` command.
It is not turned off automatically

.. _aim-telemetry:

AIM:TELemetry?
==================================

Requests a given telemetry item specific to the |AIM| unit.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    AIM:TELemetry? <N>,[DATA | ASCII | NAME | LENGTH]

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Request telemetry for a specific field N in the given format.
The formats are:

* ``DATA`` - Return telemetry in binary representation. Not available over user debug terminal.
* ``ASCII`` - Return telemetry in ASCII representation.
* ``NAME`` - The name of the telemetry item e.g. ``SupMCU Uptime``
* ``LENGTH`` - Length (in bytes) of telemetry in its binary representation.


Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Query OEM719 Power status in ASCII representation::

    AIM:TEL? 3,ASCII

Query MAI-400 ADCS status in binary representation::

    AIM:TEL? 9

Query ADCS UART Status binary representation byte length::

    AIM:TEL? 7,LENGTH

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If no format representation is specified, ``DATA`` is used as the default.

See AIM Telemetry section for more detail.

.. _aim-calibration-query:

AIM:CALibration?
==================================

Queries |AIM| specific calibration values.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    AIM:CALibration? LINEAR,<idx>,[ASCII | NAME | DATA]

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Request calibration values for a specific calibration index `idx` in a given format.
The formats are:

* ``DATA`` - Return calibration values in binary format. Not available over user debug terminal.
* ``ASCII`` - Return the calibration values in ASCII representation.
* ``NAME`` - The name of the calibration index. e.g. CTMU

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Query name of idx 0 calibration value::

    AIM:CAL? LINEAR, 0, NAME

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The `LINEAR` argument is required.
This is for forward compatibility with future calibration value types (e.g. quadratic).

.. _aim-calibration:

AIM:CALibration
==================================

Sets |AIM| specific calibration values.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    AIM:CALibration LINEAR,<idx>,<OFFSET | SCALE_FACTOR>,<value>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Set calibration ``OFFSET`` or ``SCALE_FACTOR`` values for a specific calibration ``idx`` in the Supervisor MCU calibration values.
The calibration values are used internally when calculating the telemetry values for the user, applying a linear correction factor (E.g. ``final = scale_factor*initial + offset``).
Possible value sets are:

* ``OFFSET`` - Set the offset of the calibration factor. ``<value>`` is a number in the range [-32768, 32767]
* ``SCALE_FACTOR`` - Set the scale factor of the calibration factor. ``<value>`` is a floating-point number from [-16.0, 16.0].

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Set the SCALE_FACTOR to .975 and offset to -20 for the `VCC_SYS` calibration value, save to NVM::

    AIM:GPS:CAL LINEAR,0,SCALE_FACTOR,0.975
    AIM:GPS:CAL LINEAR,0,OFFSET,-20
    SUP:NVM UNLOCK,12345
    SUP:NVM WRITE,1

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``LINEAR`` argument is required.
This is for forward compatibility with future calibration value types (e.g. quadratic).
These values directly impact the output of telemetry from the SupMCU modules.
Calibration values must be saved via :ref:`SUP:NVM <supervisor-nvm>` commands.

See AIM Calibration for more details.

.. _aim-nvm:

AIM:NVM
==================================

Controls the non-volatile parameters specific to the |AIM| module.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    NVM control commands
    AIM:NVM <UNLOCK, <key> | WRITE, <1>>

    NVM values for OEM719
    AIM:NVM <GPS>,<COMM,  <CHAN>  | <POW>, <ON | OFF>>

    NVM values for MAI-400
    AIM:NVM <ADCS>, <COMM, <CHAN> | <POW>, <ON | OFF> | <UART>, <ENABLE | DISABLE>>


Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Controls access to and writes parameters to the non-volatile memory.
Order of operations when changing NVM is: UNLOCK, <change value>, WRITE.
Possible value combinations are:

* ``UNLOCK`` - Unlocks the non-volatile memory (must be done first). Value is a key consisting of: ``device serial number + 12345``
* ``WRITE`` - Writes all set NVM values to non-volatile memory, value = 1
* ``GPS,COMM,<CHAN>`` - Sets the UART communications channel the OEM719 is on upon startup of the AIM-2. CHAN can be in the set {NONE, UART0, UART1, UART2, UART3, SUP1, SUP2}.  Default is NONE.
* ``GPS,POW,<ON | OFF>`` - Sets if the OEM719 should power ON or OFF upon startup of the AIM-2.  Default is OFF.
* ``ADCS,COMM,<CHAN>`` - Sets the UART communications channel the MAI-400 is on upon startup of the AIM-2. CHAN can be in the set  {NONE, UART0, UART1, UART2, UART3, SUP1, SUP2}.  Default is NONE.
* ``ADCS,UART, <ENABLE | DISABLE>`` - Sets if UART pass through for the MAI-400 should be ENABLEd or DISABLEd upon startup of the AIM-2.  Default is Disabled.
* ``ADCS,POW, <ON | OFF>`` - Sets if the MAI-400 should be ON or OFF upon startup of the AIM-2.  Default is OFF.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Set the OEM719 to communicate on UART1 and MAI-400 to communicate on UART2 upon startup of the AIM-2 (assuming serial number of AIM-2 is 100)::

    AIM:NVM UNLOCK,12445
    AIM:NVM GPS,COMM,UART1
    AIM:NVM ADCS,COMM,UART2
    AIM:NVM WRITE,1

Set the OEM719 and MAI-400 to power ON upon startup of AIM-2 (assuming serial number of AIM-2 is 0)::

    AIM:NVM UNLOCK,12345
    AIM:NVM GPS,POW,ON
    AIM:NVM ADCS,POW,ON
    AIM:NVM WRITE,1

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

There is NO NVM value for setting startup state of OEM719 UART pass through, it must be set explicitly ON or OFF after start with ``GPS:PASS ON`` or ``GPS:PASS OFF``.

Deprecated Commands
===================

Below is a listing of commands to be removed in a future revision of the firmware:

.. csv-table:: Deprecated Commands
    :header: "Name", "Reason"
    :widths: 30, 70

    "AIM:WDT", "Functionality not supported on current revision."

.. highlight:: python
