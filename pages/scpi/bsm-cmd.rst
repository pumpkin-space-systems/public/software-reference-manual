******************************
SCPI Commands
******************************

.. highlight:: none

The |BSM| Command set is responsible for:

* Powering each of the five BSM ports ON or OFF
* Querying telemetry for each of the five |BSM| ports.
* Setting Non-volatile memory specific to the |BSM|.

.. _bsm-port-power:

BSM:PORT:POWer
=====================================

Controls the state of the power ports on the |BSM| module.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    BSM:PORT:POWer <OFF | ON>, <N>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Power the given BSM port N ON or OFF.
``<N>`` is a value between [1,4] corresponding to the BSM port.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Power on BSM ports 1 & 2::

    BSM:PORT:POW ON,1
    BSM:PORT:POW ON,2

Power off BSM Port 3::

    BSM:PORT:POW OFF,3

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If fault occurs on port, then the numbered port must be explicitly turned off then on

.. _bsm-nvm:

BSM:NVM
=====================================

Controls non-volatile memory parameters specific to the |BSM|.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    BSM:NVM <I_LIMIT,<PORT>,<VALUE> | UNLOCK,<key> | WRITE,<1>>


Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Controls access to and writes parameters to the non-volatile memory.
Order of operations when changing NVM is: UNLOCK, <change value>, WRITE.
Possible value combinations are:

* ``UNLOCK`` - unlocks the non-volatile memory (must be done first). Value is a key consisting of: ``device serial number + 12345``
* ``WRITE`` - Writes all set NVM values to non-volatile memory, value = 1
* ``I_LIMIT, PORT, VALUE`` - Sets the Current limit for the given PORT with the given integer VALUE in mA, Default is 2000 Milliamps

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Set a current limit for BSM Port #1 & #4 to 2.5A (assuming BSM Serial Number is 0)::

    BSM:NVM UNLOCK,12345
    BSM:NVM I_LIMIT,1,2500
    BSM:NVM I_LIMIT,4,2500
    BSM:NVM WRITE,1

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``BIM:NVM UNLOCK`` and ``BIM:NVM WRITE`` provide the same functionality as ``SUP:NVM UNLOCK`` and ``SUP:NVM WRITE``.

.. _bsm-telemetry:

BSM:TELemetry?
=====================================

Requests a given telemetry item specific to the |BSM| unit.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    BSM:TELemetry? <N>,[DATA | ASCII | NAME | LENGTH]

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Request telemetry for a specific field N in the given format.
The formats are:

* ``DATA`` - Return telemetry in binary representation. Not available over user debug terminal.
* ``ASCII`` - Return telemetry in ASCII representation.
* ``NAME`` - The name of the telemetry item e.g. ``SupMCU Uptime``
* ``LENGTH`` - Length (in bytes) of telemetry in its binary representation.


Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Query BSM current telemetry in ASCII format::

    BSM:TEL? 0,ASCII

Query BSM port status in binary representation::

    BSM:TEL? 3,DATA

Query the name of the 2nd BSM telemetry item::

    BSM:TEL? 1,NAME

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If no format representation is specified, ``DATA`` is used as the default.

See BSM Telemetry section for more detail.

.. _bsm-calibration-query:

BSM:CALibration?
=====================================

Queries |BSM| specific calibration values.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    BSM:CALibration? LINEAR,<idx>,[ASCII | NAME | DATA]

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Request calibration values for a specific calibration index `idx` in a given format.
The formats are:

* ``DATA`` - Return calibration values in binary format. Not available over user debug terminal.
* ``ASCII`` - Return the calibration values in ASCII representation.
* ``NAME`` - The name of the calibration index. e.g. CTMU

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Query name of idx 0 calibration value::

    BSM:CAL? LINEAR, 0, NAME

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``LINEAR`` argument is required.
This is for forward compatibility with future calibration value types (e.g. quadratic).

.. _bsm-calibration:

BSM:CALibration
=====================================

Sets |BSM| specific calibration values.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    BSM:CALibration LINEAR,<idx>,<OFFSET | SCALE_FACTOR>,<value>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Set calibration ``OFFSET`` or ``SCALE_FACTOR`` values for a specific calibration ``idx`` in the Supervisor MCU calibration values.
The calibration values are used internally when calculating the telemetry values for the user, applying a linear correction factor (E.g. ``final = scale_factor*initial + offset``).
Possible value sets are:

* ``OFFSET`` - Set the offset of the calibration factor. ``<value>`` is a number in the range [-32768, 32767]
* ``SCALE_FACTOR`` - Set the scale factor of the calibration factor. ``<value>`` is a floating-point number from [-16.0, 16.0].

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Set the SCALE_FACTOR to .975 and offset to -20 for the `Port 1 Current` calibration value, save to NVM::

    BSM:CAL LINEAR,0,SCALE_FACTOR,0.975
    BSM:CAL LINEAR,0,OFFSET,-20
    SUP:NVM UNLOCK,12345
    SUP:NVM WRITE,1

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``LINEAR`` argument is required.
This is for forward compatibility with future calibration value types (e.g. quadratic).
These values directly impact the output of telemetry from the SupMCU modules.
Calibration values must be saved via :ref:`SUP:NVM <supervisor-nvm>` commands.

See BSM Calibration for more details.

.. highlight:: python