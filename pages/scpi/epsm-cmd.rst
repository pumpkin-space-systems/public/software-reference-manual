************************
SCPI Commands
************************

.. highlight:: none

The EPSM Command set is responsible for:

* Turning on / off 3.3v, 5v, 12v and AUX converters.
* Controlling charging voltage and current to batteries.
* Turning on/off battery converters.
* Turning on/off SAI1-6 converters.

.. _epsm-bus:

EPSM:BUS
===============================

Controls the state of the voltage buses on the EPSM modules.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:BUS <3V3, 5V, 12V, AUX, VBATT>, <ON | OFF>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^

Turns the given EPSM bus ON or OFF:

* ``3V3, <ON | OFF>`` - Turns the 3.3v bus ON or OFF.
* ``5V, <ON | OFF>`` - Turns the 5v bus ON or OFF.
* ``12V, <ON | OFF>`` - Turns the 12v bus ON or OFF.
* ``AUX, <ON | OFF>`` - Turns the AUX bus ON or OFF.
* ``<BAT1 | BAT2>, <ON | OFF>`` - Turns the BAT1 or BAT2 bus ON or OFF

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn the 3.3v and 5v buses off::

    EPSM:BUS 3V3,OFF
    EPSM:BUS 5V,OFF

Turn the AUX bus on::

    EPSM:BUS AUX,ON

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^

Buses will be turned on when the SupMCU boots. For at a minimum of 5 seconds, the EPSM will provide no power to ``12V/5V/3.3V/AUX``.

.. _epsm-sai:

EPSM:SAI
===============================

Controls the state of the SAI ports on the EPSM.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:SAI <saiNum>, <ON | OFF>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn input ON or OFF from one of the six SAIs on the EPSM.


Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn off input from SAI6 and turn on input from SAI1::

    EPSM:SAI 6,OFF
    EPSM:SAI 1,ON

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^

All SAI's 1-6 start off enabled.

.. _epsm-battery:

EPSM:BATtery
===============================

Controls the enable state of the battery ports on the EPSM.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:BATtery <batNum>,<ON | OFF>
    EPSM:BATtery <batNum>,<I_MAX_CHG>,<mA>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^

Controls the BAT1 and BAT2 ports on the EPSM, turning them ON or OFF, or setting charging current limits on the ports.
Possible value combinations are:

* ``<batNum>, <ON | OFF>`` -  Turns ON or OFF the given battery port. ``<batNum>`` is either 1 or 2.
* ``<batNum>, <I_MAX_CHG>, <mA>`` - Sets the maximum charge current limit on the given battery port.


Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^

Turn off output and input from BATT1 port::

    EPSM:BAT 1,OFF

Set the current charging limit to ``200 mA`` on BAT #2 ::

    EPSM:BAT 2,I_MAX_CHG,200

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``I_MAX_CHG`` limit set in the ``EPSM:BAT`` command does not persist across reboot of the EPSM.

THe ``<I_MAX_CHG>`` parameter can be persisted in NVM via :ref:`epsm-nvm` command.

.. _epsm-debug:

EPSM:DEBug
===============================

Enables or disables debug information specific to the EPSM/DCPS output via the Debug UART port.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:DEBug <ENable | DISable>,<mask>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^


Enables or disables debug information being printed out over the UART Debug port on the EPSM.
Mask is the bits of the debug information to enable.
Possible value combinations are:

* ``ENable, <mask>`` - Enables the bits represented by the mask value. Mask is a bit field and bits that are 1 will enable debug information.
* ``DISable, <mask>`` - Disables the bits represented by the mask value. Mask is a bit field and bits that are 1 will disable debug information.

There are 9 bits used for debug information on the EPSM:

* ``0x0001`` - Print out raw bytes read from FPGA while commanding the EPSM.
* ``0x0002`` - Show time between successive reads of FPGA ADCs.
* ``0x0004`` - Show FPGA Frequency.
* ``0x0008`` - Show UART receive buffers.
* ``0x0010`` - Show FPGA Temperatures
* ``0x0020`` - Show FPGA Block (SAI1-6, 12V, 5V, 3V3, AUX, BAT1 & 2) data.
* ``0x0040`` - Show FPGA Voltage reference and dosimeter data.
* ``0x0080`` - Show SupMCU ADC reads.
* ``0x0100`` - Test control of inhibits on EPSM.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^

Enable all debug information on EPSM, except FPGA reads and ADC timing::

    EPSM:DEB EN,0x00FC

Disable debug information on SAI converters::

    EPSM:DEB DIS,0x0020

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^

Debug flags are disabled

.. _epsm-fpga:

EPSM:FPGA
===============================

Sub-commands to interact with the FPGA. Currently there are:

* UNLOCK -- Unlocks the FPGA subsystem to allow sending commands directly to the FPGA. Must be sent ``5`` times to unlock the device.
* INIT -- Re-runs the FPGA initialization routine to restart FPGA
* FLASH -- Puts the FPGA in a state to allow updating the FPGA Bitstream
* ON/OFF -- Turns ON or OFF the FPGA subsystem
* READ -- Reads a value from a given memory address in the FPGA
* WRITE -- Writes a value to one of the allowed registers on the FPGA

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:FPGA READ,<addr>,<format>
    EPSM:FPGA WRITE,<addr>,<format>,<value>
    EPSM:FPGA <UNLOCK | INIT | ON | OFF | FLASH>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^

Interacts with the FPGA on the EPSM directly. Allows the user to read/write/flash/turn on/off FPGA.

Before any command is sent to the FPGA, the FPGA **must** be unlocked via 5x ``EPSM:FPGA UNLOCK`` SCPI commands.

For READ and WRITE subcommand, format specifiers are:

* ``T`` – One byte, print out as int8_t.
* ``U`` – One byte, print out as uint8_t.
* ``X`` – One byte, print out hexadecimal value.
* ``S`` – Two bytes, print out as int16_t.
* ``D`` – Two bytes, print out as uint16_t.
* ``XX`` – Two bytes, print out as hexadecmial value.

Note the value is also stored as `Last register Read` in :ref:`EPS:TEL? 40 <tlm-epsm-last-register-read>`.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^

Print out two bytes starting at 0x20fe in hexadecimal format::

    EPSM:FPGA UNLOCK
    EPSM:FPGA UNLOCK
    EPSM:FPGA UNLOCK
    EPSM:FPGA UNLOCK
    EPSM:FPGA UNLOCK
    EPSM:FPGA READ,0x20fe,XX

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^

The list of available registers on the EPSM:

.. csv-table::
    :header: "Name", "Address", "Is Writable"
    :widths: 40,40,20

    "FPGA_CHIP_ID", "0x0000", "No"
    "FPGA_CHIP_VER", "0x0002", "No"
    "FPGA_SCRATCH", "0x0004", "Yes"
    "FPGA_SYSCON0", "0x0006", "Yes"
    "FPGA_SYSCON1", "0x0008", "Yes"
    "FPGA_SYSCON2", "0x000A", "Yes"
    "FPGA_SYSCON3", "0x000C", "Yes"
    "FPGA_LOOP_CNT_HI", "0x0010", "No"
    "FPGA_LOOP_CNT_LO", "0x0012", "No"
    "FPGA_PWM_BAT2", "0x1000", "No"
    "FPGA_PWM_SAI1", "0x1002", "No"
    "FPGA_PWM_SAI2", "0x1004", "No"
    "FPGA_PWM_BAT1", "0x1006", "No"
    "FPGA_PWM_SAI5", "0x1008", "No"
    "FPGA_PWM_SAI6", "0x100A", "No"
    "FPGA_PWM_AUX", "0x100C", "No"
    "FPGA_PWM_SAI3", "0x100E", "No"
    "FPGA_PWM_SAI4", "0x1010", "No"
    "FPGA_PWM_12V", "0x1012", "No"
    "FPGA_PWM_5V", "0x1014", "No"
    "FPGA_PWM_3V3", "0x1016", "No"
    "FPGA_SAI1_VSET", "0x1800", "No"
    "FPGA_SAI2_VSET", "0x1802", "No"
    "FPGA_SAI3_VSET", "0x1804", "No"
    "FPGA_SAI4_VSET", "0x1806", "No"
    "FPGA_SAI5_VSET", "0x1808", "No"
    "FPGA_SAI6_VSET", "0x180A", "No"
    "FPGA_BAT1_VSET", "0x180C", "No"
    "FPGA_BAT2_VSET", "0x180E", "No"
    "FPGA_AUX_VSET", "0x1810", "Yes"
    "FPGA_12V_VSET", "0x1812", "No"
    "FPGA_5V_VSET", "0x1814", "No"
    "FPGA_3V3_VSET", "0x1816", "No"
    "FPGA_AUX_IMAX", "0x1830", "Yes"
    "FPGA_IMAX_12V", "0x1832", "No"
    "FPGA_IMAX_5V", "0x1834", "No"
    "FPGA_IMAX_3V3", "0x1836", "No"
    "FPGA_SAI1_IMAX", "0x1840", "Yes"
    "FPGA_SAI2_IMAX", "0x1848", "Yes"
    "FPGA_SAI3_IMAX", "0x1850", "Yes"
    "FPGA_SAI4_IMAX", "0x1858", "Yes"
    "FPGA_SAI5_IMAX", "0x1860", "Yes"
    "FPGA_SAI6_IMAX", "0x1868", "Yes"
    "FPGA_BAT1_VLIMIT", "0x1882", "Yes"
    "FPGA_BAT1_CHG_IMAX", "0x1884", "Yes"
    "FPGA_BAT1_DSG_IMAX", "0x1886", "Yes"
    "FPGA_BAT2_VLIMIT", "0x188A", "Yes"
    "FPGA_BAT2_CHG_IMAX", "0x188C", "Yes"
    "FPGA_BAT2_DSG_IMAX", "0x188E", "Yes"

.. _epsm-nvm:

EPSM:NVM
===============================

Controls NVM values specific to the |EPSM| module.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:NVM UNLOCK,<key>
    EPSM:NVM WRITE,1
    EPSM:NVM <BAT1 | BAT2>,<V_CHG_MAX | I_CHG_LIMIT | I_DSG_LIMIT>,<value>
    EPSM:NVM AUX,I_LIMIT,<mA>
    EPSM:NVM DEBUG,<mask>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^

Controls access to and writes parameters to the non-volatile memory.
Order of operations when changing NVM is: UNLOCK, <change value>, WRITE.
Possible value combinations are:

* ``UNLOCK, <key>`` - unlocks the non-volatile memory (must be done first). key is a value consisting of: ``device serial number + 12345``
* ``WRITE, <value>`` - Writes all set NVM values to non-volatile memory, value = 1.
* ``BAT1 | BAT2, V_CHG_MAX | I_CHG_LIMIT | I_DSG_LIMIT, <value>`` - Sets the maximum charge voltage (V_CHG_MAX) or maximum charge current (I_CHG_LIMIT) or maximum discharge current (I_DSG_LIMIT) of BAT1 or BAT2 ports.
* ``AUX,I_LIMIT,<mA>`` - Sets the current limit on the AUX bus to mA. The upper limit of mA is 8000 mA (8A).
* ``AUX,V_SET, <mV>`` - Sets the AUX bus to the given set-point voltage.
* ``WDT,<seconds>`` - Sets the period (in seconds) of the WDT before restarting the system. WDT kicked via ``EPSM:WDT KICK`` command.
* ``DEBUG, <mask>`` - Sets the debug mask to be set on boot. The current debug mask is also set to the mask value passed. See :ref:`epsm-debug` command to see list of masks.
* ``<OT_MAX | OT2_MAX>,<mC>`` - Sets the OT1 (OT_MAX) or OT2 (OT2_MAX) overtemperature conditions. See remarks below for usage.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^

Set a charging current limit for BAT1 to 200 mA, discharge limit for BAT1 to 6A and charging voltage for BAT1 to 16.8V (default)::

    EPSM:NVM UNLOCK,12345
    EPSM:NVM BAT1,I_CHG_LIMIT,200
    EPSM:NVM BAT1,I_DSG_LIMIT,6000
    EPSM:NVM BAT1,V_CHG_MAX,16800
    EPSM:NVM WRITE,1

Set debug mask to show SAI state upon boot and now::

    EPSM:NVM UNLOCK,12345
    EPSM:NVM DEBUG,0x0020
    EPSM:NVM WRITE,1

Set the current limit on the AUX bus to 4A::

    EPSM:NVM UNLOCK,12345
    EPSM:NVM AUX,I_LIMIT,4000
    EPSM:NVM WRITE,1

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^

The OT1 limit corresponds to when the EPSM will **turn off** the **AUX** rail forcefully to shed thermal load from the system.

The OT2 limit corresponds to when the EPSM will enter a **survival** mode and restart the FPGA to shed thermal load by restarting the system.

.. _epsm-telemetry:

EPSM:TELemetry?
===============================

Requests a given telemetry item specific to the |EPSM| unit.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TELemetry? <N>,[DATA | ASCII | NAME | LENGTH]

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^

Request telemetry for a specific field N in the given format.
The formats are:

* ``DATA`` - Return telemetry in binary representation. Not available over user debug terminal.
* ``ASCII`` - Return telemetry in ASCII representation.
* ``NAME`` - The name of the telemetry item e.g. ``SupMCU Uptime``
* ``LENGTH`` - Length (in bytes) of telemetry in its binary representation.


Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^

Query EPSM SAI1 Converter data in ASCII::

    EPSM:TEL? 0,ASCII

Query EPSM 3.3V Converter in binary representation::

    EPSM:TEL? 6,DATA

Query the name of the 2nd EPSM telemetry item::

    EPSM:TEL? 1,NAME

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^

If no format representation is specified, ``DATA`` is used as the default.

See EPSM Telemetry section for more detail.

.. _epsm-calibration-query:

EPSM:CALibration?
===============================

Queries |EPSM| specific calibration values.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:CALibration? LINEAR,<idx>,[ASCII | NAME | DATA]

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^

Request calibration values for a specific calibration index `idx` in a given format.
The formats are:

* ``DATA`` - Return calibration values in binary format. Not available over user debug terminal.
* ``ASCII`` - Return the calibration values in ASCII representation.
* ``NAME`` - The name of the calibration index. e.g. CTMU

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^

Query ``SAI1_IO`` calibration values in ASCII::

    EPSM:CAL? LINEAR, 0, ASCII

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``LINEAR`` argument is required.
This is for forward compatibility with future calibration value types (e.g. quadratic).

.. _epsm-calibration:

EPSM:CALibration
===============================

Sets |EPSM| specific calibration values.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:CALibration LINEAR,<idx>,<OFFSET | SCALE_FACTOR>,<value>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^

Set calibration ``OFFSET`` or ``SCALE_FACTOR`` values for a specific calibration ``idx`` in the Supervisor MCU calibration values.
The calibration values are used internally when calculating the telemetry values for the user, applying a linear correction factor (E.g. ``final = scale_factor*initial + offset``).
Possible value sets are:

* ``OFFSET`` - Set the offset of the calibration factor. ``<value>`` is a number in the range [-32768, 32767]
* ``SCALE_FACTOR`` - Set the scale factor of the calibration factor. ``<value>`` is a floating-point number from [-16.0, 16.0].

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^

Set the SCALE_FACTOR to .975 and offset to -20 for the ``SAI1_IO`` calibration value, save to NVM::

    EPSM:CAL LINEAR,0,SCALE_FACTOR,0.975
    EPSM:CAL LINEAR,0,OFFSET,-20
    SUP:NVM UNLOCK,12345
    SUP:NVM WRITE,1

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``LINEAR`` argument is required.
This is for forward compatibility with future calibration value types (e.g. quadratic).
These values directly impact the output of telemetry from the SupMCU modules.
Calibration values must be saved via :ref:`supervisor-nvm` commands.

See EPSM Calibration for more details.

.. highlight:: python