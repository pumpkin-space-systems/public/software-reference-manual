************************
SCPI Commands
************************

.. highlight:: none

The LEPS Command set is responsible for:

* Controlling the enable state of battery ports on the LEPS
* Enabling and disabling debug information output
* Querying LEPS telemetry items

.. _leps-battery:

LEPS:BATtery
===============================

Controls the fast charging state of the battery ports on the LEPS.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    LEPS:BATtery FAST

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^

Toggles between fast and slow charging for the battery ports on the LEPS.


Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^

Toggle fast charging for the LEPS batteries::

    LEPS:BAT FAST

.. _leps-debug:

LEPS:DEBug
===============================

Enables or disables debug information specific to the LEPS/DCPS output via the Debug UART port.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    LEPS:DEBug <ENable | DISable>,<mask>

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^


Enables or disables debug information being printed out over the UART Debug port on the LEPS.
Mask is the bits of the debug information to enable.
Possible value combinations are:

* ``ENable, <mask>`` - Enables the bits represented by the mask value. Mask is a bit field and bits that are 1 will enable debug information.
* ``DISable, <mask>`` - Disables the bits represented by the mask value. Mask is a bit field and bits that are 1 will disable debug information.

There are 5 bits used for debug information on the LEPS:

* ``0x0001`` - Show system voltages.
* ``0x0002`` - Show battery status.
* ``0x0004`` - Show raw ADC values read.
* ``0x0008`` - Show external ADC values read.
* ``0x0010`` - Show raw SPI transactions from SPI bus.

Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^

Enable all debug information on LEPS, except system voltages and battery status::

    LEPS:DEB EN,0x001C

Disable debug information on battery status::

    LEPS:DEB DIS,0x0002

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^

Debug flags are disabled

LEPS:TELemetry?
===============================

Requests a given telemetry item specific to the |LEPS| unit.

Syntax
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    LEPS:TELemetry? <N>,[DATA | ASCII | NAME | LENGTH]

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^

Request telemetry for a specific field N in the given format.
The formats are:

* ``DATA`` - Return telemetry in binary representation. Not available over user debug terminal.
* ``ASCII`` - Return telemetry in ASCII representation.
* ``NAME`` - The name of the telemetry item e.g. ``SupMCU Uptime``
* ``LENGTH`` - Length (in bytes) of telemetry in its binary representation.


Examples
^^^^^^^^^^^^^^^^^^^^^^^^^^

Query LEPS 3.3V Converter data in ASCII::

    LEPS:TEL? 8,ASCII

Query LEPS 3.3V Converter in binary representation::

    LEPS:TEL? 8,DATA

Query the name of the 10th LEPS telemetry item::

    LEPS:TEL? 9,NAME

Remarks
^^^^^^^^^^^^^^^^^^^^^^^^^^

If no format representation is specified, ``DATA`` is used as the default.

See LEPS Telemetry section for more detail.
