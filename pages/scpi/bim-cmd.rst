************************
SCPI Commands
************************

The |BIM| command set is responsible for:

* Controlling the TiNi Pin Puller for solar panel deployment.
* Switching the three UART ports on the top of the |BIM|.
* Switching the |BIM| Ethernet and EEProm daughter board (if |BIM| is equipped with daughter board.)
* Powering the IMU on the |BIM| (if |BIM| is equipped with IMU.)
* Powering the temperature sensors on the |BIM|.
* Querying |BIM| specific telemetry.
* Setting Non-volatile memory values specific to the |BIM|.

.. _bim-tini:

BIM:TINI
====================

Controls the TiNi pin-puller mechanism.

Syntax
^^^^^^^^^^^^^^^^

.. code-block::

    BIM:TINI <DISAble | ENABle | UNARm | ARM | FIRE, <N>>

Description
^^^^^^^^^^^^^^^^

Controls the TiNi Pin Puller.
When energizing the pin puller, the order of operations is ENABLE → ARM → FIRE, N.
The different parameter strings mean:

* ``DISAble`` - Disables the TiNi pin puller, effectively resetting the sequence.
* ``ENABle`` - Enables the TiNi pin puller, first step in panel deployment.
* ``ARM`` - Arms the pin puller, second step in panel deployment.
* ``UNARm`` - Disables the SupMCU task responsible for energizing the Pin Puller. Does not disable the TiNi.
* ``FIRE``, ``<N>`` - Energizes the Pin Puller, releasing the deployment mechanism of the solar panels. Will keep energized for N seconds where N is in the range [1, 30].


Examples
^^^^^^^^^^^^^^^^

Energize the TiNi pin puller for 25 seconds::

    BIM:TINI ENAB
    BIM:TINI ARM
    BIM:TINI FIRE,25

Reset the TiNi pin puller sequence::

    BIM:TINI DISAble

Remarks
^^^^^^^^^^^^^^^^

Any command done out of order will reset the sequence.
E.g. if ``BIM:TINI ENAB`` → ``BIM:TINI FIRE,30`` is done, then ``BIM:TINI ENAB`` must be sent again.

.. _bim-uart-power:

BIM:UART:POWer
====================

Controls the electrical power to the UART ports on the |BIM|.

Syntax
^^^^^^^^^^^^^^^^

.. code-block::

    BIM:UART:POWer <N>,<OFF | ON>

Description
^^^^^^^^^^^^^^^^

Turns power ``ON`` or ``OFF`` to a UART channel on the |BIM|.
Specify channel number (N) 1, 2 or 3 (e.g., ``BIM:UART:POW 1, ON``).

Examples
^^^^^^^^^^^^^^^^

Supply power |BIM| port #2::

    BIM:UART:POW 2,ON

Depower |BIM| port #3::

    BIM:UART:POW 3,OFF

Remarks
^^^^^^^^^^^^^^^^

The UART RX/TX/RTS/CTS and PPS are still electrically connected to the corresponding CSK UARTs when the port is ``OFF``.

.. _bim-temp-power:

BIM:TEMP:POWer
====================

Controls the power to the temperature sensor reference.

Syntax
^^^^^^^^^^^^^^^^

.. code-block::

    BIM:TEMP:POWer <OFF | ON>

Description
^^^^^^^^^^^^^^^^

Turns power to |BIM| temperature sensors ON or OFF.

Examples
^^^^^^^^^^^^^^^^

Turn on |BIM| temperature sensors::

    BIM:TEMP:POW ON

Remarks
^^^^^^^^^^^^^^^^

The temperature reference must be ON before querying temperature sensor telemetry.
``BIM:TEL? 0`` (Temperature) will return ``0`` until the reference is on.

.. _bim-calibration-query:

BIM:CALibration?
====================

Queries the |BIM| specific calibration values.

Syntax
^^^^^^^^^^^^^^^^

.. code-block::

    BIM:CALibration? LINEAR,<idx>,[ASCII | NAME | DATA]

Description
^^^^^^^^^^^^^^^^

Request calibration values for a specific calibration index ``<idx>`` in a given format. The formats are:

* ``DATA`` - Return calibration values in binary format. Not available over user debug terminal.
* ``ASCII``- Return the calibration values in ASCII representation.
* ``NAME`` - The name of the calibration index. e.g. CTMU

Examples
^^^^^^^^^^^^^^^^

Query `temp sns. 1` calibration values in ASCII::

    BIM:CAL? LINEAR, 0, ASCII

Remarks
^^^^^^^^^^^^^^^^

The `LINEAR` argument is required. This is for forward compatibility with future calibration value types (e.g. quadratic).

.. _bim-calibration:

BIM:CALibration
====================

Sets the calibration values specific to the |BIM| module.

Syntax
^^^^^^^^^^^^^^^^

.. code-block::

    BIM:CALibration LINEAR,<idx>,<OFFSET | SCALE_FACTOR>,<value>

Description
^^^^^^^^^^^^^^^^

Set calibration ``OFFSET`` or ``SCALE_FACTOR`` values for a specific calibration ``<idx>`` in the |BIM| calibration values.
The calibration values are used internally when calculating the telemetry values for the user, applying a linear correction factor (e.g. ``final = scale_factor*initial + offset``).
Possible value sets are:

* OFFSET: Set the offset of the calibration factor. <value> is a number in the range [-32768, 32767]
* SCALE_FACTOR: Set the scale factor of the calibration factor. <value> is a floating-point number from [-16.0, 16.0].

Examples
^^^^^^^^^^^^^^^^

Set the SCALE_FACTOR to .975 and offset to -20 for the `temp sns. 1` calibration value, save to NVM::

    BIM:CAL LINEAR,0,SCALE_FACTOR,0.975
    BIM:CAL LINEAR,0,OFFSET,-20
    SUP:NVM UNLOCK,12345
    SUP:NVM WRITE,1

Remarks
^^^^^^^^^^^^^^^^

The `LINEAR` argument is required. This is for forward compatibility with future calibration value types (e.g. quadratic).

These values directly impact the output of telemetry from the SupMCU modules.

Calibration values must be saved via :ref:`SUP:NVM <supervisor-nvm>` commands.

See |BIM| Calibration for calibration values.

.. _bim-nvm:

BIM:NVM
====================

Controls the non-volatile memory specific to |BIM|.

Syntax
^^^^^^^^^^^^^^^^

.. code-block::

    BIM:NVM <UNLOCK,<value> | WRITE,<1>>

Description
^^^^^^^^^^^^^^^^

Controls access to and writes parameters to the non-volatile memory.
Order of operations when changing NVM is: UNLOCK, <change value>, WRITE.
Fields possible are:
* ``UNLOCK`` - unlocks the non-volatile memory (must be done first). Value is a key consisting of: ``device serial number + 12345``
* ``WRITE`` - Writes all set NVM values to non-volatile memory, value = 1.

Examples
^^^^^^^^^^^^^^^^

Unlock and write NVM memory (assuming serial number is 0.)::

    BIM:NVM UNLOCK,12345
    BIM:NVM WRITE,1

Remarks
^^^^^^^^^^^^^^^^

The ``UNLOCK`` and ``WRITE`` subcommands provide the same functionality as the ``SUP:NVM WRITE`` and ``SUP:NVM UNLOCK`` subcommands.

BIM:EXTLED
====================

Sets the RGB LED on the |BIM| module to the specified R, G, B color.

Syntax
^^^^^^^^^^^^^^^^

.. code-block::

    BIM:EXTLED <R>,<G>,<B>

Description
^^^^^^^^^^^^^^^^

Sets the multicolor LED on the external interface board to a color defined by 8-bit R,G and B values.

Examples
^^^^^^^^^^^^^^^^

Set the BIM EXTLED to red::

    BIM:EXTLED 255,0,0

Remarks
^^^^^^^^^^^^^^^^

None.

Deprecated Commands
====================

Below is a listing of commands to be removed in a future revision of Firmware

.. csv-table::
    :header: "Name", "Reason"
    :widths: 30,70

    "BIM:WDT", "No longer supported"
    "BIM:IMU:POWer", "Sub-component no longer supported"
    "BIM:ETH:POWer", "Sub-component no longer supported; Ethernet on PIM"
    "BIM:ETH:RESet", "Sub-component no longer supported; Ethernet on PIM"
    "BIM:ETH:MODE", "Sub-component no longer supported; Ethernet on PIM"
    "BIM:ETH:I2C", "Sub-component no longer supported; Ethernet on PIM"
    "BIM:EEprom:I2C", "Sub-component no longer supported"