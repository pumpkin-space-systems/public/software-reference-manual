========================================
SCPI Commands
========================================

The |BM| command set is responsible for:

* Querying Telemetry and Calibration values for the BM2
* Controlling the state of the BM2 heater unit
* Controlling the state of the cell balance circuit
* Setting NVM values in the BM2
* Controlling the deep-sleep functionality of the BM2
* Sending debug/raw commands to the BQ34Z Gas Gauge

.. _bm-bqflash:

BM:BQFlash
===============================

.. attention:: This command is meant for debug access only. Use when directed.

Reads/Writes to the Gas Gauge Flash memory directly.

Syntax
^^^^^^

BM:BQFlash ::

    BM:BQFlash <READ>, <subclass>, <address>, <length>
    BM:BQFlash <WRITE>, <subclass>, <address>, <length>, <byte0>, <byte1>, ..., <byteN>
    BM:BQFlash <START | STOP>, 0, 0, 0
    BM:BQFlash <READ_PAGE>, <subclass>, <address>

Description
^^^^^^^^^^^

Reads/Writes to the BQ34Z chip directly. Choices are:

* ``READ`` - Reads from a subclass section of the BQ chip at address (``0-255``) and length (``0-32``).
* ``WRITE`` - Writes to a subclass section of the BQ chip at address (``0-255``) length (``0-32``) bytes deliminated by comma.
* ``START | STOP`` - Starts/stops the flash transaction.
* ``READ_PAGE`` - Reads a whole page of the BQ chip subclass section at address (``0-8``).

Remarks
^^^^^^^

Use only when directed, for debug purposes.

.. _bm-bqmaccess:

BM:BQMaccess
===============================

.. attention:: This command is meant for debug access only. Use when directed.

Send commands to the ManufacturerAccess register of the BQ34Z Gas Gauge chip. See the ``ManufacturerAccess`` in the
`BQ34Z Technical Reference Manual <https://www.ti.com/lit/ug/sluu986a/sluu986a.pdf>`_.

Syntax
^^^^^^

BM:BQMaccess ::

    BM:BQMaccess <READ>,<length>
    BM:BQMaccess <WRITE>,<length>,<byte0>,<byte1>,...,<byteN>

Description
^^^^^^^^^^^

Writes or reads to/from the ManufacturerAccess register on the Gas Gauge chip.

Results are stored in the :ref:`bm-tlm-ma-entry` telemetry.

Remarks
^^^^^^^

For debug access only, use when directed.

.. _bm-bqsmb:

BM:BQSmb
===============================

.. attention:: This command is meant for debug access only. Use when directed.

Reads/writes from/to registers directly on the BQ34Z Gas Gauge chip.

Syntax
^^^^^^

BM:BQSmb ::

    BM:BQSmb <READ>,<address>,<length>
    BM:BQSmb <WRITE>,<address>,<length>,<byte0>,<byte1>,...,<byteN>

Description
^^^^^^^^^^^

Writes or reads to/from the BQ34Z Gas Gauge chip registers at ``<address>`` the amount of bytes specified in ``<length>``.

Results are stored in the :ref:`bm-tlm-smb-read` telemetry.

Remarks
^^^^^^^

For debug access, use only when directed.

.. _bm-function:

BM:FUNCtion
===============================

.. attention:: This command is meant for debug access only. Use when directed.

Calls a set of built-in debug functions on the BM2 module when operation is not nominal.

Syntax
^^^^^^

BM:FUNCtion ::

    BM:FUNCtion <function-id>


Description
^^^^^^^^^^^

Calls a numbered function for debugging the state of the BM2 battery. Current list is:

* ``14`` - Bypass Cell OV open
* ``15`` - Bypass Cell OV close
* ``16`` - Bypass Cell OV on
* ``17`` - Bypass Cell OV off
* ``20`` - Cell OV reinitialization
* ``21`` - Cell OV state
* ``22`` - BQ34 ALERT reinitialization
* ``23`` - BQ34 ALERT state
* ``24`` - BQ34 SAFE reinitialization
* ``25`` - BQ34 SAFE state
* ``26`` - BM2 Present open
* ``27`` - BM2 Present close
* ``28`` - BM2 Present on
* ``29`` - BM2 Present off
* ``30`` - SMB pullups reinitialization
* ``31`` - SMB pullups on
* ``32`` - SMB pullups off
* ``33`` - SMB pullups state
* ``34`` - BQ34 Wakeup on
* ``35`` - BQ34 Wakeup off

Remarks
^^^^^^^

For debugging access, only use when directed

.. _bm-nvm:

BM:NVM
===============================

Controls the NVM memory of the BM2 module

Syntax
^^^^^^

BM:NVM ::

    BM:NVM <UNLOCK>,12345
    BM:NVM <WRITE>,1
    BM:NVM <DEBUG>,<flag>
    BM:NVM <SLEEP_SF>,<scale_factor>
    BM:NVM <HEATER_SP>,<setpoint C>
    BM:NVM <HEATER_HYST>,<hyst c>


Description
^^^^^^^^^^^

Used to set the:

* Sleep scale factor on the BM2. Only set the ``SLEEP_SF`` if the sleep period is not what was expected.
* Persistent debug mask for the BM2. Do not set for FM units, meant for in-lab debugging only.
* Set the ``BM2:HEATER AUTO`` set-point, in degrees C. This is the temperature at which if the BM2 is **below** the heater will turn ON.
* Set the ``BM2:HEATER AUTO`` hysteresis. This is added to the ``BM:NVM HEATER_SP,<VAL>`` and is the point at which the automatic heater turns OFF.
    * Effectively, the ON temperature is `HEATER_SP` and the OFF temperature is ``HEATER_SP + HEATER_HYST``

Use ``BM:NVM UNLOCK,12345`` followed by ``BM:NVM WRITE,1`` to write the NVM to flash.

Remarks
^^^^^^^

Future keys may be added in the future.

NVM can be listed as well via ``cd NVM`` followed by ``list`` command via 4-pin Debug port on BM2 unit.

Example
^^^^^^^

The following will set the heater setpoint to 8C with a hysteresis value of 3C so the heater turns on <8C and turns off >11C ::

    BM:NVM UNLOCK,12345
    BM:NVM HEATER_SP,8
    BM:NVM HEATER_HYST,3
    BM:NVM WRITE,1

    # if using USB Debug Adapter, use the following to verify heater setpoint
    0)00:00:08:13.26 BM 2 SCPI|0x5C> cd ..
    0)00:00:08:14.56 BM 2 ~> cd NVM
    0)00:00:08:15.63 BM 2 NVM> list
    NVM locked, has no writes pending.
    NVM block @  0x00800-0x00FFF
    NVM mirror @ 0x03812-0x04011
    NVM mirror values:
     SupMCU:
      CRC                = 0x0123456789ABCDEF
      checksum           = 0xA36E
      E/W cycles         = 4
      boot counter       = 0
      FRC OSCTUN         = -3
      I2C address        = 0x5C
      serial number      = 0
     -RESET gate TO (s)  = 0
      debug mask         = 0x0000
     BM 2:
      debug mask         = 0x0000
      heater setpoint    =  8 degC # heater setpoint
      heater hysteresis  = +3 degC # heater hyst
      sleep time         = 0 minutes
      sleep time SF      = 0.0000
      int. resistance    = 150 mOhm
    NVM calibration values:
     SupMCU:
      CTMU               = 1.000, 0
     BM 2:
      EXT_TEMP_3         = 1.000, 0
      EXT_TEMP_4         = 1.000, 0
      EXT_TEMP_5         = 1.000, 0
      EXT_TEMP_6         = 1.000, 0
      EXT_TEMP_7         = 1.000, 0
      EXT_TEMP_8         = 1.000, 0
      BRD_T              = 1.000, 0
      SNS_SAFE           = 1.000, 0
      SNS_DQ_PFIN        = 1.000, 0
      SNS_CHARGER        = 1.000, 0
      SNS_ALERT_0        = 1.000, 0

.. _bm-telemetry:

BM:TELemetry?
===============================

Reads the telemetry from the BM2 unit

Syntax
^^^^^^

BM:TELemetry? ::

    BM:TELemetry? <N>,[DATA | ASCII | NAME | LENGTH]


Description
^^^^^^^^^^^

Request telemetry for a specific field N in the given format.
The formats are:

* ``DATA`` - Return telemetry in binary representation. Not available over user debug terminal.
* ``ASCII`` - Return telemetry in ASCII representation.
* ``NAME`` - The name of the telemetry item e.g. ``SupMCU Uptime``
* ``LENGTH`` - Length (in bytes) of telemetry in its binary representation.

Remarks
^^^^^^^

None.

Example
^^^^^^^

Request the Current on the BM2 via Debug Port::

    BM:TEL? 10,ASCII

.. _bm-heater:

BM:HEAter
===============================

Controls the state of the heater on the BM2 unit

Syntax
^^^^^^

BM:HEAter ::

    BM:HEAter <ON | OFF | AUTO>

Description
^^^^^^^^^^^

Controls the state of the heater unit on the BM2. Choices are:

* ``ON`` - Turns the heaters on. Must be turned off via ``OFF`` or ``AUTO`` command.
* ``OFF`` - Turns the heaters off. Must be turned on via ``ON`` or ``AUTO`` command.
* ``AUTO`` - **Default**. Automatically turns on the heaters when the BM2 cells reach unsafe temperatures for charging.

Remarks
^^^^^^^

None.

Example
^^^^^^^

Turn on the BM2 heater unit::

    BM:HEA ON

Turn the heater into auto mode::

    BM:HEA AUTO

.. _bm-sleep:

BM:SLEep
===============================

Puts the BM2 unit in a deep-sleep state, turning off the DSG fet to stop output from the battery for a specified time.

Charging the unit will bring the BM2 back on, or when sleep expires/aborted.

.. note:: The BM2 unit will periodically restart during a sleep to update the timer.

    This is expected behavior and DSG fets are not enabled until the end of sleep.

Syntax
^^^^^^^

BM:SLEep ::

    BM:SLEep <FOR>, <minutes>
    BM:SLEep <STOP | ABORT>

Description
^^^^^^^^^^^

The sleep command can:

* ``<FOR>, <minutes>`` - Tells the BM2 unit to sleep for ``minutes`` up to ``7200 minutes (5 days)``.
* ``<STOP | ABORT>`` - Aborts the current sleep cycle.

Remarks
^^^^^^^

None.

Example
^^^^^^^

Start sleeping for 30 minutes ::

    BM:SLEEP FOR,30

Abort sleep ::

    BM:SLEEP ABORT

.. _bm-balance:

BM:BALance
===============================

Controls the balancing circuit on the BM2.

Syntax
^^^^^^

BM:BALance ::

    BM:BALance <ON | OFF | AUTO>

Description
^^^^^^^^^^^

Turns the balancing circuit ON or OFF forcefully or enables automatic mode that will bring the cell voltages within 50 mV of each other.

Remarks
^^^^^^^

None.

Example
^^^^^^^

Turns on cell balancing ::

    BM:BAL ON

.. _bm-debug:

BM:DEBug
===============================

Controls the output of debug messages to the debug UART terminal.

Syntax
^^^^^^

BM:DEBug ::

    BM:DEBug <ENable | DISable>, mask


Description
^^^^^^^^^^^

Enables or disables debug information being printed out over the UART Debug port on the BM.
Mask is the bits of the debug information to enable.
Possible value combinations are:

* ``ENable, <mask>`` - Enables the bits represented by the mask value. Mask is a bit field and bits that are 1 will enable debug information.
* ``DISable, <mask>`` - Disables the bits represented by the mask value. Mask is a bit field and bits that are 1 will disable debug information.

Possible masks are:

* ``0x0001`` - Show the current balance of the cells periodically
* ``0x0002`` - Shows the state of the balance task periodically
* ``0x0004`` - Shows the state of the sleep task used to control ``BM:SLEep`` command.
* ``0x0008`` - Shows the debug information on the state of the heater task.
* ``0x0010`` - Shows updates to the internal resistance calculation

Remarks
^^^^^^^

None.

.. _bm-pfin:

BM:PFIn
===============================

Asserts or clears a permanent fault on the BQ34Z chip. For **debug** purposes only.

Syntax
^^^^^^

BM:PFIn ::

    BF:PFin <ON | OFF>

Description
^^^^^^^^^^^

``ON`` asserts a PF, and ``OFF`` clears the asserted PF.

Remarks
^^^^^^^

Only use when directed.

Example
^^^^^^^

Assert a PF::

    BM:PF ON

.. _bm-reset:

BM:RESet
===============================

Resets the state of the BQ34Z Gas Gauge chip. Use only when directed.

Syntax
^^^^^^

BM:RESet ::

    BM:RESet <BQ>

Description
^^^^^^^^^^^

Resets the BQ34Z chip by asserting a PF directly in the ManufacturerAccess register.

Remarks
^^^^^^^

Use only when directed.
