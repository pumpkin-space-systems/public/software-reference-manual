.. _epsm-dcps-calibration-values:

************************
Calibration
************************

Possible calibration values for the EPSM are:

.. csv-table:: EPSM Calibration Values Calibration Values
    :header: "Index", "Name", "Note"
    :widths: 10, 20, 45

    0,SAI1_IO,Applies calibration to EPSM SAI1 temp. telemetry (:ref:`EPSM:TEL? 25 <tlm-epsm-sai1-temperature>`)
    1,SAI2_IO,Applies calibration to EPSM SAI2 temp. telemetry (:ref:`EPSM:TEL? 26 <tlm-epsm-sai2-temperature>`)
    2,SAI3_IO,Applies calibration to EPSM SAI3 temp. telemetry (:ref:`EPSM:TEL? 27 <tlm-epsm-sai3-temperature>`)
    3,SAI4_IO,Applies calibration to EPSM SAI4 temp. telemetry (:ref:`EPSM:TEL? 28 <tlm-epsm-sai4-temperature>`)
    4,SAI5A_IO,Applies calibration to EPSM SAI5A temp. telemetry (:ref:`EPSM:TEL? 29 <tlm-epsm-sai5a-temperature>`)
    5,SAI5B_IO,Applies calibration to EPSM SAI5B temp. telemetry (:ref:`EPSM:TEL? 30 <tlm-epsm-sai5b-temperature>`)
    6,SAI6A_IO,Applies calibration to EPSM SAI6A temp. telemetry (:ref:`EPSM:TEL? 31 <tlm-epsm-sai6a-temperature>`)
    7,SAI6B_IO,Applies calibration to EPSM SAI6B temp. telemetry (:ref:`EPSM:TEL? 32 <tlm-epsm-sai6b-temperature>`)
    8,SNS_VUSB,Applies calibration to EPSM SNS VUSB telemetry (:ref:`EPSM:TEL? 33 <tlm-epsm-sns-vusb-ref>`)
    9,SNS_nRESET,Applies calibration to EPSM SNS nRESET telemetry (:ref:`EPSM:TEL? 34 <tlm-epsm-sns--reset-data>`)
    10,SNS_VREF_4096MV,Applies calibration to EPSM VREF 4096 telemetry (:ref:`EPSM:TEL? 35 <tlm-epsm-sns-4096mv-ref>`)
    11,V_BAT2,Applies voltage calibration to EPSM BAT2 telemetry (:ref:`EPSM:TEL? 7 <tlm-epsm-bat2>`)
    12,I_BAT2,Applies current calibration to EPSM BAT2 telemetry (:ref:`EPSM:TEL? 7 <tlm-epsm-bat2>`)
    13,V_SAI1,Applies voltage calibration to EPSM SAI1 telemetry (:ref:`EPSM:TEL? 0 <tlm-epsm-sai1>`)
    14,I_SAI1,Applies current calibration to EPSM SAI1 telemetry (:ref:`EPSM:TEL? 0 <tlm-epsm-sai1>`)
    15,V_SAI2,Applies voltage calibration to EPSM SAI2 telemetry (:ref:`EPSM:TEL? 1 <tlm-epsm-sai2>`)
    16,I_SAI2,Applies current calibration to EPSM SAI2 telemetry (:ref:`EPSM:TEL? 1 <tlm-epsm-sai2>`)
    17,V_RING1,Applies calibration to EPSM VRING1 telemetry (:ref:`EPSM:TEL? 22 <tlm-epsm-ring1-ref>`)
    18,V_REF1,Applies calibration to EPSM VREF1 telemetry (:ref:`EPSM:TEL? 12 <tlm-epsm-vref1>`)
    19,V_BAT1,Applies voltage calibration to EPSM BAT1 telemetry (:ref:`EPSM:TEL? 6 <tlm-epsm-bat1>`)
    20,I_BAT1,Applies current calibration to EPSM BAT1 telemetry (:ref:`EPSM:TEL? 6 <tlm-epsm-bat1>`)
    21,V_SAI5,Applies voltage calibration to EPSM SAI5 telemetry (:ref:`EPSM:TEL? 4 <tlm-epsm-sai5>`)
    22,I_SAI5,Applies current calibration to EPSM SAI5 telemetry (:ref:`EPSM:TEL? 4 <tlm-epsm-sai5>`)
    23,V_SAI6,Applies voltage calibration to EPSM SAI6 telemetry (:ref:`EPSM:TEL? 5 <tlm-epsm-sai6>`)
    24,I_SAI6,Applies current calibration to EPSM SAI6 telemetry (:ref:`EPSM:TEL? 5 <tlm-epsm-sai6>`)
    25,V_RING2,Applies calibration to EPSM VRING1 telemetry (:ref:`EPSM:TEL? 22 <tlm-epsm-ring1-ref>`)
    26,V_REF2,Applies calibration to EPSM VREF2 telemetry (:ref:`EPSM:TEL? 13 <tlm-epsm-vref2>`)
    27,V_AUX,Applies voltage calibration to EPSM AUX telemetry (:ref:`EPSM:TEL? 11 <tlm-epsm-aux>`)
    28,I_AUX,Applies current calibration to EPSM AUX telemetry (:ref:`EPSM:TEL? 11 <tlm-epsm-aux>`)
    29,V_SAI3,Applies voltage calibration to EPSM SAI3 telemetry (:ref:`EPSM:TEL? 2 <tlm-epsm-sai3>`)
    30,I_SAI3,Applies current calibration to EPSM SAI3 telemetry (:ref:`EPSM:TEL? 2 <tlm-epsm-sai3>`)
    31,V_SAI4,Applies voltage calibration to EPSM SAI4 telemetry (:ref:`EPSM:TEL? 3 <tlm-epsm-sai4>`)
    32,I_SAI4,Applies current calibration to EPSM SAI4 telemetry (:ref:`EPSM:TEL? 3 <tlm-epsm-sai4>`)
    33,V_RING3,Applies calibration to EPSM VRING3 telemetry (:ref:`EPSM:TEL? 24 <tlm-epsm-ring3-ref>`)
    34,V_REF3,Applies calibration to EPSM VREF3 telemetry (:ref:`EPSM:TEL? 14 <tlm-epsm-vref3>`)
    35,V_12V,Applies voltage calibration to EPSM 12V telemetry (:ref:`EPSM:TEL? 10 <tlm-epsm-12v>`)
    36,I_12V,Applies current calibration to EPSM 12V telemetry (:ref:`EPSM:TEL? 10 <tlm-epsm-12v>`)
    37,V_3V3,Applies voltage calibration to EPSM 3V3 telemetry (:ref:`EPSM:TEL? 8 <tlm-epsm-3v3>`)
    38,I_3V3,Applies current calibration to EPSM 3V3 telemetry (:ref:`EPSM:TEL? 8 <tlm-epsm-3v3>`)
    39,V_5V,Applies voltage calibration to EPSM 5V telemetry (:ref:`EPSM:TEL? 9 <tlm-epsm-5v>`)
    40,I_5V,Applies current calibration to EPSM 5V telemetry (:ref:`EPSM:TEL? 9 <tlm-epsm-5v>`)
    41,T_SB,Used internally. Edit only when directed by Pumpkin
    42,V_REF4,Applies calibration to EPSM VREF4 telemetry (:ref:`EPSM:TEL? 15 <tlm-epsm-vref4>`)
    43,V_HSK_10V,Applies calibration to EPSM HSK 10V ref telemetry (:ref:`EPSM:TEL? 21 <tlm-epsm-10v-ref>`)
    44,V_RAD,Applies calibration to EPSM dosimeter telemetry (:ref:`EPSM:TEL? 41 <tlm-epsm-dosimeter>`)
    45,V_HSK_3V3,Applies calibration to EPSM HSK 3V3 ref telemetry (:ref:`EPSM:TEL? 8 <tlm-epsm-3v3>`)
    46,V_CORE_1V5,Applies calibration to EPSM CORE 1V5 ref telemetry (:ref:`EPSM:TEL? 18 <tlm-epsm-core-1v5-ref>`)
    47,T_FPGA,Used internally. Edit only when directed by Pumpkin
    48,V_HSK_5V,Applies calibration to EPSM HSK 5V ref telemetry (:ref:`EPSM:TEL? 20 <tlm-epsm-hsk-5v-ref>`)
    49,V_HSK_N5V,Applies calibration to EPSM HSK N5V ref telemetry (:ref:`EPSM:TEL? 17 <tlm-epsm-hsk--5v-ref>`)
    50,V_REF5,Applies calibration to EPSM VREF3 telemetry (:ref:`EPSM:TEL? 16 <tlm-epsm-vref5>`)
