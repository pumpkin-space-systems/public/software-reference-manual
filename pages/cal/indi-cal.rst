.. _indi-calibration-values:

******************************
Calibration
******************************

All calibration values set via the :ref:`INDI:CALibration <indi-calibration>` command

Possible calibration values for the INDI are:

.. csv-table:: INDI Calibration Values Calibration Values
    :header: "Index", "Name", "Note"
    :widths: 10, 20, 45

    0,TEMP_SENS1,Applies calibration to INDI Temperature Sensor #1. telemetry (:ref:`INDI:TEL? 4 <tlm-indi-temperature-1>`)
    1,TEMP_SENS2,Applies calibration to INDI Temperature Sensor #2. telemetry (:ref:`INDI:TEL? 5 <tlm-indi-temperature-2>`)
    2,TEMP_SENS3,Applies calibration to INDI Temperature Sensor #3. telemetry (:ref:`INDI:TEL? 6 <tlm-indi-temperature-3>`)
    3,TEMP_SENS4,Applies calibration to INDI Temperature Sensor #4. telemetry (:ref:`INDI:TEL? 7 <tlm-indi-temperature-4>`)
    4,VNE2,Applies calibration to INDI Flyback converter voltage. telemetry (:ref:`INDI:TEL? 3 <tlm-indi-flyback-voltage>`)
    5,I5V0,Applies calibration to INDI 5V current telemetry (:ref:`INDI:TEL? 1 <tlm-indi-5v-current>`)
    6,IFB,Applies calibration to INDI Full-bridge current telemetry (:ref:`INDI:TEL? 2 <tlm-indi-full-bridge-current>`)
    7,I12V,Applies calibration to INDI 12V current telemetry (:ref:`EPSM:TEL? 0 <tlm-indi-12v-current>`)
