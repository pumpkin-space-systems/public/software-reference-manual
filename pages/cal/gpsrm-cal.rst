.. _gpsrm-calibration-values:

*************************
Calibration
*************************

The possible GPSRM calibration values are:

.. csv-table:: GPSRM Calibration values Calibration Values
    :header: "Index", "Name", "Note"
    :widths: 10, 20, 45

    0,VCC_SYS,Applies calibration to GPS OEM719 Power telemetry (:ref:`GPS:TEL? 3 <tlm-gpsrm-novatel-oem719-power>`)
    1,3V3_USB,Applies calibration to GPS OEM719 Power telemetry (:ref:`GPS:TEL? 3 <tlm-gpsrm-novatel-oem719-power>`)
    2,P5V_SYS,Applies calibration to GPS OEM719 Power telemetry (:ref:`GPS:TEL? 3 <tlm-gpsrm-novatel-oem719-power>`)
    3,P5V_USB,Applies calibration to GPS OEM719 Power telemetry (:ref:`GPS:TEL? 3 <tlm-gpsrm-novatel-oem719-power>`)
