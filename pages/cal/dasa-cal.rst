.. _dasa-calibration-values:

******************************
Calibration
******************************

The possible DASA calibration values are:

.. csv-table:: DASA Calibration Values Calibration Values
    :header: "Index", "Name", "Note"
    :widths: 10, 20, 45

    0,Temp. sensor 1,Applies calibration to DASA Temp. 1 telemetry (:ref:`DASA:TEL? 6 <tlm-dasa-temperature-1>`)
    1,Temp. sensor 2,Applies calibration to DASA Temp. 2 telemetry (:ref:`DASA:TEL? 7 <tlm-dasa-temperature-2>`)
    2,Temp. sensor 3,Applies calibration to DASA Temp. 3 telemetry (:ref:`DASA:TEL? 8 <tlm-dasa-temperature-3>`)
    3,Temp. sensor 4,Applies calibration to DASA Temp. 4 telemetry (:ref:`DASA:TEL? 9 <tlm-dasa-temperature-4>`)
    4,release current,Applies current calibration to DASA release telemetry (:ref:`DASA:TEL? 3 <tlm-dasa-tini-pin-puller-status>`)
    5,release voltage,Applies voltage calibration to DASA release telemetry (:ref:`DASA:TEL? 3 <tlm-dasa-tini-pin-puller-status>`)
    6,drive B sns,Applies calibration to DASA B-drive current telemetry (:ref:`DASA:TEL? 2 <tlm-dasa-drive-motor-status>`)
    7,drive A sns,Applies calibration to DASA A-drive current telemetry (:ref:`DASA:TEL? 2 <tlm-dasa-drive-motor-status>`)
    8,NPR,Applies calibration to DASA NPR temperature reference telemetry (:ref:`DASA:TEL? 4 <tlm-dasa-npr-status>`). This will also affect the temperature readings as well.
    9,+5V_SYS,Not used.
    10,Hall-effect sensor,"Applies calibration to HES sensor reading telemetry (:ref:`DASA:TEL? 5 <tlm-dasa-hes-status>`). **If set incorrectly, DASA homing will fail.**"
