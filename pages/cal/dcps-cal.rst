.. _dcps-calibration-values:

************************
Calibration
************************

The DCPS calibration values are:

.. csv-table:: DCPS Calibration values Calibration Values
    :header: "Index", "Name", "Note"
    :widths: 10, 20, 45

    0,I_SNS_12V,Applies current calibration to DCPS 12V telemetry (:ref:`DCPS:TEL? 9 <tlm-dcps-5v>`)
    1,I_SNS_3V3,Applies current calibration to DCPS 3V3 telemetry (:ref:`DCPS:TEL? 8 <tlm-dcps-3v3>`)
    2,I_SNS_VBATT,Applies current calibration to DCPS VBATT telemetry (:ref:`DCPS:TEL? 11 <tlm-dcps-vbatt>`)
    3,CADEX,Not used.
    4,I_SNS_5V,Applies current calibration to DCPS 5V telemetry (:ref:`DCPS:TEL? 9 <tlm-dcps-5v>`)
