.. _aim-calibration-values:

***********************
Calibration
***********************

Possible AIM calibration values are:

.. csv-table:: AIM Calibration values Calibration Values
    :header: "Index", "Name", "Note"
    :widths: 10, 20, 45

    0,VCC_SYS,Applies calibration to AIM OEM719 Power telemetry (:ref:`AIM:TEL? 3 <tlm-aim-oem719-power>`)
    1,3V3_USB,Applies calibration to AIM OEM719 Power telemetry (:ref:`AIM:TEL? 3 <tlm-aim-oem719-power>`)
