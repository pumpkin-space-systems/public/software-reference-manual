.. _pim-calibration-values:

************************
Calibration
************************

The possible PIM calibration values are:

.. csv-table:: PIM Calibration Values Calibration Values
    :header: "Index", "Name", "Note"
    :widths: 10, 20, 45

    0,SNS_CH1_I,Applies calibration to PIM current telemetry (:ref:`PIM:TEL? 0 <tlm-pim-channel-currents>`)
    1,SNS_CH1_V,Applies calibration to PIM voltage telemetry (:ref:`PIM:TEL? 5 <tlm-pim-channel-voltages>`)
    2,SNS_CH2_I,Applies calibration to PIM current telemetry (:ref:`PIM:TEL? 0 <tlm-pim-channel-currents>`)
    3,SNS_CH2_V,Applies calibration to PIM voltage telemetry (:ref:`PIM:TEL? 5 <tlm-pim-channel-voltages>`)
    4,SNS_CH3_I,Applies calibration to PIM current telemetry (:ref:`PIM:TEL? 0 <tlm-pim-channel-currents>`)
    5,SNS_CH3_V,Applies calibration to PIM voltage telemetry (:ref:`PIM:TEL? 5 <tlm-pim-channel-voltages>`)
    6,SNS_CH4_I,Applies calibration to PIM current telemetry (:ref:`PIM:TEL? 0 <tlm-pim-channel-currents>`)
    7,SNS_CH4_V,Applies calibration to PIM voltage telemetry (:ref:`PIM:TEL? 5 <tlm-pim-channel-voltages>`)
