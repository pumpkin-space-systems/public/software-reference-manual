.. _supmcu-calibration-values:

******************************
Calibration
******************************

The possible SupMCU Calibration values are:

.. csv-table:: SupMCU Calibration Values Calibration Values
    :header: "Index", "Name", "Note"
    :widths: 10, 20, 45

    0,CTMU,Applies calibration to the CTMU telemetry (:ref:`SUP:TEL? 15 <tlm-sup-supmcu-temperature>`)
