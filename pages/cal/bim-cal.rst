.. _bim-calibration-values:

************************
Calibration
************************

The possible BIM calibration values are:

.. csv-table:: BIM Calibration Values Calibration Values
    :header: "Index", "Name", "Note"
    :widths: 10, 20, 45

    0,Temp. sns 1,Applies calibration to BIM temperature sensor telemetry (:ref:`BIM:TEL? 0 <tlm-bim-temperature>`)
    1,Temp. sns 2,Applies calibration to BIM temperature sensor telemetry (:ref:`BIM:TEL? 0 <tlm-bim-temperature>`)
    2,Temp. sns 3,Applies calibration to BIM temperature sensor telemetry (:ref:`BIM:TEL? 0 <tlm-bim-temperature>`)
    3,Temp. sns 4,Applies calibration to BIM temperature sensor telemetry (:ref:`BIM:TEL? 0 <tlm-bim-temperature>`)
    4,Temp. sns 5,Applies calibration to BIM temperature sensor telemetry (:ref:`BIM:TEL? 0 <tlm-bim-temperature>`)
    5,Temp. sns 6,Applies calibration to BIM temperature sensor telemetry (:ref:`BIM:TEL? 0 <tlm-bim-temperature>`)
