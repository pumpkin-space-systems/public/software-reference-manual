##################################
Battery Module 2 (BM2)
##################################

The Battery Module 2 (BM2) provides up-to 100Wh of battery life in a `2S4P`, `3S2P` or `4S2P` cell configuration and sustain high-discharge rates of upto 10A.
The unit has individual safeguards to protect the battery unit its self (thermal/short circuit/cell balancing/overcurrent/overvoltage/...).

See the BM2 data sheet for more information.

.. include:: ./scpi/bm-cmd.rst

.. include:: ./tlm/bm-tlm.rst

.. include:: ./cal/bm-cal.rst

.. include:: ./substitutions.rst