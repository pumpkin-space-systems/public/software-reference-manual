###########################
Telemetry
###########################

Each |SupMCU| contains a telemetry table data structure comprised of |SupMCU| parent fields and module specific fields.
This data structure is updated when telemetry is requested.
Each field of the table can be queried by sending the corresponding field index when sending a SCPI Telemetry command.

Sending/writing a telemetry request via I2C should be followed by an I2C read to get the queried data. Pumpkin |SupMCU| modules have 4 data parameters:

* ``data`` - Telemetry data
* ``name`` - Name of field in a 32 byte ASCII string
* ``length`` - 2 byte unsigned integer value of data length for I2C read
* ``ascii`` - Telemetry data in ASCII format (max 128 bytes)

The full list of SupMCU and Module telemetry fields are in the following tables.
Examples of how to correctly send and receive telemetry requests and responses are provided in the sections following the telemetry field tables.
These sections detail the header lengths associated with telemetry requests, and data lengths.

Here is a list of the format specifiers and their corresponding C language types from ``stdint.h`` header.

.. csv-table:: Format Specifiers
    :header: "Format", "C Standard Type"
    :widths: 15, 40

    "|u8|, |u16|, |u32|, |u64|","``uint8_t``, ``uint16_t``, ``uint32_t``, ``uint64_t``"
    "|i8|, |i16|, |i32|, |i64|","``int8_t``, ``int16_t``, ``int32_t``, ``int64_t``"
    "|f32|, |f64|","``float``, ``double``"
    "|hex|","``uint8_t`` (usually encoding a bit-field)"
    "string/ASCII","``char*``"

.. include:: ./substitutions.rst