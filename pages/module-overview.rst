###############################
Supervisor MCU Module Overview
###############################

Pumpkin provides a range of |SupMCU| that integrate directly into a |SUPERNOVA| bus.
The |SupMCU| provides a human readable SCPI command interface communicated via I2C or UART Debug Port, in which the user commands the module, sets calibration values for telemetry items, retreives telemetry, and sets NVM parameters.
Once a module is commanded for telemetry or calibration values, the binary data can be retreived via I2C on a C&DH acting as an I2C Master.
Alternatively human readable ASCII representation can be seen via the UART Debug Port.

.. caution:: The output to the Debug UART is considered unsuitable for FSW parsing, and should only be used for debug and testing purposes. C&DH modules shall interface with the |SupMCU| modules via I2C.

**********************
Pumpkin SupMCU Modules
**********************

The |SupMCU| modules provide functionality such as:

* Power switching
* Bus power source
* Energy storage
* Radio power and communication interfaces
* Ethernet switching
* UART Communication switching
* Panel Release
* GPS host and communication interface
* ADCS communication interface
* Solar panel power input
* Solar panel articulation
* Satellite temperature sensors
* Orbit Propegation

Pumpkin currently provides the following modules that can integrate into a |SUPERNOVA| bus:

* ADCS Interface Module (AIM)
* Payload Interface Module (PIM)
* Bus Interface Module (BIM)
* GPS Receiver Module (GPSRM)
* Elecritcal Power System Module (EPSM)
* Battery Module (BM)
* Battery Switch Module (BSM)
* Solar Interface Module (SIM)
* Desktop Cubesat Power System (DCPS)
* Deployable Articulated Solar Array (DASA)
* Radio Host Module (RHM)

.. Include substitutions for various modules.

.. include:: ./substitutions.rst
