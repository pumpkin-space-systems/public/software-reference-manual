###############################
Power Distribution Board (PDB)
###############################

The Power Distribution Board (PDB) manages power distribution for high-power nano satellites.
It provides three power ports.

See PDB data sheet for more information.

.. include:: ./scpi/pdb-cmd.rst

.. include:: ./tlm/pdb-tlm.rst

.. include:: ./substitutions.rst
