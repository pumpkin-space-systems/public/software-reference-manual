#######################################
Desktop Cubesat Power System (DCPS)
#######################################

The Desktop Cubesat Power System (DCPS) provides all standard power levels for a Cubesat from a single external DC supply.
This module is used in place of an EPS for bench testing of a cubesat.

See the DCPS data sheet for more information.


.. include:: ./scpi/dcps-cmd.rst

.. include:: ./tlm/dcps-tlm.rst

.. include:: ./cal/dcps-cal.rst

.. include:: ./substitutions.rst