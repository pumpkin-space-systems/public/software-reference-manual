##########################
Bus Interface Module (BIM)
##########################

The BIM SupMCU is in charge of interfacing to all components on the bus that are remote (not a part of standard CSK-compatible module).
The current version allows for serial and power control of connected devices. The BIM also implements three serial UART interfaces on the main PCB.

In addition to this, BIM also runs the Analog to Digital Conversion (ADC) task required to read the temperature from the temperature sensors embedded on the solar panels if fit.
It is also capable of running a task that is in charge of energizing the TiNi pin puller connected to J12 to deploy solar panels on a 6U or larger craft.

Current FW Version: ``fw v1.3.1a & SupMCU Core v1.6.0f``

See BIM data sheet for more information.

.. include:: ./scpi/bim-cmd.rst

.. include:: ./tlm/bim-tlm.rst

.. include:: ./cal/bim-cal.rst

.. include:: ./substitutions.rst