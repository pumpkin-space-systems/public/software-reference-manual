###################
SCPI Command Format
###################

|SUPERNOVA| modules are commanded via a subset of the Standard Commands for Programmable Instruments (SCPI) protocol.
In this document the UPPER CASE letters of a command are required and the lower case letters are optional.
Possible options for a given slot in a command are separated with a ``|`` character.
Below are samples of how commands are described in this document::

	DEVice:TASK <Required param opt 1 | required param opt 2 | required param opt 3>,[Optional param 1 | optional param 2]
	DEVice:SUBsystem:TASK <Required parameter>,[Optional parameter]

Each SCPI command must end with a <CR> (``0x0A``) character.
Note that SCPI commands are human-readable; a protocol analyzer that is snooping the I2C bus will reveal easy-to-read commands being sent to the various SupMCUs in a |SUPERNOVA| system.

Commands for each module are broken out by module while commands that apply to all modules are broken out in the SupMCU SCPI Commands section. 

.. include:: ./substitutions.rst