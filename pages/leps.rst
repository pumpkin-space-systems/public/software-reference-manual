#######################################
Linear Electrical Power System (LEPS)
#######################################

The Linear Electrical Power System (LEPS) manages power distribution for nano satellites.
The LEPS provides 5V, 3.3V and VBATT output rails.

See LEPS data sheet for more information.

.. include:: ./scpi/leps-cmd.rst

.. include:: ./tlm/leps-tlm.rst

.. include:: ./substitutions.rst