##################################
ADCS Interface Module (AIM)
##################################

The AIM2 provides functionality to interface and power the MAI-400 ADCS module.
The AIM2 also houses the OEM719 module and provides communication and power interfaces to the OEM719.

The AIM2 also contains all of the same functionality as the GPSRM module.

See the AIM2 data sheet for more information

.. include:: ./substitutions.rst

.. include:: ./scpi/aim-cmd.rst

.. include:: ./tlm/aim-tlm.rst

.. include:: ./cal/aim-cal.rst
