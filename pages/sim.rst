Solar Interface Module (SIM)
****************************

The Solar Interface Module (SIM) connects coarse sun sensors for the MAI-400 ADCS, power and temperature sensors for the solar panels along with firing the panel release modules for a MISC-3 CubeSat.
See the SIM data sheet for more information.

.. include:: ./scpi/sim-cmd.rst

.. include:: ./tlm/sim-tlm.rst

.. include:: ./cal/sim-cal.rst

.. include:: ./substitutions.rst
