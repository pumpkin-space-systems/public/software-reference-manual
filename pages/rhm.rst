######################################
Radio Host Module (RHM)
######################################

The Radio Host Module (RHM) provides communication and power interfaces to a GlobalStar Simplex and/or AstroDev Lithium Radio **(RHM1 Only)**.
The RHM1 can also host the Watch-dog timer circuit of the SUPERNOVA bus as well.

See the RHM data sheet for more information.

.. include:: ./scpi/rhm-cmd.rst

.. include:: ./tlm/rhm-tlm.rst

.. include:: ./cal/rhm-cal.rst

.. include:: ./substitutions.rst