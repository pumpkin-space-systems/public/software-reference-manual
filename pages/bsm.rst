##################################
Battery Switch Module (BSM)
##################################

The Battery Switch Module (BSM) provides up to 5 output ports directly from a BM2 module.
The BSM also breaks out 5 extra temperature sensors that are directly on the board or present on the BM2.

See the BSM data sheet for more information.

.. include:: ./scpi/bsm-cmd.rst

.. include:: ./tlm/bsm-tlm.rst

.. include:: ./cal/bsm-cal.rst

.. include:: ./substitutions.rst