##################################
Supervisor MCU (SupMCU)
##################################

These are all of the commands/telemetry/calibration that are common across all of the SupMCU modules.

See the individual module data sheet for more information.

.. include:: ./scpi/supmcu-cmd.rst

.. include:: ./tlm/supmcu-tlm.rst

.. include:: ./cal/supmcu-cal.rst

.. include:: ./substitutions.rst