#######################################
Electrical Power System Module (EPSM)
#######################################

The Electrical Power System Module (EPSM) manages power distribution for high-power nano satellites.
The EPSM provides 12V, 5V, 3.3V and AUX output rails.
It also provides six solar panel input SAIs, and two battery input/output blocks.

See EPSM data sheet for more information.

.. include:: ./scpi/epsm-cmd.rst

.. include:: ./tlm/epsm-tlm.rst

.. include:: ./cal/epsm-cal.rst

.. include:: ./substitutions.rst