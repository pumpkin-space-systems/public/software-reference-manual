.. |SupMCU| replace:: :abbr:`SupMCU (Supervisor MCU)`

.. |SUPERNOVA| replace:: :abbr:`SUPERNOVA (Six-U Preformance Enhanced Radiation-tolerant Nanosatellite Vehicle Architecture)`

.. |AIM| replace:: :abbr:`AIM (ADCS Interface Module)`

.. |BIM| replace:: :abbr:`BIM (Bus Interface Module)`

.. |GPSRM| replace:: :abbr:`GPSRM (GPS Receiver Module)`

.. |LEPS| replace:: :abbr:`LEPS (Linear Electrical Power Systen)`

.. |MBM| replace:: :abbr:`MBM (Motherboard Module)`

.. |MBM2| replace:: :abbr:`MBM2 (Motherboard Module 2)`

.. |EPSM| replace:: :abbr:`EPSM (Electrical Power System Module)`

.. |BM| replace:: :abbr:`BM (Battery Module)`

.. |BM1| replace:: :abbr:`BM1 (Battery Module 1)`

.. |BM2| replace:: :abbr:`BM2 (Battery Module 2)`

.. |BSM| replace:: :abbr:`BSM (Battery Switch Module)`

.. |SIM| replace:: :abbr:`SIM (Solar Interface Module)`

.. |DCPS| replace:: :abbr:`DCPS (Desktop Cubesat Power Supply)`

.. |DASA| replace:: :abbr:`DASA (Deployable Articulated Solar Array)`

.. |PDB| replace:: :abbr:`PDB (Power Distribution Board)`

.. |PIM| replace:: :abbr:`PIM (Payload Interface Module)`

.. |RHM| replace:: :abbr:`RHM (Radio Host Module)`

.. |INDI| replace:: :abbr:`INDI (Shutter Motor, Lamp, Heater controller)`

.. |u8| replace:: **(u**\ :sub:`8`\ **)**

.. |u16| replace:: **(u**\ :sub:`16`\ **)**

.. |u32| replace:: **(u**\ :sub:`32`\ **)**

.. |u64| replace:: **(u**\ :sub:`64`\ **)**

.. |i8| replace:: **(i**\ :sub:`8`\ **)**

.. |i16| replace:: **(i**\ :sub:`16`\ **)**

.. |i32| replace:: **(i**\ :sub:`32`\ **)**

.. |i64| replace:: **(i**\ :sub:`64`\ **)**

.. |f32| replace:: **(f**\ :sub:`32`\ **)**

.. |f64| replace:: **(f**\ :sub:`64`\ **)**

.. |hex| replace:: **(hex)**

.. |string| replace:: **(string)**

.. |str| replace:: **(string)**
