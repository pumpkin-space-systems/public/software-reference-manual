####################
Calibration
####################

SupMCU modules provide a calibration system which allows the user to apply a linear slope and offset correction to select telemetry values and modules come pre-calibrated, where necessary (e.g. the PIM comes with factory calibration values installed for current measurement telemetry).
Modules can be calibrated by the user for their specific application, or to refine the calibration values already entered.
Each module has a set of SupMCU global calibration values, and module specific calibration values.
These sets have their own version of the calibration commands.

The calibration values are represented in the telemetry format: ``scale factor`` |f32|, ``offset`` |i16|
There are two calibration commands: Query and Set.
In order to query a calibration value, use::

    <SUP | MOD>:CAL? LINEAR,<idx>,[DATA | ASCII | NAME]

The calibration query response for ``DATA`` will only be sent over I2C, for the format and structure of the I2C response, see section Structure and Format of Telemetry and Calibration.
Data requests will not be sent over the Debug UART port, ASCII format must be used.
The ASCII response will output the value of the given calibration index in ASCII string representation, which is sent over I2C and via UART Debug Port.
A typical query will look like::

    0)00:00:00:26.41 RHM SCPI|0x55> SUP:CAL? LINEAR,0,ASCII
    0)00:00:00:39.52 RHM SCPI|0x55> [3:3954] 1.0000,0

The ``NAME`` response will output the name of the calibration index, up to a max of 32 ASCII characters.
This data is sent over I2C and via UART Debug Port.
A typical query will look like::

    0)00:00:19:10.86 RHM (on STM) SCPI|0x55> SUP:CAL? LINEAR,0,NAME
    0)00:00:19:12.21 RHM (on STM) SCPI|0x55> [1:115223] CTMU

Note, when the ``[DATA | ASCII | NAME]`` subcommand is omitted, ``DATA`` is selected as the default for the query command.
 
In order to set a calibration value, use::

    <SUP | MOD>:CAL LINEAR,<idx>,<OFFSET, <integer> | SCALE_FACTOR, <floating-point>>

Note the ``SCALE_FACTOR`` and ``OFFSET`` values are set separately.
Note that the ``SCALE_FACTOR`` entered won’t always match the user input value, due to how the values are stored internally.
An example to set the CTMU calibration factor to a scale factor of 0.98 and offset of -20 is::

    0)00:00:37:29.75 RHM (on STM) SCPI|0x55> SUP:CAL LINEAR,0,SCALE_FACTOR,0.98
    0) 00:00:44:33.72 supmcu_scpi_cmds: Accepted "SUP:CAL LINEAR,0,SCALE_FACTOR,0.98" command.
    0)00:00:47:00.46 RHM (on STM) SCPI|0x55> SUP:CAL LINEAR,0,OFFSET,-20
    0)00:00:47:00.49 supmcu_scpi_cmds: Accepted "SUP:CAL LINEAR,0,OFFSET,-20" command.
    0)00:00:47:10.23 RHM (on STM) SCPI|0x55> SUP:CAL? LINEAR,0,ASCII
    [3:283025] 0.9800,-20
    0)00:00:47:10.28 supmcu_scpi_cmds: Accepted "SUP:CAL? LINEAR,0,ASCII" command.

After setting calibration values, the NVM memory needs to be unlocked and written to in order for the calibration to be persistent on next restart of the module.
Use the SUP:NVM command in the SupMCU SCPI command section in order to unlock and write NVM memory.
Below are the listing of calibration values possible as well as notes about them.

.. include:: ./substitutions.rst
