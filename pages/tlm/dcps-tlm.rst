*************************
Telemetry
*************************

.. highlight:: none

The DCPS Telemetry set contains

* State of bus power rails (3.3V, 5V, 12V, VBATT)

.. note:: The DCPS contains more telemetry indicies than listed below. However, all entries pertain to EPSM-only EPS telemetry.

    These unused telemetry indicies are all set to zero's/defaults and don't contain any relevant information to operation of the module.

.. _tlm-dcps-3v3:

3.3V Converter Data
==============================

The current voltage, voltage set point, current, current limit and status for 3.3V converter.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    DCPS:TEL? 8, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**13** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|, |i16|, |i16|, |u8|, |i32|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Converter Data Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50

    "0-1","Voltage reading in mV on converter, fixed to ``3300 mV`` (u16)."
    "2-3","Voltage maximum set point in mV on converter, fixed to ``3300 mV`` (u16)."
    "4-5","Current reading in mA on converter (i16)."
    "6-7","Current limit in mA on converter, fixed to ``5000 mA`` (i16)."
    "8","Status of Bus. Possible values are::

        0x00 – Enabled.
        0x01 – Disabled."
    "9-12","Power in mW (i32)"

.. _tlm-dcps-5v:

5V Converter Data
==============================

The current voltage, voltage set point, current, current limit and status for 5V converter.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    DCPS:TEL? 9, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**13** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|, |i16|, |i16|, |hex|, |i32|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Converter Data Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50

    "0-1","Voltage reading in mV on converter, fixed to ``5000 mV`` (u16)."
    "2-3","Voltage maximum set point in mV on converter, fixed to ``5000 mV`` (u16)."
    "4-5","Current reading in mA on converter (i16)."
    "6-7","Current limit in mA on converter, fixed to ``4700 mA`` (i16)."
    "8","Status of Bus. Possible values are::

        0x00 – Enabled.
        0x01 – Disabled."
    "9-12","Power in mW (i32)"

.. _tlm-dcps-12v:

12V Converter Data
==============================

The current voltage, voltage set point, current, current limit and status for 12V converter.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    DCPS:TEL? 10, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**13** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|, |i16|, |i16|, |hex|, |i32|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Converter Data Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50

    "0-1","Voltage reading in mV on converter, fixed to ``12000 mV`` (u16)."
    "2-3","Voltage maximum set point in mV on converter, fixed to ``12000 mV`` (u16)."
    "4-5","Current reading in mA on converter (i16)."
    "6-7","Current limit in mA on converter, fixed to ``3500 mA`` (i16)."
    "8","Status of Bus. Possible values are::

        0x00 – Enabled.
        0x01 – Disabled."
    "9-12","Power in mW (i32)"

.. _tlm-dcps-vbatt:

VBATT Converter Data
==================================

The current voltage, voltage set point, current, current limit and status for VBATT converter.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 11, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**13** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|, |i16|, |i16|, |hex|, |i32|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Converter Data Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50

    "0-1","Voltage reading in mV on converter, fixed to selected VBATT voltage configuration ``8400/16800 mV`` (u16)."
    "2-3","Voltage maximum set point in mV on converter, fixed to selected VBATT voltage configuration ``8400/16800 mV`` (u16)."
    "4-5","Current reading in mA on converter (i16)."
    "6-7","Current limit in mA on converter, fixed to ``2500 mA`` (i16)."
    8,"Status of Bus. Possible values are::

        0x00 – Enabled.
        0x01 – Disabled."
    "9-12","Power in mW (i32)"
