************************
Telemetry
************************

.. highlight:: none

The GPSRM Telemetry set contains:

* Power state information on the OEM719
* Orbit Propagator results
* OEM719 power usage.

.. _tlm-gpsrm-power-status:

Power Status
====================================

State values pertaining to GPS power, reset, position valid, and pass through.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    GPS:TEL? 0, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A single bit-field value with the data format:  |hex|

Bit-field
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Power status Bit-field
    :header: "Bits", "Values"
    :widths: 10, 50

    "0","1 =  Power is applied to OEM719"
    "1","1 =  RESET signal is applied to OEM719"
    "2","1 =  Position Valid pin on OEM719 is HIGH (ON)"
    "3","1 =  Pass through is enabled on GPSRM."
    "4-7","Reserved (0)"

.. _tlm-gps-orbit-propagator:

Orbit Propagator Results
====================================

Results from Vinti7 or SGP4 orbit propagators.
See :ref:`gps-propagate` for usage. All zeros until propagator has ran.

.. note:: Invalid propagator input will set all values to 0 for propagator results.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    GPS:TEL? 2, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**56** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |f64|, |f64|, |f64|, |f64|, |f64|, |f64|, |f64|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Orbit Propagator Results Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50

    "0-7","Time t1 (Vinti7) or time_delta from initial time (TLE SGP4) (f64)"
    "8-15","X-position of propagated orbit (f64)"
    "16-23","Y-position of propagated orbit (f64)"
    "24-31","Z-position of propagated orbit (f64)"

.. _tlm-gpsrm-novatel-oem719-power:

Novatel OEM719 Power [mW]
====================================

Power draw readings of the OEM719 voltage supply rails. Values are measured in milliwatts.

.. note:: See GPSRM data sheet for more information on voltage rails.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    GPS:TEL? 3, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**8** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|, |u16|, |u16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Novatel OEM719 Power [mW] Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50

    "0-1","Power draw reading in milliwatts from VCC_SYS (u16)"
    "2-3","Power draw reading in milliwatts from +5V_SYS (u16)"
    "4-5","Power draw reading in milliwatts from 3V3_USB (u16)"
    "6-7","Power draw reading in milliwatts from +5V_USB (u16)"

Deprecated Telemetry
====================

Below is a listing of telemetry to be removed in a future revision:

.. csv-table:: Deprecated Telemetry
    :header: "Index", "Name", "Reason"
    :widths: 10, 30, 60

    "1", "NMEA String", "Functionality not supported on latest revision, use direct UART to OEM719 unit"

.. highlight:: python
