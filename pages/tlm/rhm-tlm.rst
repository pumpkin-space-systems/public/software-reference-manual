**************************
Telemetry
**************************

.. highlight:: none

The RHM telemetry set contains information for:

* Lithium Radio state **RHM1 Only**
* GlobalStar Radio state
* GlobalStar ``GS:SEND`` state and beacons sent

.. _tlm-rhm-lithium-power-status:

Lithium Power Status
========================================

State of Lithium Radio power amplifier (PA) and microcontroller unit (MCU) Power.
Use :ref:`RHM:LI:PA <rhm-lithium-pa>`, :ref:`RHM:LI:MCU <rhm-lithium-mcu>` and :ref:`RHM:LI:POW <rhm-lithium-power>` commands to manipulate state.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    RHM:TEL? 0, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^

**1** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^

A single bit-field value with the data format:  |hex|

Bit-field
^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Lithium Power Status Bit-field
    :header: "Bits", "Values"
    :widths: 10, 50
    :file: ./rhm/lithium-power-status.csv

.. _rhm-lithium-uart-status:

Lithium UART Status
========================================

State of Lithium Radio UART communications.
Represents current communication channel and if UART pass through is ON or OFF.
Control via :ref:`RHM:LI:COMM <rhm-lithium-comm>` and :ref:`RHM:LI:PASS <rhm-lithium-passthrough>` commands.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    RHM:TEL? 1, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^

**1** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u8|

Bit-field
^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Lithium UART status Bit-field
    :header: "Bits", "Values"
    :widths: 10, 50
    :file: ./rhm/lithium-uart-status.csv

.. _tlm-rhm-lithium-config-status:

Lithium Config Status
========================================

State of the configure pins on the Lithium radio. Controlled via the :ref:`RHM:LI:CONF <rhm-lithium-config>` command.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    RHM:TEL? 2, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^

**1** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^

A single bit-field value with the data format:  |hex|

Bit-field
^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Lithium Config Status Bit-field
    :header: "Bits", "Values"
    :widths: 10, 50
    :file: ./rhm/lithium-config-status.csv

.. _tlm-rhm-globalstar-power-status:

GlobalStar Power Status
========================================

Reporting power status for the GlobalStar Simplex radio. Controlled via :ref:`RHM:GS:POW <rhm-gstar-power>` command.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    RHM:TEL? 3, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^

**1** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^

A single bit-field value with the data format:  |hex|

Bit-field
^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: GlobalStar Power Status Bit-field
    :header: "Bits", "Values"
    :widths: 10, 50
    :file: ./rhm/globalstar-power-status.csv

.. _tlm-rhm-globalstar-uart-status:

GlobalStar UART Status
========================================

State of UART Communication and transmission status for GlobalStar Simplex radio.
Reports current UART channel, pass through enabled, and transmission status.
Use :ref:`RHM:GS:COMM <rhm-gstar-comm>`, :ref:`RHM:GS:PASS <rhm-gstar-passthrough>` to control UART state.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    RHM:TEL? 4, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^

**1** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u8|

Bit-field
^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: GlobalStar UART Status Bit-field
    :header: "Bits", "Values"
    :widths: 10, 50
    :file: ./rhm/globalstar-uart-status.csv

.. _tlm-rhm-gs-din-output:

GS DIN Outputs
========================================

The state of the Digital Input (DIN) pins on GlobalStar Simplex radio. Use :ref:`RHM:GS:DIN <rhm-gstar-din>` to control state of pins.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    RHM:TEL? 5, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^

**1** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^

A single bit-field value with the data format:  |hex|

Bit-field
^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: GlobalStar DIN Outputs Bit-field
    :header: "Bits", "Values"
    :widths: 10, 50
    :file: ./rhm/globalstar-din-output.csv

.. _tlm-rhm-gs-busy:

GS Busy
========================================

State of BUSY line on GlobalStar Simplex radio.
This is raised HIGH (ON) when a transmission starts, or during boot of the GlobalStar Simplex.
See GlobalStar Simplex ICD for information on when BUSY line is HIGH.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    RHM:TEL? 6, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^

**1** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^

A single bit-field value with the data format:  |hex|

Bit-field
^^^^^^^^^^^^^^^^^^^^^^^^
.. csv-table:: GlobalStar Busy Bit-field
    :header: "Bits", "Values"
    :widths: 10, 50
    :file: ./rhm/gs-busy.csv

.. _tlm-rhm-gs-transmission-status:

GS Transmission Status
========================================

State of the GS transmission triggered by the :ref:`RHM:GS:SEND <rhm-gs-send>` SCPI command.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    RHM:TEL? 8, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^

**1** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^

A single bit-field value with the data format:  |hex|

Values
^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: GlobalStar Transmission Status Values
    :header: "Value", "Purpose"
    :widths: 10, 50
    :file: ./rhm/gs-trans-status.csv

.. _tlm-rhm-gs-transmissions-sent:

GS Transmissions Sent
========================================

Number of successful GS beacons sent.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    RHM:TEL? 9, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^

**4** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u32|

.. _tlm-rhm-gs-transmissions-failed:

GS Transmissions Failed
========================================

Number of beacons not sent due to NACK or lack of GS response.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    RHM:TEL? 10, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^

**4** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u32|

.. highlight:: python

Deprecated Telemetry
=====================

Below is a listing of telemetry to be removed in a future revision:

.. csv-table::
    :header: "Index","Name","Reason"
    :widths: 10,30,60

    "7","WDT Period","Feature not supported on current revision."