**************************
Telemetry
**************************

.. highlight:: none

The INDI telemetry set contains information for:

* Reading current readings from the 12V, 5V and Full-bridge motor driver circuitry
* Reading voltage on the Flyback converter
* Reading temperature sensors #1-4

.. _tlm-indi-12v-current:

12V current in mA
========================================

Current reading in **mA** on the 12V line powering the INDI controller board

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    INDI:TEL? 0, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u16|

.. _tlm-indi-5v-current:

5V current in mA
========================================


Current reading in **mA** on the 5V line powering the INDI controller board

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    INDI:TEL? 1, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u16|

.. _tlm-indi-full-bridge-current:

Full bridge Current in mA
========================================

Current reading in **mA** for the Full-bridge motor control circuitry controlling the shutter motor.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    INDI:TEL? 2, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u16|

.. _tlm-indi-flyback-voltage:

Flyback voltage in mV
========================================

Voltage reading in **mV** on the flyback converter used for powering the neon lamp

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    INDI:TEL? 3, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |i16|

.. _tlm-indi-temperature-1:

Temp sensor 1 reading (0.1K)
========================================


Temperature reading in **0.1K** from temperature sensor #1 connected to the INDI controller board

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    INDI:TEL? 4, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u16|

.. _tlm-indi-temperature-2:

Temp sensor 2 reading (0.1K)
========================================


Temperature reading in **0.1K** from temperature sensor #2 connected to the INDI controller board

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    INDI:TEL? 5, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u16|

.. _tlm-indi-temperature-3:

Temp sensor 3 reading (0.1K)
========================================

Temperature reading in **0.1K** from temperature sensor #3 connected to the INDI controller board

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    INDI:TEL? 6, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u16|

.. _tlm-indi-temperature-4:

Temp sensor 4 reading (0.1K)
========================================

Temperature reading in **0.1K** from temperature sensor #4 connected to the INDI controller board

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    INDI:TEL? 7, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u16|
