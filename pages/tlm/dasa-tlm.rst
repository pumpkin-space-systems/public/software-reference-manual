***********************
Telemetry
***********************

.. highlight:: none

DASA Telemetry set includes:

* Drive motor status, position/angle.
* Pin-puller status
* DASA module status
* Temperature readings

.. _tlm-dasa-drive-motor-position:

Drive Motor Position
======================================

The current step position of the DASA array.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    DASA:TEL? 0, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^

**4** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |i32|

.. _tlm-dasa-drive-motor-angle:

Drive Motor Angle
======================================

The current angle of the DASA array.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    DASA:TEL? 1, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^

**4** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |f32|

.. _tlm-dasa-drive-motor-status:

Drive Motor Status
======================================

The current state of the articulated array.
Contains direction of movement, if articulated array is moving, and if the articulated device has been homed.
Also contains the current draw of A/B drive circuitry.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    DASA:TEL? 2, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^

**5** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |hex|, |u16|, |u16|

Values
^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Drive Motor Status Register Bit-field
    :header: "Bits", "Values"
    :widths: 10, 50
    :file: ./dasa/motor-status.csv

.. _tlm-dasa-drive-motor-status-register:

Status Register Bit-field
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Drive Motor Status Register Bit-field
    :header: "Bits", "Values"
    :widths: 10, 50
    :file: ./dasa/motor-status-register.csv

.. _tlm-dasa-tini-pin-puller-status:

TiNi Pin-Puller status
======================================

The current state of the solar panel release mechanism.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    DASA:TEL? 3, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^

**5** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u8|, |u16|, |u16|

Values
^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Panel Release Status Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./dasa/release-status.csv

.. _tlm-dasa-npr-status:

NPR Status [enabled, mV]
======================================

The current state of the negative power rail for the temperature sensors connected to the DASA module.
Enable to increase range.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    DASA:TEL? 4, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^

**3** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u8|, |i16|

Values
^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: NPR Status Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./dasa/npr-status.csv

.. _tlm-dasa-hes-status:

HES Status [enabled, mV]
======================================

The current state of the Hall-effect sensor used to home the DASA array.
Controlled by DASA:HES ENABle and DASA:HES DISAble.

.. note:: The Hall-effect sensor will always be enabled during a DASA:STEP/ROTATE HOME command.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    DASA:TEL? 5, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^

**3** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^

Set of values with the data format:  |hex|, |u16|

Values
^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: HES Status Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./dasa/hes-status.csv

.. _tlm-dasa-temperature-1:

Temperature 1 [0.1 K]
======================================

Reading of the #1 temperature sensor.
Units are ``[0.1 K]``.
Use ``DASA:NPR ENable``, otherwise the reading will be ``0``

.. note:: In standard DASA build configurations, this returns the SupMCU's internal temperature in ``0.1K``.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    DASA:TEL? 6, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u16|

.. _tlm-dasa-temperature-2:

Temperature 2 [0.1 K]
======================================

Reading of the temperature sensor.
Units are [0.1 K].
Use ``DASA:NPR ENable``, otherwise the reading will be ``0``

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    DASA:TEL? 7, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u16|

.. _tlm-dasa-temperature-3:

Temperature 3 [0.1 K]
======================================

Reading of the temperature sensor.
Units are [0.1 K].
Use ``DASA:NPR ENable``, otherwise the reading will be ``0``

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    DASA:TEL? 8, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u16|

.. _tlm-dasa-temperature-4:

Temperature 4 [0.1 K]
======================================

Reading of the temperature sensor.
Units are [0.1 K].
Use ``DASA:NPR ENable``, otherwise the reading will be ``0``

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    DASA:TEL? 9, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u16|

.. highlight:: python