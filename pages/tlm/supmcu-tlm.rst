*******************************
Telemetry
*******************************

.. highlight:: none

The SupMCU Telemetry set includes items for:

* Module firmware version
* SCPI command success & rejected counts
* MCU State
* MCU Health (temperature/NVM writes)
* RTOS State
* Module information

All telemetry listed in ``SupMCU Telemetry`` is present across all modules.

.. _tlm-sup-firmware:

Firmware Version
===========================================

The current version of firmware loaded onto the |SupMCU| module.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    SUP:TEL? 0, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^

**48** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |string|

.. _tlm-sup-scpi-commands-parsed:

SCPI Commands Parsed
===========================================

The amount of successful SCPI commands processed.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    SUP:TEL? 1, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^

**8** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u64|

.. _tlm-sup-scpi-errors:

SCPI Errors (SCPI commands rejected)
===========================================

The amount of rejected SCPI commands received.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    SUP:TEL? 2, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^

**8** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u64|

.. _tlm-sup-cpu-selftests:

SupMCU CPU Selftests
===========================================

The results of the :ref:`supervisor-selftest` test code.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    SUP:TEL? 4, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^

**22** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u64|, |u64|, |i16|, |i16|, |i16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^^
.. csv-table:: SupMCU CPU Selftests Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./supmcu/supmcu-cpu-selftests.csv

.. _tlm-sup-elapsed-time:

Elapsed time in seconds
===========================================

Amount of time in seconds since the power on of the Module.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    SUP:TEL? 5, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^

**8** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u64|

.. _tlm-sup-context-switching:

Context Switching Occurred
===========================================

How many context switches Salvo RTOS has made.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    SUP:TEL? 6, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^

**8** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u64|

.. _tlm-sup-idling-hooks:

Idling Hooks Remaining
===========================================

How many times the Salvo RTOS has scheduled the idle task.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    SUP:TEL? 7, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^

**8** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u64|

.. _tlm-sup-mcu-load:

MCU Load
===========================================

Percentage of load the MCU is under.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    SUP:TEL? 8, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^

**4** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |f32|

.. _tlm-sup-serial-number:

Serial Number
===========================================

The serial number assigned in NVM memory of the module.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    SUP:TEL? 9, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u16|

.. _tlm-sup-i2c-address:

I2C Address
===========================================

The I2C Address of the module.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    SUP:TEL? 10,[param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^

**1** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u8|

.. _tlm-sup-oscillator-tuning-value:

Oscillator Tuning Value
===========================================

The value set by ``SUP:OSC`` SCPI command

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    SUP:TEL? 11,[param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^

**1** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |i8|

.. _tlm-sup-nvm-write-cycles:

NVM Write Cycles
===========================================

The number of times the NVM memory has been written to.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    SUP:TEL? 12,[param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u16|

.. _tlm-sup-pic24-rcon-reset:

PIC24 RCON reset cause
===========================================

The cause of reset read upon power on of the module.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    SUP:TEL? 13, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u16|

.. _tlm-sup-number-of-telemetry-items:

Number of Telemetry Items [SupMCU, Module]
===========================================

Number of telemetry items.
First value is the number of |SupMCU| telemetry items.
Second is the number of module specific telemetry values.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    SUP:TEL? 14, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^

**4** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|

.. _tlm-sup-supmcu-temperature:

SupMCU (CTMU) Temperature [0.1K]
===========================================

The current temperature of the MCU in ``.1 K`` units.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    SUP:TEL? 15, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u16|

.. _tlm-sup-supmcu-nvm-bc:

SupMCU NVM Boot Counter
===========================================

The current boot counter that is stored in non-volatile memory (NVM). 
The NVM boot counter (when enabled) increments by one each time the
SupMCU (re)starts. The NVM boot counter is enabled via bit 0 of the
SupMCU configuration word; it is **not** enabled by default. The NVM
boot counter can be reset via the ``SUP:NCM BC,RESET`` command, followed by
a write to NVM. 


SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    SUP:TEL? 21, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u16|

.. note:: 
    The maximum value of the NVM boot counter is limited to **128**.

.. _tlm-sup-supmcu-ram-bc:

SupMCU RAM Boot Counter
===========================================

The current boot counter that is stored in RAM. 
The RAM boot counter increments by one each time the
SupMCU (re)starts. The RAM boot counter is always enabled. The RAM
boot counter can be reset via the ``SUP:BC,RESET`` command. 

.. note:: 
    The SupMCU's RAM has no battery backup; therefore the RAM boot
    boot counter will take on a random value after a POR startup. Enable 
    and use the NVM boot counter (:ref:`supervisor-nvm`) in situations 
    where a boot counter must persist over successive power cycles.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    SUP:TEL? 22, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u16|

.. highlight:: python