==========================
Telemetry
==========================

.. highlight:: none

The BM2 telemetry set is responsible for:

* Reading the voltages of the pack and individual cells
* Reading the current telemetry of the pack.
* Reading status information from the BQ34z Gas Gauge chip

.. note:: The telemetry set from the BM2 module is derived from the BQ34Z653 Technical Reference Manual.

    See the `BQ34Z653 Technical Reference here <https://www.ti.com/lit/ug/sluu986a/sluu986a.pdf>`_.

.. _bm-telemetry-sensor-layout:

Temperature Sensor Layout
==========================

The physical mapping of temperature sensors to battery cells is as follows:

.. figure:: /img/bm2_temp_layout.png

    Temperature mappings

The relevant telemetry items are:

+-------------+-----------------------+
| Cell Number |     Telemetry Item    |
+=============+=======================+
| Cell #1     | :ref:`bm-cell-temp-1` |
+-------------+-----------------------+
| Cell #2     | :ref:`bm-cell-temp-2` |
+-------------+-----------------------+
| Cell #3     | :ref:`bm-cell-temp-3` |
+-------------+-----------------------+
| Cell #4     | :ref:`bm-cell-temp-4` |
+-------------+-----------------------+
| Cell #5     | :ref:`bm-cell-temp-5` |
+-------------+-----------------------+
| Cell #6     | :ref:`bm-cell-temp-6` |
+-------------+-----------------------+
| Cell #7     | :ref:`bm-cell-temp-7` |
+-------------+-----------------------+
| Cell #8     | :ref:`bm-cell-temp-8` |
+-------------+-----------------------+

The other temperature telemetry items include:

* :ref:`bm-temperature` -- Average of Temp #1 and Temp #2, BQ uses this temperature to gauge decisions about whether to allow charge or not.
* :ref:`bm-temperature-range` -- ``Temperature Range`` field in the BQ chip
* :ref:`bm-board-temperature` -- Temperature reading of sensor directly mounted on the BM2 controller board

.. note:: The ``Cell Temperature #`` telemetry will read ``External Temp Sensor #`` when queried for the ``BM:TEL? #,NAME`` data.

    The ``External Temp Sensor #`` name is present in BM2 firmware version ``1.5.X`` and below.

.. _bm-cell-temp-1:

Cell Temperature 1 [0.1K]
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The reading of the 1st temperature sensor on the BM2.

SCPI Command
++++++++++++++++++++++++++++++++++

.. code-block::

    BM:TEL? 48, [param]


Length
++++++++++++++++++++++++++++++++++

**2** bytes

Data Format
++++++++++++++++++++++++++++++++++

A single value with the data format: |u16|


.. _bm-cell-temp-2:

Cell Temperature 2 [0.1K]
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The reading of the 2nd temperature sensor on the BM2.

SCPI Command
++++++++++++++++++++++++++++++++++

.. code-block::

    BM:TEL? 49, [param]


Length
++++++++++++++++++++++++++++++++++

**2** bytes

Data Format
++++++++++++++++++++++++++++++++++

A single value with the data format: |u16|

.. _bm-cell-temp-3:

Cell Temperature 3 [0.1K]
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The reading of the 3rd temperature sensor on the BM2.

SCPI Command
++++++++++++++++++++++++++++++++++

.. code-block::

    BM:TEL? 50, [param]

Length
++++++++++++++++++++++++++++++++++

**2** bytes

Data Format
++++++++++++++++++++++++++++++++++

A single value with the data format: |u16|

.. _bm-cell-temp-4:

Cell Temperature 4 [0.1K]
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The reading of the 4th temperature sensor on the BM2.

SCPI Command
++++++++++++++++++++++++++++++++++

.. code-block::

    BM:TEL? 51, [param]


Length
++++++++++++++++++++++++++++++++++

**2** bytes

Data Format
++++++++++++++++++++++++++++++++++

A single value with the data format: |u16|

.. _bm-cell-temp-5:

Cell Temperature 5 [0.1K]
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Temperature in ``1/10 K`` per unit for temperature sensor #5.

SCPI Command
++++++++++++++++++++++++++++++++++

.. code-block::

    BM:TEL? 71, [param]


Length
++++++++++++++++++++++++++++++++++

**2** bytes

Data Format
++++++++++++++++++++++++++++++++++

A single value with the data format: |u16|

Values
++++++++++++++++++++++++++++++++++

Values are to be ignored if ``0``, this indicates the temperature sensor is disconnected.

.. _bm-cell-temp-6:

Cell Temperature 6 [0.1K]
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Temperature in ``1/10 K`` per unit for temperature sensor #6.

SCPI Command
++++++++++++++++++++++++++++++++++

.. code-block::

    BM:TEL? 72, [param]


Length
++++++++++++++++++++++++++++++++++

**2** bytes

Data Format
++++++++++++++++++++++++++++++++++

A single value with the data format: |u16|

Values
++++++++++++++++++++++++++++++++++

Values are to be ignored if ``0``, this indicates the temperature sensor is disconnected.

.. _bm-cell-temp-7:

Cell Temperature 7 [0.1K]
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Temperature in ``1/10 K`` per unit for temperature sensor #7.

SCPI Command
++++++++++++++++++++++++++++++++++

.. code-block::

    BM:TEL? 73, [param]


Length
++++++++++++++++++++++++++++++++++

**2** bytes

Data Format
++++++++++++++++++++++++++++++++++

A single value with the data format: |u16|

Values
++++++++++++++++++++++++++++++++++

Values are to be ignored if ``0``, this indicates the temperature sensor is disconnected.

.. _bm-cell-temp-8:

Cell Temperature 8 [0.1K]
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Temperature in ``1/10 K`` per unit for temperature sensor #8.

SCPI Command
++++++++++++++++++++++++++++++++++

.. code-block::

    BM:TEL? 74, [param]


Length
++++++++++++++++++++++++++++++++++

**2** bytes

Data Format
++++++++++++++++++++++++++++++++++

A single value with the data format: |u16|

Values
++++++++++++++++++++++++++++++++++

Values are to be ignored if ``0``, this indicates the temperature sensor is disconnected.

.. _bm-temperature:

Temperature [0.1K]
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Returns an unsigned integer value of the temperature in units of 0.1 K, as measured by the bq34z653. An average of :ref:`bm-cell-temp-1` and :ref:`bm-cell-temp-2` readings.

**This telemetry is used by the bq34z653 to make decisions about if it's too cold to charge or not**

SCPI Command
++++++++++++++++++++++++++++++++++

.. code-block::

    BM:TEL? 8, [param]

Data Format
++++++++++++++++++++++++++++++++++

A single value with the data format: |u16|

.. _bm-board-temperature:

Board Temperature [0.1K]
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Value read from the temperature sensor mounted on the BM2 RevF2 board, in 0.1K.

SCPI Command
++++++++++++++++++++++++++++++++++

.. code-block::

    BM:TEL? 115, [param]

Length
++++++++++++++++++++++++++++++++++

**2** bytes

Data Format
++++++++++++++++++++++++++++++++++

A single value with the data format: |u16|

Values
++++++++++++++++++++++++++++++++++

The measured temperature of the controller board.

.. _bm-temperature-range:

Temperature Range
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Value read from the ``TempRange`` register of the BQ34Z Gas Gauge chip.

See the Technical reference manual for more information.

SCPI Command
++++++++++++++++++++++++++++++++++

.. code-block::

    BM:TEL? 114, [param]


Length
++++++++++++++++++++++++++++++++++

**2** bytes

Data Format
++++++++++++++++++++++++++++++++++

A single value with the data format: |u16|

Gas Gauge Firmware Version
====================================================

Reads the currently loaded firmware on the Gas Gauge chip used to regulate the DSG and CHG operation of the BM2.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 0, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**32** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format: |str|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The loaded Gas Gauge firmware.

Voltage [mV]
====================================================

Sum of the cell voltages on the BM2 unit

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 9, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^

A single value with the data format: |u16|

Values
^^^^^^^^^^^

The value of the sum of the individual cell voltage measurements in mV, with a range of 0 to 20,000mV

Current [mA]
====================================================

Current into/out of the pack, taken as an average of 4 measurements over a period of 1 second

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 10, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^

A single value with the data format: |i16|

Value
^^^^^^^^^^^

An integer value of the measured current being supplied (or accepted) by the battery in mA, with a range of
–32,768 to 32,767. A positive value indicates charge current and a negative value indicates discharge.Any
current value within Deadband is reported as 0 mA by the Current function.

Average Current [mA]
====================================================

A rolling average of the current over the span of one minute.

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 11, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^

A single value with the data format: |i16|

Values
^^^^^^^^^^^

A signed integer value that approximates a one-minute rolling average of the current being supplied
(or accepted) through the battery terminals in mA, with a range of –32,768 to 32,767.

Average Current is calculated by a rolling IIR filtered average of Current function data with a period of 14.5s.
During the time after a reset and before 14.5s has elapsed, the reported AverageCurrent=Current function value.

Relative State Of Charge [%]
====================================================

The state of charge of the battery module. A value of 100% is full charge.

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 13, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**1** bytes

Data Format
^^^^^^^^^^^^

A single value with the data format: |u8|

Values
^^^^^^^^^^^

This read-word function returns an unsigned integer value of the predicted remaining battery capacity expressed as
a percentage of FullCharge Capacity with a range of 0 to 100%, with fractions of % rounded up.

Absolute State Of Charge [%]
====================================================

Charge left in battery relative to the design capacity of the battery. Note this value can be greater than 100%

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 14, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**1** bytes

Data Format
^^^^^^^^^^^^

A single value with the data format: |u8|

Values
^^^^^^^^^^^

Charge left in battery using the design capacity as the reference.

Remaining Capacity [10mWh]
====================================================

Remaining capacity of the BM2 measured in 10 mWh per unit.

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 15, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^

A single value with the data format: |u16|

Values
^^^^^^^^^^^

An unsigned integer value, with a range of 0 to 65,535, of the predicted charge or energy remaining in the battery.

Full Charge Capacity [10mWh]
====================================================

The pack capacity when the unit is fully charged, measured in 10 mWh per unit.

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 16, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^

A single value with the data format: |u16|

Values
^^^^^^^^^^^

An unsigned integer value of the predicted pack capacity when it is fully charged.

Run Time To Empty [min]
====================================================

The amount of time til the battery is discharged completely at the current rate of discharge.
A value of 65535 indicates the battery is not being discharged.

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 17, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^

A single value with the data format: |u16|

Values
^^^^^^^^^^^

An unsigned integer value of the predicted remaining battery life at the present rate of discharge,in minutes,with a
range of 0 to 65,534 minutes. A value of 65,535 indicates that the battery is not being discharged.

Average Time To Empty [min]
====================================================

The amount of time in minutes, based off average current, til the battery is discharged.

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 18, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^

A single value with the data format: |u16|

Values
^^^^^^^^^^^

An unsigned integer value of the predicted remaining battery life, in minutes,based on AverageCurrent, with a range
of 0 to 65,534. A value of 65,535 indicates that the battery is not being discharged.

Average Time To Full [min]
====================================================

The amount of time til the battery is fully charged at the current rate. A value of 65535 indicates no charge.

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 19, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^

A single value with the data format: |u16|

Values
^^^^^^^^^^^

An unsigned integer value of predicted remaining time until the battery reaches full charge, in minutes, based on AverageCurrent,
with a range of 0 to 65,534. A value of 65,535 indicates that the battery is not being charged.

Charging Current [mA]
====================================================

The desired charging current to use for the battery.

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 20, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^

A single value with the data format: |u16|

Values
^^^^^^^^^^^

An unsigned integer value of the desired charging current, in mA, with a range of 0 to 65,534. A value of 65,535 indicates
that a charger should operate as a voltage source outside its maximum regulated current range.

Charging Voltage [mV]
====================================================

The designed charging voltage of the BM2.

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 21, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^

A single value with the data format: |u16|

Values
^^^^^^^^^^^

An unsigned integer value of the desired charging voltage, in mV, where the range is 0 to 65,534.A value of 65,535
indicates that the charger should operate as a current source outside its maximum regulated voltage range.

Battery Status [hex code]
====================================================

Expresses the status of the battery as two separate bit-fields. See ``BatteryStatus (0x16)`` in the Technical Reference of the BQ34Z

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 22, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^

A multi-value field with the data format and order: |u8|, |u8|

Values
^^^^^^^^^^^^^^^^^^^

.. csv-table:: Fields
    :header: "Description","Data Type","Position"
    :widths: 60, 20, 20

    "The high byte of the status","|u8|","0"
    "The low byte of the status","|u8|","1"

Cycle Count
====================================================

The amount of cycles the battery has experienced.

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 23, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^

A single value with the data format: |u16|

Values
^^^^^^^^^^^

the number of cycles the battery has experienced,with a range of 0 to 65,535.The default value is stored in the data
flash value CycleCount, which is updated each time this variable is incremented. One cycle count is the accumulated
discharge of CC Threshold.


Design Capacity [10mWh]
====================================================

The designed capacity of the battery in 10 mWh per unit.

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 24, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^

A single value with the data format: |i16|

Values
^^^^^^^^^^^

The design capacity of the battery as programmed in the BQ34Z flash.

Design Voltage [mV]
====================================================

The theoretical designed voltage of a new pack.

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 25, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^

A single value with the data format: |i16|

Values
^^^^^^^^^^^

An unsigned integer value of the theoretical voltage of a new pack, in mV, with a range of 0 to 65,535.
The default value is stored in DesignVoltage.


Safety Alert Registers
====================================================

The Safety alert registers, see ``SafetyAlert`` & ``SafteyAlert2`` in the BQ34Z technical reference

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 28, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**4** bytes

Data Format
^^^^^^^^^^^

A multi-value field with the data format and order: |u8|, |u8|, |u8|, |u8|

Values
^^^^^^^^^^^^^^^^^^^

.. csv-table:: Fields
    :header: "Description","Data Type","Position"
    :widths: 60, 20, 20

    "SafetyAlert High Byte","|u8|","0-0"
    "SafetyAlert Low Byte","|u8|","1-1"
    "SafetyAlert2 High Byte","|u8|","2-2"
    "SafetyAlert2 Low Byte","|u8|","3-3"

Safety Status Registers
====================================================

Returns the status of the 1st level safety features. See the ``SafetyStatus`` and ``SafetyStatus2`` registers in the
BQ34Z technical reference manual.

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 29, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**4** bytes

Data Format
^^^^^^^^^^^

A multi-value field with the data format and order: |u8|, |u8|, |u8|, |u8|

Values
^^^^^^^^^^^^^^^^^^^

.. csv-table:: Fields
    :header: "Description","Data Type","Position"
    :widths: 60, 20, 20

    "SafetyStatus High Byte","|u8|","0-0"
    "SafetyStatus Low Byte","|u8|","1-1"
    "SafetyStatus2 High Byte","|u8|","2-2"
    "SafetyStatus2 Low Byte","|u8|","3-3"

Charging Status Register
====================================================

``ChargingStatus`` and ``ChargingStatus2`` registers from the BQ34Z Gas Gauge. See the BQ34Z technical reference for more information.

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 30, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**4** bytes

Data Format
^^^^^^^^^^^

A multi-value field with the data format and order: |u8|, |u8|, |u8|, |u8|

Values
^^^^^^^^^^^^^^^^^^^

.. csv-table:: Fields
    :header: "Description","Data Type","Position"
    :widths: 60, 20, 20

    "ChargingStatus High Byte","|u8|","0-0"
    "ChargingStatus Low Byte","|u8|","1-1"
    "ChargingStatus2 High Byte","|u8|","2-2"
    "ChargingStatus2 Low Byte","|u8|","3-3"

PFAlert Registers
====================================================

``PFAlert`` and ``PFAlert2`` registers from the BQ34Z Gas Gauge. See the technical reference for more information.

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 31, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**4** bytes

Data Format
^^^^^^^^^^^

A multi-value field with the data format and order: |u8|, |u8|, |u8|, |u8|

Values
^^^^^^^^^^^^^^^^^^^

.. csv-table:: Fields
    :header: "Description","Data Type","Position"
    :widths: 60, 20, 20

    "PFAlert High Byte","|u8|","0-0"
    "PFAlert Low Byte","|u8|","1-1"
    "PFAlert2 High Byte","|u8|","2-2"
    "PFAlert2 Low Byte","|u8|","3-3"

PFStatus Registers
====================================================

``PFStatus`` and ``PFStatus2`` registers from the BQ34Z Gas Gauge. See the technical reference for more information.

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 32, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**4** bytes

Data Format
^^^^^^^^^^^

A multi-value field with the data format and order: |u8|, |u8|, |u8|, |u8|

Values
^^^^^^^^^^^^^^^^^^^

.. csv-table:: Fields
    :header: "Description","Data Type","Position"
    :widths: 60, 20, 20

    "PFStatus High Byte","|u8|","0-0"
    "PFStatus Low Byte","|u8|","1-1"
    "PFStatus2 High Byte","|u8|","2-2"
    "PFStatus2 Low Byte","|u8|","3-3"

Last Minimum pack voltage [mV]
====================================================

Last detected minimum pack voltage.

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 33, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^

A single value with the data format: |u16|

Values
^^^^^^^^^^^

This is the last recorded minimum pack voltage. Value is reset upon module reset.

Last Maximum pack voltage [mV]
====================================================

This is the last recorded maximum pack voltage.

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 34, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^

A single value with the data format: |u16|

Values
^^^^^^^^^^^

The last recorded max pack voltage. Value is reset upon module reset.

Approx State of Charge [%]
====================================================

The approximate state of charge based off of pack voltage.

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 36, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**1** bytes

Data Format
^^^^^^^^^^^^

A single value with the data format: |u8|

Values
^^^^^^^^^^^

Value is derived by the pack voltage and cell chemistry used.


Current Compensated Voltage [mV]
====================================================

Returns the smoothed voltage, factoring in the calculated internal pack resistance.

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 38, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^

A single value with the data format: |u16|

Values
^^^^^^^^^^^

The value is calculated using the average current and voltage of the BQ34Z chip

SupMCU Status Register
====================================================

A series of bit-flags to express the state of the BM2 pack

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 52, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**1** bytes

Values
^^^^^^^^^^^^^^^^^^^^^^^

The following fields are encoded in a **|u8|**:

.. csv-table:: Encoded Fields
    :header: "Description","Position"
    :widths: 80, 20

    "Safe Signal from GG","0"
    "Alert Signal from GG","1"
    "Cell OV Protection Enabled","2"
    "Unused","3"
    "SMB Pull-up Sense","4"
    "Reserved","5-7"

Time of last Permanent Failure
====================================================

Returns the uptime count of the SupMCU module of when the last PF was asserted by the BQ34Z chip.

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 53, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**28** bytes

Data Format
^^^^^^^^^^^^

A single value with the data format: |str|

Values
^^^^^^^^^^^

Format of output is ``[<seconds>:<ticks>]`` where ticks is ``1/300th`` of a second.

Registers for last PF
====================================================

The ``PFAlert`` and ``PFAlert2`` registers recorded for the last PF.

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 54, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**4** bytes

Values
^^^^^^^^^^^

Field Layout
^^^^^^^^^^^^^^^^^^^

.. csv-table:: Fields
    :header: "Values","Data Type","Position"
    :widths: 60, 20, 20

    "PFAlert High Byte","|u8|","0-0"
    "PFAlert Low Byte","|u8|","1-1"
    "PFAlert2 High Byte","|u8|","2-2"
    "PFAlert2 Low Byte","|u8|","3-3"

.. _bm-tlm-smb-read:

Data from requested SMB Read
====================================================

Data requested from the last BQSMB Read command.

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 55, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**45** bytes

Values
^^^^^^^^^^^

**32-byte** array of the requested SMB read.

Data from requested Flash Entry
====================================================

Data from requested BQFlash command.

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 56, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**45** bytes

Values
^^^^^^^^^^^

**32 byte** |u8| array of the flash entry data

.. _bm-tlm-ma-entry:

Data from requested MA Entry
====================================================

Data from requested ManufacturerAccess read command

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 57, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u8|, |u8|

Values
^^^^^^^^^^^^^^^^^^^

.. csv-table:: Fields
    :header: "Description","Data Type","Position"
    :widths: 60, 20, 20

    "The ManufacturerAccess Register High byte","|u8|","0-0"
    "The ManufacturerAccess Register Low byte","|u8|","1-1"

Data from last function call
====================================================

Data returned from select BM:FUNCtion calls.

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 58, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**8** bytes

Data Format
^^^^^^^^^^^^^^^^^^

A **8-byte** |u8| array.


Cell Voltage 4 [mV]
====================================================

The voltage on Cell #4 in mV.

For ``2S4P`` and ``3S2P`` this value will read zero.

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 60, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^

A single value with the data format: |u16|

Values
^^^^^^^^^^^

Values typically range from ``~2800 mV`` to ``~4200 mV`` for cells.

Cell Voltage 3 [mV]
====================================================

The voltage on Cell #3 in mV.

For ``2S4P`` this value will read zero.

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 61, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^

A single value with the data format: |u16|

Values
^^^^^^^^^^^

Values typically range from ``~2800 mV`` to ``~4200 mV`` for cells.

Cell Voltage 2 [mV]
====================================================

The voltage on Cell #2 in mV.

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 62, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^

A single value with the data format: |u16|

Values
^^^^^^^^^^^

Values typically range from ``~2800 mV`` to ``~4200 mV`` for cells.

Cell Voltage 1 [mV]
====================================================

The voltage on Cell #1 in mV.

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 63, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^

A single value with the data format: |u16|

Values
^^^^^^^^^^^

Values typically range from ``~2800 mV`` to ``~4200 mV`` for cells.

Status of Cell Balancing system
====================================================

Status of the cell balancing circuitry used to discharge the individual cells to match the cell voltages.

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 77, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**1** bytes

Values
^^^^^^^^^^^^^^^^^^^^^^^

The following fields are encoded in a **|u8|**:

.. csv-table:: Encoded Fields
    :header: "Description","Position"
    :widths: 80, 20

    "Cell 1 Balance Circuit Enabled","0"
    "Cell 2 Balance Circuit Enabled","1"
    "Cell 3 Balance Circuit Enabled","2"
    "Cell 4 Balance Circuit Enabled","3"
    "Unused","4-7"

Data from requested Flash Page
====================================================

The data requested from the BQFlash read command for a whole page.

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 78, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**46** bytes

Data Format
^^^^^^^^^^^^^^^^^^

A byte array (|u8|) of 32 bytes of the data flash page read.

Operation Status Register
====================================================

The value of the OperationStatus register on the BQ34Z Gas Gauge chip.

See the BQ34Z Technical Reference for bitfield layout.

    Operation Status information from the reference manual

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 84, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u8|, |u8|

Values
^^^^^^^^^^^^^^^^^^^

.. csv-table:: Fields
    :header: "Values","Data Type","Position"
    :widths: 60, 20, 20

    "OperationStatus High Byte","|u8|","0-0"
    "OperationStatus Low Byte","|u8|","1-1"

Pack Voltage [mV]
====================================================

The voltage as measured on the ``PACK`` pin of the Gas Gauge chip. May vary slighty from the ``Voltage [mV]``
telemetry.

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 90, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^

A single value with the data format: |u16|

Values
^^^^^^^^^^^

Voltage on the ``PACK`` pin of the GG chip. Should closely follow ``Voltage [mV]`` of the pack.

Average Voltage [mV]
====================================================

Rolling one minute average for the sum cell voltages.

SCPI Command
^^^^^^^^^^^^^^^^^

.. code-block::

    BM:TEL? 93, [param]


Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^

A single value with the data format: |u16|

Deprecated/Unused Telemetry
===========================

Below is a listing of the unused/deprecated telemetry items. Note that unused/Reserved items return undefined data.

.. csv-table:: Deprecated/Unused Telemetry
    :header: "Index", "Name", "Reason"
    :widths: 10, 30, 60

    37, "Pack Internal Resistance [0.1mOh]", "For internal/development usage. Removed in future firmware"
    75, "Temp offsets for linear fit", "Not used"
    76, "Temp linear fit scaling factors", "Not used"
    80, "Safety Alert Reg (Deprecated)", "Use Safety Alert Registers"
    81, "Safety Status Reg (Deprecated)", "Use Safety Status Registers"
    82, "PF Alert Register (Deprecated)", "Use PF Alert Registers"
    83, "PF Status Register (Deprecated)", "Use PF Status Registers"
    85, "ChargingStatus Reg (Deprecated)", "Not used"
    94, "TS1 Temperature [0.1K]", "Use ``External Temp 1 Sensor [0.1K]`` instead"
    95, "TS2 Temperature [0.1K]", "Use ``External Temp 2 Sensor [0.1K]`` instead"
    96, "TS3 Temperature [0.1K]", "Use ``External Temp 3 Sensor [0.1K]`` instead"
    97, "TS4 Temperature [0.1K]", "Use ``External Temp 4 Sensor [0.1K]`` instead"
    104, "Safety Alert 2 Reg (Deprecated)", "Use Safety Alert Registers"
    105, "Safety Status 2 Reg (Deprecated)", "Use Safety Status Registers"
    106, "PF Alert 2 Reg (Deprecated)", "Use PF Alert Registers"
    107, "PF Status 2 Reg (Deprecated)", "Use PF Status Registers"
    1, "Reserved", "Reserved for future use"
    2, "Reserved", "Reserved for future use"
    3, "Reserved", "Reserved for future use"
    4, "Reserved", "Reserved for future use"
    5, "Reserved", "Reserved for future use"
    6, "Reserved", "Reserved for future use"
    7, "Reserved", "Reserved for future use"
    12, "Reserved", "Reserved for future use"
    26, "Reserved", "Reserved for future use"
    27, "Reserved", "Reserved for future use"
    35, "Reserved", "Reserved for future use"
    39, "Reserved", "Reserved for future use"
    40, "Reserved", "Reserved for future use"
    41, "Reserved", "Reserved for future use"
    42, "Reserved", "Reserved for future use"
    43, "Reserved", "Reserved for future use"
    44, "Reserved", "Reserved for future use"
    45, "Reserved", "Reserved for future use"
    46, "Reserved", "Reserved for future use"
    47, "Reserved", "Reserved for future use"
    59, "Reserved", "Reserved for future use"
    64, "Reserved", "Reserved for future use"
    65, "Reserved", "Reserved for future use"
    66, "Reserved", "Reserved for future use"
    67, "Reserved", "Reserved for future use"
    68, "Reserved", "Reserved for future use"
    69, "Reserved", "Reserved for future use"
    70, "Reserved", "Reserved for future use"
    79, "Reserved", "Reserved for future use"
    86, "Reserved", "Reserved for future use"
    87, "Reserved", "Reserved for future use"
    88, "Reserved", "Reserved for future use"
    89, "Reserved", "Reserved for future use"
    91, "Reserved", "Reserved for future use"
    92, "Reserved", "Reserved for future use"
    98, "Reserved", "Reserved for future use"
    99, "Reserved", "Reserved for future use"
    100, "Reserved", "Reserved for future use"
    101, "Reserved", "Reserved for future use"
    102, "Reserved", "Reserved for future use"
    103, "Reserved", "Reserved for future use"
    108, "Reserved", "Reserved for future use"
    109, "Reserved", "Reserved for future use"
    110, "Reserved", "Reserved for future use"
    111, "Reserved", "Reserved for future use"
    112, "Reserved", "Reserved for future use"
    113, "Reserved", "Reserved for future use"