*******************************
Telemetry
*******************************

.. highlight:: none

The AIM Telemetry set contains:

* GPS state information
* ADCS state information
* Orbit propagator results
* GPS Power status

.. _tlm-aim-gps-status:

GPS Status
====================================

State values pertaining to GPS power, reset, position valid, and pass through.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    AIM:TEL? 0, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A single bit-field value with the data format:  |hex|

Bit-field
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: GPS Status Bit-field
    :header: "Bits", "Values"
    :widths: 10, 50

    "0","1 =  Power is applied to OEM719"
    "1","1 =  RESET signal is applied to OEM719"
    "2","1 =  Position Valid pin on OEM719 is HIGH (ON)"
    "3","1 =  Pass through is enabled on GPSRM."
    "4-7","Reserved (0)"

.. _tlm-aim-orbit-propagator-results:

Orbit Propagator Results
====================================

Results from Vinti7 or SGP4 orbit propagators.
See GPSRM SCPI Commands for usage. All zeros until propagator has ran.

.. note:: Invalid propagator input will set all values to 0 for propagator results.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    AIM:TEL? 2, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**56** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |f64|, |f64|, |f64|, |f64|, |f64|, |f64|, |f64|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Orbit Propagator Results Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50

    "0-7","Time t1 (Vinti7) or time_delta from initial time (TLE SGP4) (f64)"
    "8-15","X-position of propagated orbit (f64)"
    "16-23","Y-position of propagated orbit (f64)"
    "24-31","Z-position of propagated orbit (f64)"

.. _tlm-aim-oem719-power:

OEM719 Power [mW]
====================================

Power draw readings of the OEM719 voltage supply rails. Values are measured in milliwatts.

.. note:: See AIM-2 data sheet for more information on voltage rails.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    AIM:TEL? 3, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**8** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|, |u16|, |u16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: OEM719 Power [mW] Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50

    0-1,Power draw reading in milliwatts from VCC_SYS (u16)
    2-3,Power draw reading in milliwatt from +3V3_SYS (u16)
    4-5,Reserved (0)
    6-7,Reserved (0)

.. _tlm-aim-gps-uart-status:

GPS UART Status
====================================

The state of the OEM719 UART Communications port and if UART pass through is ON or OFF.
Use :ref:`aim-gps-comm` and :ref:`aim-gps-passthrough` commands to change these states.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    AIM:TEL? 4, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**1** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A single bit-field with the data format:  |u8|

Bit-field
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: GPS UART Status Bit-field
    :header: "Bits", "Values"
    :widths: 10, 50

    "0 - 2","GPS UART Port selection. Values are::

        0 = NONE
        1 = UART0
        2 = UART1
        3 = UART2
        4 = UART3
        6 = SUP1
        7 = SUP2"
    "3 - 6","Reserved (0)"
    "7","1 = Pass through is on."

.. _tlm-aim-gps-event-pin-status:

GPS Event Pin Status
====================================

The state of the EVENT1 and EVENT2 pins on the OEM719

.. note:: See the OEM719 Manual for `MARKxTIME <https://docs.novatel.com/oem7/Content/Logs/MARKxTIME.htm>`_ for EVENT1/2 purpose.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    AIM:TEL? 5, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**1** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A single bit-field value with the data format:  |u8|

Bit-field
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: GPS Event Status Bit-field
    :header: "Bits", "Values"
    :widths: 10, 50

    0,1 =  OEM719 EVENT1 is HIGH (ON)
    1,1 =  OEM719 EVENT2 is HIGH (ON)

.. _tlm-aim-gps-power-supply-status:

GPS Power Supply Status
====================================

Status of voltage rails that supply 3v3 volts to OEM719. Bits are HIGH (1) if NO current or faults are detected.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    AIM:TEL? 6, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**1** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A single bit-field value with the data format:  |u8|

Bit-field
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: GPS Power Supply Status Bit-field
    :header: "Bits", "Values"
    :widths: 10, 50

    "0","0 = Current is flowing from ``VCC_SYS`` to OEM719 3v3 Rail."
    "1","0 = Current is flowing from ``3V3_USB`` to OEM719 3v3 Rail."
    "2","0 = Current monitor fault (thermal/overcurrent) on supply from ``VCC_SYS``."
    "3","0 = Current monitor fault (thermal/overcurrent) on supply from ``3V3_USB``."

.. _tlm-aim-adcs-uart-status:

ADCS UART Status
====================================

The state of the UART communication to the ADCS Unit.
Use :ref:`aim-adcs-comm` and :ref:`aim-adcs-passthrough` commands to manipulate the state of UART communications to MAI-401.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    AIM:TEL? 7, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**1** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A single bit-field value with the data format:  |u8|

Bit-field
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: ADCS UART Status Bit-field
    :header: "Bits", "Values"
    :widths: 10, 50

    "0 - 2","ADCS UART Port selection::

        0 = NONE
        1 = UART0
        2 = UART1
        3 = UART2
        4 = UART3
        6 = SUP1
        7 = SUP2"
    "3 - 6","Reserved (0)"
    "7","1 = Passthrough is on."

.. _tlm-aim-ftdi-chip-status:

FTDI Chip Status
====================================

The state of the FTDI chip used to update firmware on the MAI-401 unit.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    AIM:TEL? 8, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**1** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A single bit-field value with the data format:  |u8|

Bit-field
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: FTDI Chip Status Bit-field
    :header: "Bits", "Values"
    :widths: 10, 50

    "0","1 =  FTDI  power on"
    "1","1 = FTDI suspend enabled"
    "2 - 7","Reserved (0)"

.. _tlm-aim-adcs-reset-and-power-status:

ADCS Reset and Power Status
====================================

The reset and power status of the MAI-401 unit.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    AIM:TEL? 9, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**1** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A single bit-field value with the data format:  |u8|

Bit-field
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: ADCS Reset and Power Status Bit-field
    :header: "Bits", "Values"
    :widths: 10, 50

    "0","1 = MAI-401 ADCS is on"
    "1","1 = MAI-401 ADCS Reset is on"
    "2 - 7","Reserved (0)"

.. highlight:: python

Deprecated Telemetry
====================

Below is a listing of telemetry to be removed in a future revision:

.. csv-table:: Deprecated Telemetry
    :header: "Index", "Name", "Reason"
    :widths: 10, 30, 60

    "1", "NMEA String", "Functionality not supported on latest revision, use direct UART to OEM719 unit"
    "10", "WDT Period", "Functionality not supported on latest revision."