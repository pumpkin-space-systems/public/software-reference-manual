*************************
Telemetry
*************************

.. highlight:: none

The PDB Telemetry set contains

* Port voltages, currents, overcurrents, and current limits
* Port shunts
* PDB status

.. _tlm-pdb-current:

Port Currents
==============================

The current for each PDB port.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    PDB:TEL? 0, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**6** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |i16|, |i16|, |i16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

The current of the three ports, in order, XLINK, thruster, payload in milliamps.

.. _tlm-pdb-shunts:

Port Shunts
==============================

The shunt value of each port.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    PDB:TEL? 1, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**6** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|, |u16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

The shunt value of the three ports, in order, XLINK, thruster, payload in microohms.

.. _tlm-pdb-ilimits:

Port Current Limits
==============================

The current limit for each PDB port.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    PDB:TEL? 2, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**6** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|, |u16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

The current limit of the three ports, in order, XLINK, thruster, payload in milliamps.

.. _tlm-pdb-status:

Status Register
==============================

The status register of the PDB.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    PDB:TEL? 3, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**1** byte

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A single-value field with the data format |u8|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

Bits 0-2 correspond to ports 1-3 (XLINK, thruster, payload) and will be 1 if the port is enabled and 0 otherwise.

.. _tlm-pdb-overcurrents:

Port Overcurrents
==============================

The overcurrent value of each port.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    PDB:TEL? 4, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**6** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|, |u16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

The overcurrent for each port, in order, XLINK, thruster, payload in milliamps. 

.. _tlm-pdb-voltages:

Port Voltages
==============================

The voltage of each port on the PDB.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    PDB:TEL? 5, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**6** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|, |u16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

The voltage for each of the three ports, in order, XLINK, thruster, payload in millivolts.

.. _tlm-pdb-combined:

Combined Telemetry
==============================

Combined Telemetry for the PDB.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    PDB:TEL? 6, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**31** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |i16|, |i16|, |i16|, |u16|, |u16|, |u16|, |u16|, |u16|, |u16|, |u16|, |u16|, |u16|, |u8|, |u64|, |u16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Combined Telemetry Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./pdb/combined-tlm.csv
