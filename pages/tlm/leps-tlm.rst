*************************
Telemetry
*************************

.. highlight:: none

The LEPS Telemetry set contains

* State of battery and power rails
* Voltage reference state information

.. _tlm-leps-bat1:

Battery Data
==============================

The current voltage, voltage set point, current, current limit and status for the battery.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    LEPS:TEL? 6, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**13** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|, |i16|, |i16|, |hex|, |i32|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Converter Data Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./leps/converter-data.csv

.. _tlm-leps-3v3:

3.3V Converter Data
==============================

The current voltage, voltage set point, current, current limit and status for 3.3V converter.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    LEPS:TEL? 8, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**13** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|, |i16|, |i16|, |hex|, |i32|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Converter Data Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./leps/converter-data.csv

.. _tlm-leps-5v:

5V Converter Data
==============================

The current voltage, voltage set point, current, current limit and status for 5V converter.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    LEPS:TEL? 9, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**13** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|, |i16|, |i16|, |hex|, |i32|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Converter Data Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./leps/converter-data.csv

.. _tlm-leps-vbatt:

VBATT Converter Data
==================================

The current voltage, voltage set point, current, current limit and status for VBATT converter.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    LEPS:TEL? 11, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**13** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|, |i16|, |i16|, |hex|, |i32|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Converter Data Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./leps/converter-data.csv

.. _tlm-leps-hsk-3v3-ref:

HSK 3.3V REF Data
==============================

The breakdown of the internal 3.3V REF voltage and current reference read from LEPS.

.. note:: These are the internal 3.3V readings from the LEPS.

    These are broken out for debugging purposes if current/voltage readings don't match expected output

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    LEPS:TEL? 19, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**4** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Reference Values Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./leps/reference-data.csv

.. _tlm-leps-hsk-5v-ref:

HSK 5V REF Data
==============================

The breakdown of the internal 5V REF voltage and current reference read from LEPS.

.. note:: These are the internal 5V readings from the LEPS.

    These are broken out for debugging purposes if current/voltage readings don't match expected output

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    LEPS:TEL? 20, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**4** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Reference Values Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./leps/reference-data.csv

.. _tlm-leps-sns-vusb-ref:

SNS VUSB REF Data
==============================

The voltage present on the USB input read from LEPS.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    LEPS:TEL? 33, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u16|

.. _tlm-leps-sns--reset-data:

SNS -RESET Data
==============================

The voltage present on SNS -RESET read from LEPS.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    LEPS:TEL? 34, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u16|

WDT Seconds Left
================

The number of seconds left before the WDT on the LEPS restarts the system.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    LEPS:TEL? 42, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u16|
