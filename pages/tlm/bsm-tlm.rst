***********************
Telemetry
***********************

.. highlight:: none

The BSM telemetry set contains:

* Channel currents, limits, shunt values, calibration values.
* Temperature sensor values, calibration values.
* Status and overcurrent registers

.. _tlm-bsm-channel-currents:

Channel Currents (mA)
====================================

Current readings from BSM Power ports #1-4 in miliamps (mA).

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    BSM:TEL? 0, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**8** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|, |u16|, |u16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Channel Currents Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50

    "0-1","Current reading from Port #1 (u16) in mA"
    "2-3","Current reading from Port #2 (u16) in mA"
    "4-5","Current reading from Port #3 (u16) in mA"
    "6-7","Current reading from Port #4 (u16) in mA"

.. _tlm-bsm-channel-current-limits:

Channel Current Limits (mA)
====================================

Current limits for BSM power ports in mA. Controlled by :ref:`BSM:NVM I_LIMIT <bsm-nvm>` command.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    BSM:TEL? 2, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**10** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|, |u16|, |u16|, |u16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Channel Current Limits Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50

    "0-1","Current limit value for Port #1 (u16) in mA"
    "2-3","Current limit value for Port #2 (u16) in mA"
    "4-5","Current limit value for Port #3 (u16) in mA"
    "6-7","Current limit value for Port #4 (u16) in mA"


.. _tlm-bsm-status-register:

BSM Status Register
====================================

State of power to Ports #1-4, and Revision of BSM.
Use :ref:`BSM:PORT:POW <bsm-port-power>` to control port power.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    BSM:TEL? 3, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**1** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A single bit-field value with the data format:  |u8|

Bit-field
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Status Register Bit-field
    :header: "Bits", "Values"
    :widths: 10, 50

    "0","Port 1 Status: 1 = on"
    "1","Port 2 Status: 1 = on"
    "2","Port 3 Status: 1 = on"
    "3","Port 4 Status: 1 = on"
    "4-7","RESERVED 0"

.. _tlm-bsm-overcurrent-event-log:

BSM Overcurrent event log (mA)
====================================

Log of last overcurrent event in BSM.
An over-current is generated when a device draws more current from a BSM port than the current limit.
If no over-current event has occurred, then the value is ``0``.
Turn the port explicitly off then back on via :ref:`bsm-port-power` command.

Resetting the BSM device resets the overcurrent log to all ``0's``.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    BSM:TEL? 4, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**8** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|, |u16|, |u16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Overcurrent event log Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50

    "0-1","Last current measurement on overcurrent fault for Port 1. 0 is no overcurrent event."
    "2-3","Last current measurement on overcurrent fault for Port 2. 0 is no overcurrent event."
    "4-5","Last current measurement on overcurrent fault for Port 3. 0 is no overcurrent event."
    "6-7","Last current measurement on overcurrent fault for Port 4. 0 is no overcurrent event."


Deprecated Telemetry
=====================

Below is a listing of telemetry to be removed in a future revision of firmware:

.. csv-table:: Deprecated Telemetry
    :header: "Index","Name","Reason"
    :widths: 10, 30, 60

    "1","Channel Shunt Resistor Values (µΩ)","Current sense no longer depends on stored resistor values."
