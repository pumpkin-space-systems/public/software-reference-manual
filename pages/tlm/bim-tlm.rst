*************************
Telemetry
*************************

.. highlight:: none

The BIM telemetry set is responsible for:

* Reading the 6 temperature sensors
* Checking the current state of the UART ports.
* Returning IMU data readouts (**not functional**)
* Querying the status of the Pin-Puller mechanism.

.. _tlm-bim-temperature:

Temperature [0.1 K]
=======================================

The temperature readings in **0.1 kelvin** from the 6 temperature sensors on the BIM.

.. note:: The temperature sensors must be powered for valid readings. Make sure ``BIM:TEMP:POW ON`` command has been sent before reading sensor data.

SCPI Command
^^^^^^^^^^^^^^^^^^

.. code-block::

    BIM:TEL? 0, [param]

Length
^^^^^^^^^^^^^^^^^^

**12** bytes

Data Format
^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u32|, |u32|, |u32|, |u32|, |u32|, |u32|

Values
^^^^^^^^^^^^^^^^^^

.. csv-table:: Fields
    :header: "Description","Data Type","Position"
    :widths: 60, 20, 20

    "Sensor 1","uint16_t","0-1"
    "Sensor 2","uint16_t","2-3"
    "Sensor 3","uint16_t","4-5"
    "Sensor 4","uint16_t","6-7"
    "Sensor 5","uint16_t","8-9"
    "Sensor 6","uint16_t","10-11"

.. _tlm-bim-uart-status:

UART Status
=======================================

UART power status for ports 1, 2 and 3 respectively.
A value of 1 indicates power is enabled, 0 is not enabled.

SCPI Command
^^^^^^^^^^^^^^^^^^

.. code-block::

    BIM:TEL? 1, [param]

Length
^^^^^^^^^^^^^^^^^^

**6** bytes

Data Format
^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |i16|, |i16|, |i16|

Values
^^^^^^^

.. csv-table:: Fields
    :header: "Description","Data Type","Position"
    :widths: 60, 20, 20

    "Port 1","uint16_t","0-1"
    "Port 2","uint16_t","2-3"
    "Port 3","uint16_t","4-5"

.. _tlm-bim-tini-pin-puller-status:

Tini pin-puller status
=======================================

The current status of the TiNi pin-puller mechanism on the BIM.

SCPI Command
^^^^^^^^^^^^^^^^^^

.. code-block::

    BIM:TEL? 3, [param]

Length
^^^^^^^^^^^^^^^^^^

**1** bytes

Data Format
^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u8|

Values
^^^^^^^^^^^^^^^^^^

If the value is ``1``, the pin-puller is energized. ``0`` for not energized.

.. highlight:: python


.. _tlm-bim-deprecated:

Deprecated Telemetry
=====================

Below is a listing of telemetry indices that are no longer supported and will be removed in future revisions:

.. csv-table:: Deprecated Telemetry
    :header: "Index", "Name", "Reason"
    :widths: 10, 30, 60

    "2","IMU Data","No longer supported"