*************************
Telemetry
*************************

.. highlight:: none

The EPSM/DCPS Telemetry set contains

* State of SAI inputs
* State of bus power rails
* Voltage reference state information
* Temperature data for SAI inputs.
* FPGA state information

.. _tlm-epsm-sai1:

SAI1 Converter Data
==============================

The current voltage, voltage set point, current, current limit and status of SAI1.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 0, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**13** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|, |i16|, |i16|, |hex|, |i32|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Converter Data Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./epsm/converter-data.csv

.. _tlm-epsm-sai2:

SAI2 Converter Data
==============================

The current voltage, voltage set point, current, current limit and status of SAI2.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 1, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**13** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|, |i16|, |i16|, |hex|, |i32|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Converter Data Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./epsm/converter-data.csv

.. _tlm-epsm-sai3:

SAI3 Converter Data
==============================

The current voltage, voltage set point, current, current limit and status of SAI3.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 2, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**13** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|, |i16|, |i16|, |hex|, |i32|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Converter Data Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./epsm/converter-data.csv

.. _tlm-epsm-sai4:

SAI4 Converter Data
==============================

The current voltage, voltage set point, current, current limit and status of SAI4.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 3, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**13** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|, |i16|, |i16|, |hex|, |i32|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Converter Data Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./epsm/converter-data.csv

.. _tlm-epsm-sai5:

SAI5 Converter Data
==============================

The current voltage, voltage set point, current, current limit and status of SAI5.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 4, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**13** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|, |i16|, |i16|, |hex|, |i32|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Converter Data Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./epsm/converter-data.csv

.. _tlm-epsm-sai6:

SAI6 Converter Data
==============================

The current voltage, voltage set point, current, current limit and status of SAI6.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 5, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**13** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|, |i16|, |i16|, |hex|, |i32|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Converter Data Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./epsm/converter-data.csv

.. _tlm-epsm-bat1:

BAT1 Converter Data
==============================

The current voltage, voltage set point, current, current limit and status for BAT1 converter.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 6, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**13** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|, |i16|, |i16|, |hex|, |i32|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Converter Data Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./epsm/converter-data.csv

.. _tlm-epsm-bat2:

BAT2 Converter Data
==============================

The current voltage, voltage set point, current, current limit and status for BAT1 converter.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 7, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**13** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|, |i16|, |i16|, |hex|, |i32|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Converter Data Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./epsm/converter-data.csv

.. _tlm-epsm-3v3:

3.3V Converter Data
==============================

The current voltage, voltage set point, current, current limit and status for 3.3V converter.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 8, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**13** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|, |i16|, |i16|, |hex|, |i32|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Converter Data Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./epsm/converter-data.csv

.. _tlm-epsm-5v:

5V Converter Data
==============================

The current voltage, voltage set point, current, current limit and status for 5V converter.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 9, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**13** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|, |i16|, |i16|, |hex|, |i32|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Converter Data Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./epsm/converter-data.csv

.. _tlm-epsm-12v:

12V Converter Data
==============================

The current voltage, voltage set point, current, current limit and status for 12V converter.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 10, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**13** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|, |i16|, |i16|, |hex|, |i32|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Converter Data Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./epsm/converter-data.csv

.. _tlm-epsm-aux:

AUX Converter Data
==================================

The current voltage, voltage set point, current, current limit and status for AUX (*VBATT on DCPS*) converter.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 11, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**13** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|, |i16|, |i16|, |hex|, |i32|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Converter Data Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./epsm/converter-data.csv

.. _tlm-epsm-vref1:

VREF1 Data
==============================

The breakdown of VREF1 voltage reference read from EPSM FPGA.

.. note:: These are used as a reference voltage for the other voltage/current sense readings on the FPGA

    These are broken out for debugging purposes if current/voltage readings don't match expected output

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 12, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**4** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Reference Values Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./epsm/reference-data.csv

.. _tlm-epsm-vref2:

VREF2 Data
==============================

The breakdown of VREF2 voltage reference read from EPSM FPGA.

.. note:: These are used as a reference voltage for the other voltage/current sense readings on the FPGA

    These are broken out for debugging purposes if current/voltage readings don't match expected output

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 13, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**4** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Reference Values Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./epsm/reference-data.csv

.. _tlm-epsm-vref3:

VREF3 Data
==============================

The breakdown of VREF3 voltage reference read from EPSM FPGA.

.. note:: These are used as a reference voltage for the other voltage/current sense readings on the FPGA

    These are broken out for debugging purposes if current/voltage readings don't match expected output

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 14, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**4** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Reference Values Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./epsm/reference-data.csv

.. _tlm-epsm-vref4:

VREF4 Data
==============================

The breakdown of VREF4 voltage reference read from EPSM FPGA.

.. note:: These are used as a reference voltage for the other voltage/current sense readings on the FPGA

    These are broken out for debugging purposes if current/voltage readings don't match expected output

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 15, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**4** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Reference Values Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./epsm/reference-data.csv

.. _tlm-epsm-vref5:

VREF5 Data
==============================

The breakdown of VREF5 voltage reference read from EPSM FPGA.

.. note:: These are used as a reference voltage for the other voltage/current sense readings on the FPGA

    These are broken out for debugging purposes if current/voltage readings don't match expected output

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 16, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**4** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Reference Values Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./epsm/reference-data.csv

.. _tlm-epsm-hsk--5v-ref:

HSK -5V REF Data
==============================

The breakdown of HSK -5V REF voltage reference read from EPSM FPGA.

.. note:: These are used as a reference voltage for the other voltage/current sense readings on the FPGA

    These are broken out for debugging purposes if current/voltage readings don't match expected output

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 17, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**4** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Reference Values Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./epsm/reference-data.csv

.. _tlm-epsm-core-1v5-ref:

Core 1.5V REF Data
==============================

The breakdown of Core 1.5V REF voltage reference read from EPSM FPGA.

.. note:: These are used as a reference voltage for the other voltage/current sense readings on the FPGA

    These are broken out for debugging purposes if current/voltage readings don't match expected output

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 18, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**4** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Reference Values Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./epsm/reference-data.csv

.. _tlm-epsm-hsk-3v3-ref:

HSK 3.3V REF Data
==============================

The breakdown of HSK 3.3V REF voltage reference read from EPSM FPGA.

.. note:: These are used as a reference voltage for the other voltage/current sense readings on the FPGA

    These are broken out for debugging purposes if current/voltage readings don't match expected output

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 19, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**4** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Reference Values Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./epsm/reference-data.csv

.. _tlm-epsm-hsk-5v-ref:

HSK 5V REF Data
==============================

The breakdown of HSK 5V REF voltage reference read from EPSM FPGA.

.. note:: These are used as a reference voltage for the other voltage/current sense readings on the FPGA

    These are broken out for debugging purposes if current/voltage readings don't match expected output

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 20, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**4** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Reference Values Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./epsm/reference-data.csv

.. _tlm-epsm-10v-ref:

HSK 10V REF Data
==============================

The breakdown of HSK 10V REF voltage reference read from EPSM FPGA.

.. note:: These are used as a reference voltage for the other voltage/current sense readings on the FPGA

    These are broken out for debugging purposes if current/voltage readings don't match expected output

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 21, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**4** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Reference Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./epsm/reference-data.csv

.. _tlm-epsm-ring1-ref:

RING1 REF Data
==============================

The breakdown of RING1 voltage reference read from EPSM FPGA.

.. attention:: Excessive RING-bus voltages ``>30V`` with no/little system load will cause thermal stress on Overvoltage (OV) Shunt resistor.

    Monitor RING-bus voltages and FPGA/System temperatures for unsafe conditions.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 22, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**4** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Reference Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./epsm/reference-data.csv

.. _tlm-epsm-ring2-ref:

RING2 REF Data
==============================

The breakdown of RING2 voltage reference read from EPSM FPGA.

.. attention:: Excessive RING-bus voltages ``>30V`` with no/little system load will cause thermal stress on Overvoltage (OV) Shunt resistor.

    Monitor RING-bus voltages and FPGA/System temperatures for unsafe conditions.


SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 23, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**4** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Reference Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./epsm/reference-data.csv

.. _tlm-epsm-ring3-ref:

RING3 REF Data
==============================

The breakdown of RING3 voltage reference read from EPSM FPGA.

.. attention:: Excessive RING-bus voltages ``>30V`` with no/little system load will cause thermal stress on Overvoltage (OV) Shunt resistor.

    Monitor RING-bus voltages and FPGA/System temperatures for unsafe conditions.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 24, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**4** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Reference Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./epsm/reference-data.csv

.. _tlm-epsm-sai1-temperature:

SAI1 Temperature Data
==============================

The breakdown of temperature sensor readout on SAI1 port.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 25, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |i16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Temperature Data Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./epsm/temperature-data.csv

.. _tlm-epsm-sai2-temperature:

SAI2 Temperature Data
==============================

The breakdown of temperature sensor readout on SAI2 port.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 26, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |i16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Temperature Data Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./epsm/temperature-data.csv

.. _tlm-epsm-sai3-temperature:

SAI3 Temperature Data
==============================

The breakdown of temperature sensor readout on SAI3 port.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 27, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |i16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Temperature Data Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./epsm/temperature-data.csv

.. _tlm-epsm-sai4-temperature:

SAI4 Temperature Data
==============================

The breakdown of temperature sensor readout on SAI4 port.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 28, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |i16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Temperature Data Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./epsm/temperature-data.csv

.. _tlm-epsm-sai5a-temperature:

SAI5A Temperature Data
==============================

The breakdown of temperature sensor readout on SAI5A port.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 29, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |i16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Temperature Data Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./epsm/temperature-data.csv

.. _tlm-epsm-sai5b-temperature:

SAI5B Temperature Data
==============================

The breakdown of temperature sensor readout on SAI5B port.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 30, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |i16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Temperature Data Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./epsm/temperature-data.csv

.. _tlm-epsm-sai6a-temperature:

SAI6A Temperature Data
==============================

The breakdown of temperature sensor readout on SAI6A port.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 31, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |i16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Temperature Data Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./epsm/temperature-data.csv

.. _tlm-epsm-sai6b-temperature:

SAI6B Temperature Data
==============================

The breakdown of temperature sensor readout on SAI6B port.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 32, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |i16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Temperature Data Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./epsm/temperature-data.csv

.. _tlm-epsm-sns-vusb-ref:

SNS VUSB REF Data
==============================

The voltage present on SNS VUSB REF voltage reference read from EPSM FPGA.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 33, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u16|

.. _tlm-epsm-sns--reset-data:

SNS -RESET Data
==============================

The voltage present on SNS -RESET read from EPSM FPGA.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 34, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u16|

.. _tlm-epsm-sns-4096mv-ref:

SNS 4096mV REF Data
==============================

The voltage present on SNS 4096mV REF voltage reference read from EPSM FPGA.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 35, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u16|

.. _tlm-epsm-fpga-version:

FPGA Version
==============================

The current version of the FPGA bitstream installed.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 37, [param] 

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A single bit-field value with the data format:  |hex|, |hex|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: FPGA Version Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./epsm/fpga-version.csv

.. _tlm-epsm-last-register-read:

Register Read Address and Value
================================

The value of the last register read address and value from the result of :ref:`EPSM:FPGA READ <epsm-fpga>` command.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 40, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

Multiple values formatted as:

.. csv-table:: Temperature Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50

    "0-1", "Address of last EPSM:FPGA READ command |u16|"
    "1-2", "Value read from EPSM on last EPSM:FPGA READ command |u16|"

.. _tlm-epsm-dosimeter:

Dosimeter [mGy]
==============================

The total dose reading of the rad meter on the EPSM, in mGy.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 41, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u16|

WDT Seconds Left
================

The number of seconds left before the WDT on the EPSM restarts the system.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 42, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u16|

FPGA Reset Count
==============================

The amount of FPGA resets detected from the startup of the SupMCU. This is reset upon restart of the device/power-off
of whole system.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 43, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**2** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

A single value with the data format:  |u16|

Board and FPGA Temperature
==============================

The system board and FPGA temperature on the EPSM.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    EPSM:TEL? 44, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^

**4** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^

Multiple values formatted as:

.. csv-table:: Temperature Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50

    "0-1", "System board temperature in 0.1K |u16|"
    "1-2", "FPGA Temperature in 0.1K |u16|"

.. highlight:: python
