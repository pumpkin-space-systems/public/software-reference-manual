*************************
Telemetry
*************************

.. highlight:: none

PIM telemetry set contains:

* Power port amperage limits, shunt resistor values and current value
* Status of power ports
* Status of Ethernet switch

.. _tlm-pim-channel-currents:

Channel Currents (mA)
=========================================

Current readings from PIM Power ports #1-4 in miliamps (mA).
If current readings are beyond ±5% error, make sure SNS_CH#_I calibration is set correctly for PIM module (controlled by :ref:`PIM:CAL <pim-calibration>` commands).

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    PIM:TEL? 0, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**8** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|, |u16|, |u16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Channel Currents Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./pim/channel-currents.csv

.. _tlm-pim-channel-shunt-resistor-values:

Channel Shunt Resistor Values (µΩ)
=========================================

Current shunt resistor values for PIM power ports in micro-ohms ( µΩ).
Controlled by :ref:`PIM:NVM SHUNT <pim-nvm>`.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    PIM:TEL? 1, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**8** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|, |u16|, |u16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Channel Shunt Resistor Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./pim/channel-shunt.csv

.. _tlm-pim-channel-current-limits:

Channel Current Limits (mA)
=========================================

Current limits for PIM power ports in mA.
Controlled by :ref:`PIM:NVM I_LIMIT <pim-nvm>` command.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    PIM:TEL? 2, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**8** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|, |u16|, |u16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Channel Current Limits Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./pim/channel-current-limits.csv

.. _tlm-pim-status-register:

PIM Status Register
=========================================

State of power to Ports #1-4, if Ethernet is ON, if PIM button is pressed, and Revision of PIM.
Use :ref:`PIM:PORT:POW <pim-port-power>` to control port power, and :ref:`PIM:ETH <pim-ethernet>` to control Ethernet power.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    PIM:TEL? 3, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**1** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A single bit-field value with the data format:  |hex|

Bit-field
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: PIM Status Register Bit-field
    :header: "Bits", "Values"
    :widths: 10, 50
    :file: ./pim/status-register.csv

.. _tlm-pim-overcurrent-event-log:

PIM Overcurrent event log (mA)
=========================================

Log of last overcurrent event in PIM.
An over-current is generated when a device draws more current from a PIM port than the current limit.
If no over-current event has occurred, then the value is 0.
Restart the PIM device to reset the overcurrent log.
SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    PIM:TEL? 4, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**8** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|, |u16|, |u16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Overcurrent Event Log Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./pim/overcurrent-event-log.csv

.. _tlm-pim-channel-voltages:

Channel Voltages (mV)
=========================================

Channel voltages for each PIM Power port.
These are determined by the ASSY REV for the PIM module.
See PIM Data sheet for more details.

SCPI Command
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    PIM:TEL? 5, [param]

Length
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**8** bytes

Data Format
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A multi-value field with the data format and order:  |u16|, |u16|, |u16|, |u16|

Values
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. csv-table:: Channel Voltages Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: ./pim/channel-voltages.csv

.. highlight:: python