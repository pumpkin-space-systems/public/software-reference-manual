#########################################
Deployable Articulated Solar Array (DASA)
#########################################

The Deployable Articulated Solar Array (DASA) provides a platform to articulate up to six independent solar cell strings with 400 degrees of rotation per side.
The DASA is equipped a TiNi pin puller to deploy the solar arrays, with a dedicated command set to enable, arm and fire (energize) the pin puller.

See DASA data sheet for more information.

.. include:: ./scpi/dasa-cmd.rst

.. include:: ./tlm/dasa-tlm.rst

.. include:: ./cal/dasa-cal.rst

.. include:: ./substitutions.rst
