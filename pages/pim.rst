##############################
Payload Interface Module (PIM)
##############################

The Payload Interface Module (PIM) supports one method of power switching and Ethernet for payloads.
It is equipped with four voltage configurable power ports (determined at manufacture time), one 8-pin Ethernet port and three 4-pin Ethernet ports.
A magnetometer is fitted for a MAI-400 ADCS if present on the bus.

See the PIM data sheet for more information.

.. include:: ./scpi/pim-cmd.rst

.. include:: ./tlm/pim-tlm.rst

.. include:: ./cal/pim-cal.rst

.. include:: ./substitutions.rst