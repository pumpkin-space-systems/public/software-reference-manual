############################################
INDI Lamp, Motor & Heater Controller (INDI)
############################################

The INDI Lamp, Motor & Heater Controller provides communication and power interfaces to the lamp, camera shutter motor and heater assembly for the INDI project.

.. include:: ./scpi/indi-cmd.rst

.. include:: ./tlm/indi-tlm.rst

.. include:: ./cal/indi-cal.rst

.. include:: ./substitutions.rst