###########################
GPS Receiver Module (GPSRM)
###########################

The GPSRM provides communications interface to the GPS Novatel OEM719 receiver module.
It controls its power supply and enables communication with the module by sending commands in predetermined formats specified in the Novatel OEM719 Reference Manual.

The GPSRM currently supports commands that enable data logging.
GPSRM1 also runs Vinti7 and SGP4 Orbit propagators that allow an estimate of satellite orbit over a given time delta, position, velocity (for Vinti7) or given two line element (TLE) for SGP4.

See GPSRM data sheet for more information.

.. include:: ./scpi/gpsrm-cmd.rst

.. include:: ./tlm/gpsrm-tlm.rst

.. include:: ./cal/gpsrm-cal.rst

.. include:: ./substitutions.rst