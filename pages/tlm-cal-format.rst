#################################################
Structure and Format of Telemetry and Calibration
#################################################

.. highlight:: none

There are four different types of telemetry requests that can be made for a SupMCU Module:

* ``DATA``
* ``ASCII``
* ``LENGTH``
* ``NAME``

Each type returns a response with a five-byte header followed by a variable length payload, in little endian byte order (LSB).
The header contains a 1-byte unsigned integer data ready flag, proceeded by a 4-byte unsigned integer timestamp.
After the header is the payload, whose size in bytes and format depends on the field/query type.

Here is the layout of the telemetry and calibration in binary format:

+-----------------+-----------------------------------+--------+--------+-------+------+------------------------+
| Byte Position   | 0                                 | 1      | 2      | 3     | 4    | 5...N                  |
+=================+===================================+========+========+=======+======+========================+
| Field           | Data Ready |u8|                   | Timestamp |u32|                | Data                   |
+-----------------+-----------------------------------+--------------------------------+------------------------+
| Purpose         | * 0 = Data invalid.               | OS Ticks since the boot of the | Variable size          |
|                 | * 1 = Telemetry data valid.       | |SupMCU| Module.               | determined by          |
|                 | * 3 = Calibration data valid.     |                                | field/query type       |
+-----------------+-----------------------------------+--------------------------------+------------------------+

.. note:: If the Data Ready flag is 0, then all response data should be discarded.
    Data not being ready is a symptom of querying a SupMCU Module too fast.
    Allow **~75-100 ms between I2C write and I2C read**, as the SupMCU module needs time to gather and format the data.

    If more I2C data is read from the SupMCU module than the length of the telemetry/calibration response, each extra byte read is **0x00**.

When querying telemetry/calibration data from the serial debug port on the |SupMCU| module, the output is in ASCII plain-text and is formatted as ::

    [<Ready Flag Value>:<Timestamp>] <ASCII Formatted Data>

.. attention:: A ``DATA`` type telemetry/calibration is not supported via serial debug port, and will return the string: `CLI does not support TEL?/CAL? n,data commands!`.

********************
Writing Data Queries
********************

Querying telemetry or calibration data is a two step process requiring two independent I2C transactions (with their own start and stop conditions, repeated start conditions will not be accepted).
The first step/transaction is to write your telemetry or calibration query to the slave module and the second step/transaction is to read an appropriate number of bytes from the slave module.
This process is as follows:

#. Send SCPI query command encoded as an ASCII string.
#. Postfix the SCPI command with the termination character ``\n`` (hex: ``0x0A``). ``\r\n`` will work as well.

.. tip:: When commanding via the serial debug port make sure the serial console being used appends the \\n or \\r or \\r\\n as some consoles don’t append it automatically.

An example of sending a telemetry request for BIM's UART status (``BIM:TEL? 1,DATA``) is:

+-------------------+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+-----+
| Byte Position     | 0  | 1  | 2  | 3  | 4  | 5  | 6  | 7  | 8  | 9  | 10 | 11 | 12 | 13 | 14 | 15  |
+===================+====+====+====+====+====+====+====+====+====+====+====+====+====+====+====+=====+
| ASCII Character   | B  | I  | M  | :  | T  | E  | L  | ?  |    | 1  | ,  | D  | A  | T  | A  | \\n |
+-------------------+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+-----+
| Byte Value        | 42 | 49 | 4d | 3a | 54 | 45 | 4c | 3f | 20 | 31 | 2c | 44 | 41 | 54 | 41 | 0a  |
+-------------------+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+-----+

A typical response to a ``BIM:TEL? 1, DATA`` over I2C would be:

+-------------------+---------------+----+----+----+----+----+----+----+----+----+---------+
| Byte Position     | 0             | 1  | 2  | 3  | 4  | 5  | 6  | 7  | 8  | 9  | 10      |
+===================+===============+====+====+====+====+====+====+====+====+====+=========+
| Field             | Data Ready    |  Timestamp |u8|   | Telemetry Data (BIM UART Status) |
+-------------------+---------------+----+----+----+----+----+----+----+----+----+---------+
| Byte Value        | 01            | 00 | 00 | 20 | 00 | 00 | 01 | 00 | 01 | 00 | 01      |
+-------------------+---------------+----+----+----+----+----+----+----+----+----+---------+

The equivalent ASCII text response on the serial debug port is: ``[1:8192] 1,1,1``, asserting all three BIM UART ports are powered.

**********************************************************
Telemetry LENGTH and Telemetry/Calibration NAME responses.
**********************************************************

A Telemetry/calibration request for the byte length of a telemetry item is denoted as ::

    <MOD>:TEL? <Index>,LENGTH

Where ``<MOD>`` is the name of the module (e.g. ``SUP``, ``BIM``, ``PIM``, ``GPS``, ``AIM``, …) and ``<Index>`` is the index of the telemetry item (e.g. ``SupMCU Uptime index`` is ``5``).
The format of the ``TEL? LENGTH`` I2C response is:

+-------------------+---------------+----+----+----+----+-------------+-------------------+
| Byte Position     | 0             | 1  | 2  | 3  | 4  | 5           | 6                 |
+===================+===============+====+====+====+====+=============+===================+
| Field             | Data Ready    |  Timestamp |u16|  | Telemetry Length in Bytes |u16| |
+-------------------+---------------+----+----+----+----+-------------+-------------------+

The equivalent output via serial debug port is ::

    [<Data Ready>:<Timestamp>] <Telemetry Length in bytes>

.. note:: The calibration SCPI query commands don't include length subcommands, since it is fixed to 6 bytes anyways (|f32|, |i16| format).

The request for the name of the given telemetry/calibration item is denoted similarly ::

    <MOD>:<TEL? | CAL?> <Index>,NAME

The format of the ``NAME`` response is:

+-------------------+---------------+----+----+----+----+------------------------------------+
| Byte Position     | 0             | 1  | 2  | 3  | 4  | 5..36                              |
+===================+===============+====+====+====+====+====================================+
| Field             | Data Ready    |  Timestamp |u16|  | Telemetry/Calibration name (ASCII) |
+-------------------+---------------+----+----+----+----+------------------------------------+

The equivalent output via serial debug port is ::

    [<Data Ready>:<Timestamp>] <Telemetry name>

*************************
Telemetry ASCII responses
*************************

Telemetry requests in ASCII format (``<MOD>:TEL? <Index>,ASCII``) are formatted using standard C language format strings (e.g. ``sprintf`` format strings).
For a given telemetry format, it is converted to its equivalent format specification given the following table:

+-----------------+--------------------------------------+
| Format          | C Format specifier                   |
+=================+======================================+
| |u8|            | ``%u``                               |
+-----------------+--------------------------------------+
| |u16|           | ``%u``                               |
+-----------------+--------------------------------------+
| |u32|           | ``%lu``                              |
+-----------------+--------------------------------------+
| |u64|           | ``%llu``                             |
+-----------------+--------------------------------------+
| |i8|            | ``%d``                               |
+-----------------+--------------------------------------+
| |i16|           | ``%d``                               |
+-----------------+--------------------------------------+
| |i32|           | ``%ld``                              |
+-----------------+--------------------------------------+
| |i64|           | ``%lld``                             |
+-----------------+--------------------------------------+
| |f32|           | ``%.4f``                             |
+-----------------+--------------------------------------+
| |f64|           | ``%.4lf``                            |
+-----------------+--------------------------------------+
| |hex|           | ``0x%02X``                           |
+-----------------+--------------------------------------+
| |string|        | ``%s``                               |
+-----------------+--------------------------------------+

The ASCII data response is encoded in the following string format ::

    [<Data Ready>:<Timestamp>] <Data>

**Data Ready** and **Timestamp** are same fields used in all other responses.
Telemetry data is formatted after converting the telemetry format specifier to the C format specifier and using ``sprintf`` to output a string representing the data.
This output string is at **max 256 characters**, and can be read over I2C or is output to serial console connected via SupMCU Debug Port.
If a telemetry format specification lists multiple elements (e.g. BSM:TEL? 6 lists |u16|, |u16|, |u16|, |u16|, |u16| ) the individual elements contained in the format specification are separated by comma.
A format specifier with three u16 is translated to: ``%u,%u,%u``.
For example, the ASCII output for BIM UART Status when all three BIM UART ports are on (and OS ticks = 100, Data Ready = 1) is: ``[1:100] 1,1,1``

.. highlight:: python

.. include:: ./substitutions.rst