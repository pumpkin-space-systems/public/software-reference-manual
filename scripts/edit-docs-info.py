# coding: utf-8
# ##############################################################################
#  (C) Copyright 2020 Pumpkin, Inc. All Rights Reserved.                       #
#                                                                              #
#  This file may be distributed under the terms of the License                 #
#  Agreement provided with this software.                                      #
#                                                                              #
#  THIS FILE IS PROVIDED AS IS WITH NO WARRANTY OF ANY KIND,                   #
#  INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND                       #
#  FITNESS FOR A PARTICULAR PURPOSE.                                           #
# ##############################################################################
"""
Contains the app code to start up the GUI to edit the documentation database.
"""
from scripts.doc.ui.main import MainWindow
from scripts.doc.load.parse import load_module_db

from wx import App

from pathlib import Path


if __name__ == '__main__':
    db_path = Path(__file__).parent.parent / 'data' / 'reference-db.json'
    db = load_module_db(db_path)
    app = App()
    window = MainWindow(None,
                        'SFR Documentation Editor',
                        db,
                        Path(__file__).parent.parent / 'gen')
    window.Show(True)
    app.MainLoop()