# coding: utf-8
# ##############################################################################
#  (C) Copyright 2020 Pumpkin, Inc. All Rights Reserved.                       #
#                                                                              #
#  This file may be distributed under the terms of the License                 #
#  Agreement provided with this software.                                      #
#                                                                              #
#  THIS FILE IS PROVIDED AS IS WITH NO WARRANTY OF ANY KIND,                   #
#  INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND                       #
#  FITNESS FOR A PARTICULAR PURPOSE.                                           #
# ##############################################################################
"""
Makes the rST markup for a telemetry item in the SFR manual.
"""
import csv
from io import StringIO
from string import Template
from enum import Enum
from typing import List, NamedTuple


class TelemType(Enum):
    Single = 0
    Multi = 1
    Bitfield = 2


class TelemItem(NamedTuple):
    name: str
    cmd: str
    format: List[str]
    length: int
    tlm_type: TelemType


multi_desc = "A multi-value field with the data format and order: "
single_desc = "A single value with the data format: "
bit_desc = "A single bit-field value with the data format: "
TYPE_TO_DESC = {
    TelemType.Single: single_desc,
    TelemType.Multi: multi_desc,
    TelemType.Bitfield: bit_desc
}
TEMPLATE_STR = """
$name
==================================================

<Description Here>

SCPI Command
^^^^^^^^^^^^^

.. code-block::

    $cmd

Length
^^^^^^^

**$length** bytes

Data Format
^^^^^^^^^^^

$description $format
"""
TEMPLATE = Template(TEMPLATE_STR)

MULTI_VALUE_TEMPLATE_STR = """
Values
^^^^^^

.. csv-table:: $name Values
    :header: "Bytes", "Purpose"
    :widths: 10, 50
    :file: $file_name
    
-- $file_name contents --

$csv_str
"""
BITFIELD_VALUE_TEMPLATE_STR = """
Bit-field
^^^^^^^^^^

.. csv-table:: $name Bit-field
    :header: "Bits", "Values"
    :widths: 10, 50
    :file: $file_name
    
-- $file_name contents --

$csv_str
"""
CAL_VALUE_TEMPLATE_STR = """

.. csv-table:: $name Calibration Values
    :header: "Index", "Name", "Note"
    :widths: 10, 50

$csv_str
"""
MULTI_VALUE_TEMPLATE = Template(MULTI_VALUE_TEMPLATE_STR)
BITFIELD_VALUE_TEMPLATE = Template(BITFIELD_VALUE_TEMPLATE_STR)
CAL_VALUE_TEMPLATE = Template(CAL_VALUE_TEMPLATE_STR)


def determine_type(fmt: List[str]) -> TelemType:
    if 'hex' in fmt:
        return TelemType.Bitfield
    if len(fmt) > 1:
        return TelemType.Multi
    return TelemType.Single


def to_csv_rows(rows: str, delim: str = ',') -> List[List[str]]:
    with StringIO(rows) as f:
        csv_rows = []
        reader = csv.reader(f, delimiter=delim, quotechar='"')
        for row in reader:
            csv_rows.append(row)
    return csv_rows


def row_to_item(row: List[str]) -> TelemItem:
    cmd, name, length, format = row
    length = int(length)
    format = format.split(',')
    t = determine_type(format)
    format = ', '.join([f'|{fmt.strip()}|' for fmt in format])
    return TelemItem(name, cmd, format, length, t)


def print_item(item: TelemItem):
    d = item._asdict()
    d['description'] = TYPE_TO_DESC[item.tlm_type]
    t = TEMPLATE.substitute(d)
    print(t)


def print_telemetry_section(s: str, delim='\t'):
    rows = to_csv_rows(s, delim)
    items = [row_to_item(r) for r in rows]
    for item in items:
        print_item(item)


def rows_to_csv_str(rows) -> str:
    with StringIO() as f:
        writer = csv.writer(f)
        for row in rows:
            writer.writerow(row)
        return f.getvalue()


def print_value_table(s: str, name: str, file_name: str, tlm_type: TelemType, delim='\t'):
    rows = to_csv_rows(s, delim)
    rows_str = rows_to_csv_str(rows)

    d = {
        'name': name,
        'file_name': file_name,
        'csv_str': rows_str
    }
    if tlm_type == TelemType.Bitfield:
        s = BITFIELD_VALUE_TEMPLATE.substitute(**d)
    else:
        s = MULTI_VALUE_TEMPLATE.substitute(**d)
    print(s)


def print_cal_table(s: str, name: str, delim='\t'):
    rows = to_csv_rows(s, delim)
    rows_str = rows_to_csv_str(rows)
    rows_str = '    \n'.join(rows_str.splitlines())

    d = {
        'name': name,
        'csv_str': rows_str
    }

    s = CAL_VALUE_TEMPLATE.substitute(**d)
    print(s)
