# coding: utf-8
# ##############################################################################
#  (C) Copyright 2020 Pumpkin, Inc. All Rights Reserved.                       #
#                                                                              #
#  This file may be distributed under the terms of the License                 #
#  Agreement provided with this software.                                      #
#                                                                              #
#  THIS FILE IS PROVIDED AS IS WITH NO WARRANTY OF ANY KIND,                   #
#  INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND                       #
#  FITNESS FOR A PARTICULAR PURPOSE.                                           #
# ##############################################################################
"""
Strings to use when writing out rST formatted documentation for the SFR.

TODO: Separate into file/database for better configurability
"""
from string import Template

COMMAND_HEADING = """============
Commands
============

"""

TELEMETRY_HEADING = """============
Telemetry
============

"""

CALIBRATION_HEADING = """============
Calibration
============

"""

TELEMETRY_DOC = Template("""========================================
$name Telemetry
========================================

$note

""")

NO_TELEMETRY_NOTE = Template("There is no telemetry for $name on version: **$version**")
TELEMETRY_NOTE = Template("Below is a listing of telemetry for $name on version: **$version**")

TELEMETRY_DESCRIPTION = Template("""$name
------------------------------------------------------------

$full_name - $short_desc

**Index: $index **
**Size: $length **
**Simulatable on QSM/STM: $simulatable **

Description
^^^^^^^^^^^

$long_desc

$layout_information

""")

TELEMETRY_MULTI_DESCRIPTION = Template("""Field Layout
^^^^^^^^^^^^^^^^^^^

.. csv-table:: Fields
    :header: "Description","Data Type","Position"
    :widths: 60, 20, 20
    
$layout

""")

TELEMETRY_BITFIELD_DESCRIPTION = Template("""Bit-Field Layout
^^^^^^^^^^^^^^^^^^^^^^^

The following fields are encoded in a **$data_type**:

.. csv-table:: Encoded Fields
    :header: "Description","Position"
    :widths: 80, 20

$layout

""")

COMMAND_DOC = Template("""========================================
$name Command
========================================

$note

""")

NO_COMMAND_NOTE = Template("There is no commands for $name on version: **$version**")
COMMAND_NOTE = Template("Below is a listing of commands for $name on version: **$version**")

COMMAND_DESCRIPTION = Template("""$name
------------------------------------------------------

$listing_name ::
$formatted_syntax

$short_desc

Description
^^^^^^^^^^^

$long_desc

Remarks
^^^^^^^

$remarks

Example
^^^^^^^

$example

""")

CALIBRATION_DOC = Template("""========================================
$name Command
========================================

$note

""")

NO_CALIBRATION_NOTE = Template("There is no calibration fields for $name on version: **$version**")
CALIBRATION_NOTE = Template("Below is a listing of calibration fields for $name on version: **$version**")

CALIBRATION_DESCRIPTION = Template("""$name, $index, $description""")

DOCUMENTATION_DESCRIPTION = Template("""============================================================
$name
============================================================

$full_name - $short_desc

Current version: **$version**

Description
-----------

$long_desc

""")
