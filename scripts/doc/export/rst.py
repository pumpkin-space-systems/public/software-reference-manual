# coding: utf-8
# ##############################################################################
#  (C) Copyright 2020 Pumpkin, Inc. All Rights Reserved.                       #
#                                                                              #
#  This file may be distributed under the terms of the License                 #
#  Agreement provided with this software.                                      #
#                                                                              #
#  THIS FILE IS PROVIDED AS IS WITH NO WARRANTY OF ANY KIND,                   #
#  INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND                       #
#  FITNESS FOR A PARTICULAR PURPOSE.                                           #
# ##############################################################################
"""
Logic for exporting the rST version of the documentation from the TelemetryDatabase.
"""
from typing import AnyStr
from dataclasses import asdict

from scripts.doc.types import DocExporter, CalibrationDescription, CommandDescription, TelemetryDescription, \
    SingleTelemetryItemDescription, MultiTelemetryItemDescription, BitfieldTelemetryDescription, TelemetryType
from scripts.doc.export.rst_strings import *


class RstExporter(DocExporter):
    """
    Exports the rST version of the documentation
    """
    @staticmethod
    def export_single_layout_information(tlm: SingleTelemetryItemDescription):
        """
        Exports a SingleTelemetryItemDescription as a `layout_information` string.

        :param tlm: The SingleItemTelemetryDescription to export
        :return: The string representing the layout information
        """
        return f"Format is **{tlm.item[0].data_type}**"

    @staticmethod
    def export_multi_layout_information(tlm: MultiTelemetryItemDescription):
        """
        Exports a MultiTelemetryItemDescription as a `layout_information` string.

        :param tlm: The MultiTelemetryItemDescription to export
        :return: The string representing the layout information
        """
        rows = []
        for item in tlm.item:
            quote = "\""
            esc_quote = "\\\""
            rows.append(f'"{item.description.replace(quote, esc_quote)}",'
                        f'"{item.data_type}","{item.position[0]}-{item.position[1]}"')
        items = "\n".join("    " + r for r in rows)
        return TELEMETRY_MULTI_DESCRIPTION.substitute(layout=items)

    @staticmethod
    def export_bitfield_layout_information(tlm: BitfieldTelemetryDescription):
        """
        Exports a BitfieldTelemetryDescription as a `layout_information` string.

        :param tlm: The BitfieldTelemetryDescription to export
        :return: The string representing the layout information.
        """
        rows = []
        for item in tlm.item:
            quote = "\""
            esc_quote = "\\\""
            rows.append(f'"{item.description.replace(quote, esc_quote)}","{item.position[0]}-{item.position[1]}"')
        items = "\n".join("    " + r for r in rows)
        return TELEMETRY_BITFIELD_DESCRIPTION.substitute(data_type=tlm.item[0].data_type, layout=items)

    def export_telemetry_description(self, tlm: TelemetryDescription) -> AnyStr:
        """
        Exports the telemetry description in the rST format.

        :param tlm: The telemetry description to export
        :return: The string to write to the rST file
        """
        # determine layout information to use
        if tlm.tlm_type == TelemetryType.Single:
            layout_information = self.export_single_layout_information(tlm.detail)
        elif tlm.tlm_type == TelemetryType.Multiple:
            layout_information = self.export_multi_layout_information(tlm.detail)
        else:
            layout_information = self.export_bitfield_layout_information(tlm.detail)

        # export tlm as dict, and use template string to return rST formatted text
        tlm = asdict(tlm)
        tlm['layout_information'] = layout_information
        return TELEMETRY_DESCRIPTION.substitute(tlm)

    def export_command_description(self, cmd: CommandDescription) -> AnyStr:
        """
        Exports the command description in the rST format.

        :param cmd: The command description to export
        :return: The string to write to the rSt file
        """
        # format the syntax by placing 4 spaces at the beginning of every line.
        formatted_syntax = "\n".join(["    " + line for line in cmd.syntax.splitlines()])

        # create dictionary with new formatted_syntax in, return rST string representing description.
        cmd = asdict(cmd)
        cmd['formatted_syntax'] = formatted_syntax
        return COMMAND_DESCRIPTION.substitute(cmd)

    def export_calibration_description(self, cal: CalibrationDescription) -> AnyStr:
        """
        Exports the calibration description in the rST format. Note this will be a single row
        in the calibration table that is created from the handler function its self.

        :param cal: The calibration description to export
        :return: The string to write to the rST file
        """
        return CALIBRATION_DESCRIPTION.substitute(asdict(cal))

    @property
    def command_heading(self) -> AnyStr:
        """
        Exports the command heading for the section with commands in it.

        :return: The command heading to write out to the rST file.
        """
        return COMMAND_HEADING

    @property
    def telemetry_heading(self) -> AnyStr:
        """
        Exports the telemetry heading for the section with telemetry in it.

        :return: The telemetry heading to write out to the rST file.
        """
        return TELEMETRY_HEADING

    @property
    def calibration_heading(self) -> AnyStr:
        """
        Exports the calibration heading for the section with calibration in it.

        :return: The command heading to write out to the rST file.
        """
        return CALIBRATION_HEADING
