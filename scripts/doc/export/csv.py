# coding: utf-8
# ##############################################################################
#  (C) Copyright 2020 Pumpkin, Inc. All Rights Reserved.                       #
#                                                                              #
#  This file may be distributed under the terms of the License                 #
#  Agreement provided with this software.                                      #
#                                                                              #
#  THIS FILE IS PROVIDED AS IS WITH NO WARRANTY OF ANY KIND,                   #
#  INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND                       #
#  FITNESS FOR A PARTICULAR PURPOSE.                                           #
# ##############################################################################
"""
Describes exporters for the CSV format
"""
import csv
from io import StringIO
from typing import AnyStr

from scripts.doc.types import DocExporter, MultiItemPart, MultiTelemetryItemDescription, TelemetryDescription, \
    CommandDescription, CalibrationDescription, ModuleDescription


class SimpleCsvExporter(DocExporter):
    @property
    def command_heading(self) -> AnyStr:
        """
        Command heading for a command csv dump.

        `Listing Name`, `Syntax`, `Description`
        """
        return '"Listing Name", "Syntax", "Description"'

    @property
    def calibration_heading(self) -> AnyStr:
        """
        Calibration heading for calibration csv dump.

        `Index`, `Name`
        """
        return '"Index","Name"'

    @property
    def telemetry_heading(self) -> AnyStr:
        """
        Telemetry heading for a telemetry csv dump.

        `Index`, `Name`, `Description`, `Number of bytes`, `Data Format`
        """
        return '"Index", "Name", "Description", "Number of bytes", "Data Format"'

    def export_calibration_description(self, cal: CalibrationDescription) -> AnyStr:
        """
        Exports the calibration description as:
            `cal.index, cal.name`

        :param cal: The calibration description to export
        :return: An exportable CSV string
        """
        with StringIO(newline='') as s:
            writer = csv.writer(s)
            writer.writerow([cal.index, cal.name])
            return s.getvalue()

    def export_command_description(self, cmd: CommandDescription) -> AnyStr:
        """
        Exports the command description as:
            `cmd.listing_name, cmd.syntax, cmd.short_desc`

        :param cmd: The command to export the description for
        :return: The exportable CSV string
        """
        with StringIO(newline='') as s:
            writer = csv.writer(s)
            writer.writerow([cmd.listing_name, cmd.syntax, cmd.short_desc])
            return s.getvalue()

    def export_telemetry_description(self, tlm: TelemetryDescription) -> AnyStr:
        """
        Exports the telemetry description as:
        `tlm.index,
        tlm.name,
        tlm.short_desc,
        tlm.length - sizeof(header_footer),
        "','.join(item.data_type for item in tlm.items]"`

        :param tlm: The telemetry to export the description for
        :return: The exportable CSV string
        """
        SIZEOF_HEADER_FOOTER = 13  # Header is 5 bytes, footer is 8
        with StringIO(newline='') as s:
            writer = csv.writer(s)
            writer.writerow([tlm.name, tlm.short_desc, tlm.length - SIZEOF_HEADER_FOOTER,
                             ",".join(i.data_type for i in tlm.detail.item)])
            return s.getvalue()
