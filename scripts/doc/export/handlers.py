# coding: utf-8
# ##############################################################################
#  (C) Copyright 2020 Pumpkin, Inc. All Rights Reserved.                       #
#                                                                              #
#  This file may be distributed under the terms of the License                 #
#  Agreement provided with this software.                                      #
#                                                                              #
#  THIS FILE IS PROVIDED AS IS WITH NO WARRANTY OF ANY KIND,                   #
#  INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND                       #
#  FITNESS FOR A PARTICULAR PURPOSE.                                           #
# ##############################################################################
"""
Describes all of the handlers that take in a module database and path, then export
the module definition as the named format.
"""
from dataclasses import asdict

from scripts.doc.export.csv import SimpleCsvExporter
from scripts.doc.export.rst import RstExporter
from scripts.doc.export.rst_strings import COMMAND_DOC, COMMAND_NOTE, NO_COMMAND_NOTE, TELEMETRY_NOTE, \
    NO_TELEMETRY_NOTE, TELEMETRY_DOC, CALIBRATION_NOTE, NO_CALIBRATION_NOTE, CALIBRATION_DOC, DOCUMENTATION_DESCRIPTION
from scripts.doc.types import ModuleDb
from io import StringIO
from string import Template
from pathlib import Path
from typing import Callable

ExportFunction = Callable[[Path, ModuleDb], None]

# Naming for different CSV files
CSV_CMD_DUMP_NAME = Template('$name-cmd.csv')
CSV_TLM_DUMP_NAME = Template('$name-tlm.csv')
CSV_CAL_DUMP_NAME = Template('$name-cal.csv')
CSV_INFO_DUMP_NAME = Template('$name.csv')

# Naming for different rST files
RST_CMD_DUMP_NAME = Template('$name-cmd.rst')
RST_TLM_DUMP_NAME = Template('$name-tlm.rst')
RST_CAL_DUMP_NAME = Template('$name-cal.rst')
RST_INFO_DUMP_NAME = Template('$name.rst')


def export_csv(path: Path, db: ModuleDb):
    """
    Exports the `db` to the `path` using the `SimpleCsvExporter`.

    :param path: The path to save the exported CSV files to
    :param db: The modules to export as CSV.
    """
    # Make sure the export directory exists
    try:
        path.mkdir(exist_ok=True, parents=True)
    except (OSError, FileNotFoundError) as e:
        raise RuntimeError(f"Unable to create directory {path}")

    print(f'Exporting to {path} as CSV')
    exporter = SimpleCsvExporter()
    for mod in db:
        cmd_path = path / CSV_CMD_DUMP_NAME.substitute(name=mod.name)
        tlm_path = path / CSV_TLM_DUMP_NAME.substitute(name=mod.name)
        cal_path = path / CSV_CAL_DUMP_NAME.substitute(name=mod.name)
        info_path = path / CSV_INFO_DUMP_NAME.substitute(name=mod.name)

        # Write out the command description
        with cmd_path.open('w') as f:
            f.write(f'"Module Name:","{mod.full_name}"\n')
            f.write(exporter.command_heading + "\n")
            for cmd in mod.commands:
                f.write(exporter.export_command_description(cmd).rstrip() + "\n")

        # Write out the telemetry description
        with tlm_path.open('w') as f:
            f.write(f'"Module Name:","{mod.full_name}"\n')
            f.write(exporter.telemetry_heading + "\n")
            for tlm in mod.telemetry:
                f.write(exporter.export_telemetry_description(tlm).rstrip() + "\n")

        # Write out the calibration description
        with cal_path.open('w') as f:
            f.write(f'"Module Name:","{mod.full_name}"\n')
            f.write(exporter.calibration_heading + "\n")
            for cal in mod.calibration:
                f.write(exporter.export_calibration_description(cal).rstrip() + "\n")

        # Write out the info description
        with info_path.open('w') as f:
            f.write(f'"Module Name:","{mod.full_name}"\n')
            f.write(f'"Version:","{mod.version}"\n')
            f.write(f'"Description:","{mod.short_desc}"\n')


def export_rst(path: Path, db: ModuleDb):
    """
    Exports the `db` to the `path` using the `RstExporter`.

    :param path: The path (or object that returns path from __str__ method) to export the module database to
    :param db: The module database to export
    """
    # Make sure the export directory exists
    try:
        path.mkdir(exist_ok=True, parents=True)
    except (OSError, FileNotFoundError) as e:
        raise RuntimeError(f"Unable to create directory {path}")

    print(f'Exporting to {path} as rST')
    exporter = RstExporter()
    for mod in db:
        cmd_path = path / "cmd" / RST_CMD_DUMP_NAME.substitute(name=mod.name)
        tlm_path = path / "tlm" / RST_TLM_DUMP_NAME.substitute(name=mod.name)
        cal_path = path / "cal" / RST_CAL_DUMP_NAME.substitute(name=mod.name)
        info_path = path / RST_INFO_DUMP_NAME.substitute(name=mod.name)

        # Create directories
        cmd_path.parent.mkdir(parents=True, exist_ok=True)
        tlm_path.parent.mkdir(parents=True, exist_ok=True)
        cal_path.parent.mkdir(parents=True, exist_ok=True)
        info_path.parent.mkdir(parents=True, exist_ok=True)

        # Write out the command description
        with cmd_path.open('w') as f:
            if mod.commands:
                note = COMMAND_NOTE.substitute(name=mod.name, version=mod.version)
            else:
                note = NO_COMMAND_NOTE.substitute(name=mod.name, version=mod.version)
            f.write(COMMAND_DOC.substitute(name=mod.name, note=note) + "\n")
            for cmd in mod.commands:
                f.write("\n" + exporter.export_command_description(cmd).rstrip() + "\n")

        # Write out the telemetry description
        with tlm_path.open('w') as f:
            if mod.telemetry:
                note = TELEMETRY_NOTE.substitute(name=mod.name, version=mod.version)
            else:
                note = NO_TELEMETRY_NOTE.substitute(name=mod.name, version=mod.version)
            f.write(TELEMETRY_DOC.substitute(name=mod.name, note=note) + "\n")
            for tlm in mod.telemetry:
                f.write("\n" + exporter.export_telemetry_description(tlm).rstrip() + "\n")

        # Write out the calibration description
        with cal_path.open('w') as f:
            if mod.calibration:
                note = CALIBRATION_NOTE.substitute(name=mod.name, version=mod.version)
            else:
                note = NO_CALIBRATION_NOTE.substitute(name=mod.name, version=mod.version)
            f.write(CALIBRATION_DOC.substitute(name=mod.name, note=note) + "\n")
            for cal in mod.calibration:
                f.write("\n" + exporter.export_calibration_description(cal).rstrip() + "\n")

        # Write out the info description
        with info_path.open('w') as f:
            info_str = DOCUMENTATION_DESCRIPTION.substitute(name=mod.name, full_name=mod.full_name, version=mod.version,
                                                            short_desc=mod.short_desc, long_desc=mod.long_desc)
            f.write(info_str)


EXPORTERS = {
    'CSV': export_csv,
    'rST': export_rst
}
