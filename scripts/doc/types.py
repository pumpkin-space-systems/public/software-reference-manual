# coding: utf-8
# ##############################################################################
#  (C) Copyright 2020 Pumpkin, Inc. All Rights Reserved.                       #
#                                                                              #
#  This file may be distributed under the terms of the License                 #
#  Agreement provided with this software.                                      #
#                                                                              #
#  THIS FILE IS PROVIDED AS IS WITH NO WARRANTY OF ANY KIND,                   #
#  INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND                       #
#  FITNESS FOR A PARTICULAR PURPOSE.                                           #
# ##############################################################################
"""
Types needed for the documentation editing library.
"""
from dataclasses import dataclass
from typing import List, Tuple, Union, Protocol, AnyStr
from enum import IntEnum


class TelemetryType(IntEnum):
    """What type of telemetry field this is."""
    Single = 0
    Multiple = 1
    Bitfield = 2


Position = Tuple[int, int]


@dataclass
class SingleTelemetryItemDescription:
    """
    Describes a single telemetry item type of field
    """
    data_type: str


@dataclass
class MultiItemPart:
    """
    Describes a single item in the MultiTelemetryItemDescription
    """
    description: str
    data_type: str
    position: Position  # bytes [start, end]


@dataclass
class MultiTelemetryItemDescription:
    """
    Describes a set of multiple items contained within a single telemetry index
    """
    item: List[MultiItemPart]


@dataclass
class BitfieldItemPart:
    """
    Describes a single value encoded into the bitfield
    """
    description: str
    position: Position  # bytes [start, end]


@dataclass
class BitfieldTelemetryDescription:
    """
    Describes a bitfield-encoded telemetry item.
    """
    data_type: str
    fields: List[BitfieldItemPart]


TelemetryDetail = MultiTelemetryItemDescription


@dataclass
class TelemetryDescription:
    """
    Describes a single telemetry index with a short/long description of
    the telemetry item as well as if it is a singular telemetry item, bit-field or contains
    multiple items
    """
    index: int
    name: str
    full_name: str
    short_desc: str
    long_desc: str
    simulatable: bool
    length: int
    tlm_type: TelemetryType
    detail: TelemetryDetail


# ---- Command Description Section ----


@dataclass
class CommandDescription:
    """
    Describes a single command with a short/long description, syntax, name and example usage.
    """
    name: str
    listing_name: str  # As shows up in debug console in SCPI when `ls` is issued
    syntax: str  # Syntax string for the command
    short_desc: str
    long_desc: str
    remarks: str
    example: str


# ---- Calibration Description Section ----


@dataclass
class CalibrationDescription:
    """
    Describes a single calibration factor with name, index and description
    """
    name: str
    index: int
    description: str


@dataclass
class ModuleDescription:
    """
    Describes a modules name, description, version, telemetry, commands and calibration.
    """
    name: str
    full_name: str
    short_desc: str
    long_desc: str
    version: str
    telemetry: List[TelemetryDescription]
    commands: List[CommandDescription]
    calibration: List[CalibrationDescription]


ModuleDb = List[ModuleDescription]


class DocExporter(Protocol):
    """
    Describes how to export writable string output for a documentation format (e.g. csv/rST/md...)
    """
    def export_telemetry_description(self, tlm: TelemetryDescription) -> AnyStr:
        """
        Exports the `tlm` to a string format for output.

        :param tlm: The telemetry description
        :return: The string for output to exporter format
        """

    def export_command_description(self, cmd: CommandDescription) -> AnyStr:
        """
        Exports the `cmd` to a string format for output.

        :param cmd: The command description
        :return: The string for output to exporter format
        """

    def export_calibration_description(self, cal: CalibrationDescription) -> AnyStr:
        """
        Exports the `cal` to a string format for output.

        :param cal: The calibration description
        :return: The string for output to exporter format
        """
