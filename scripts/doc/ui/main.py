# coding: utf-8
# ##############################################################################
#  (C) Copyright 2020 Pumpkin, Inc. All Rights Reserved.                       #
#                                                                              #
#  This file may be distributed under the terms of the License                 #
#  Agreement provided with this software.                                      #
#                                                                              #
#  THIS FILE IS PROVIDED AS IS WITH NO WARRANTY OF ANY KIND,                   #
#  INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND                       #
#  FITNESS FOR A PARTICULAR PURPOSE.                                           #
# ##############################################################################
"""
Main UI panel that contains the main documentation window to select
commands/telemetry/calibration values for each of the modules loaded into the
documentation.db file.
"""
from dataclasses import dataclass

import wx

from pathlib import Path
from typing import Any, Callable, Union, Optional, List, Dict, Tuple

from wx import NOT_FOUND

from scripts.doc.export.handlers import EXPORTERS
from scripts.doc.types import ModuleDb, ModuleDescription, TelemetryDescription, CalibrationDescription, \
    CommandDescription

LAYOUT_REMOVE = 'remove'
LAYOUT_ADD = 'add'
LAYOUT_EDIT = 'edit'
LAYOUT_LIST = 'list'
LAYOUT_LABEL = 'label'
LAYOUT_SIZER = 'sizer'

DEFAULT_DB_LOCATION = Path('./data/reference-db.json')
DEFAULT_OUT_LOCATION = Path('./gen/')

# UI constants
MIN_WINDOW_WIDTH = 800
MIN_WINDOW_HEIGHT = 400
BG_COLOR = "#EFEFEF"

# The margin around the controls
DEFAULT_UI_MARGIN = 5

# This tells the UI_Element to expand to take up as much space as it can
FILL_UI_PRIORITY = 1

# This tells the UI Element to take only to its minimum space required to
# display properly
NO_FILL_UI_PRIORITY = 0

TLM_LAYOUT = "Telemetry"
CMD_LAYOUT = "Command"
CAL_LAYOUT = "Calibration"
MOD_LAYOUT = "Module"

DescriptionItem = Union[ModuleDescription, TelemetryDescription, CommandDescription, CalibrationDescription]


@dataclass
class SectionInfo:
    current_selection: Optional[str]
    current_selection_item: Optional[DescriptionItem]
    selections: List[str]


class MainWindow(wx.Frame):
    """
    The main window of the documentation editor program.
    """

    def __init__(self, parent: Any, title: str, module_db: ModuleDb, output_path: Path, *args, **kwargs):
        """
        Constructs GUI for MainWindow. Use `.show()` to make UI visible.\

        :param parent: Parent window (or app)
        :param title: The title of the window
        :param module_db: The module database list
        :param output_path: The path to output the generated documentation to
        :param args: Additional position arguments to pass to super constructor for wx
        :param kwargs: Additional kw args to pass to super constructor for wx
        """
        wx.Frame.__init__(self, parent, title=title, *args, **kwargs)

        self._output_location = output_path
        self._db = module_db
        self._current_mod = None

        # Variables to hold state information for the UI
        self._sections = {
            TLM_LAYOUT: SectionInfo(None, None, []),
            CMD_LAYOUT: SectionInfo(None, None, []),
            MOD_LAYOUT: SectionInfo(None, None, []),
            CAL_LAYOUT: SectionInfo(None, None, []),
        }

        # Build UI Layout, bind
        self.layout()
        self.bind()

        # Populate UI
        self.build()

        self.Show()

    @property
    def output_location(self) -> Path:
        """Where to output the generated documentation"""
        return self._output_location

    @output_location.setter
    def output_location(self, value: Union[Path, str]):
        """Sets the output location to the new `value`"""
        self._output_location = Path(str(value))

    @property
    def db(self) -> ModuleDb:
        """The underlying module database"""
        return self._db

    @db.setter
    def db(self, value: ModuleDb):
        """Set the module database `db` to `value`. Will build UI to reflect changes afterwards"""
        self._db = value
        self.build()

    @property
    def current_mod(self) -> ModuleDescription:
        """Returns the current module selected, or `None` if none is selected."""
        return self._current_mod

    @current_mod.setter
    def current_mod(self, value: ModuleDescription):
        """Sets the current module selected, this will also update the current telemetry/command/calibration lists."""
        self._current_mod = value
        if value:
            self._build_module_list(value)
        else:
            self._clear_module_lists()

    def layout(self):
        """Instantiates the UI layout and elements"""
        main_layout = wx.BoxSizer(wx.HORIZONTAL)
        self.SetBackgroundColour(BG_COLOR)
        sections = [MOD_LAYOUT, TLM_LAYOUT, CMD_LAYOUT, CAL_LAYOUT]
        self.layouts = {}

        for section in sections:
            # Create selection layout and elements
            layout = {LAYOUT_SIZER: wx.BoxSizer(wx.VERTICAL),
                      LAYOUT_LABEL: wx.StaticText(self, label=section, style=wx.ALIGN_CENTER_HORIZONTAL),
                      LAYOUT_LIST: wx.ListBox(self, style=wx.LB_SINGLE | wx.LB_SORT),
                      LAYOUT_EDIT: wx.Button(self, label=f"&Edit {section}..."),
                      LAYOUT_ADD: wx.Button(self, label=f"&Add {section}..."),
                      LAYOUT_REMOVE: wx.Button(self, label=f"&Remove {section}")
                      }

            # Add elements to layout, add layout main_layout
            layout[LAYOUT_SIZER].Add(layout[LAYOUT_LABEL],
                                     NO_FILL_UI_PRIORITY, wx.ALIGN_CENTER | wx.TOP | wx.BOTTOM,
                                     DEFAULT_UI_MARGIN)
            layout[LAYOUT_SIZER].Add(layout[LAYOUT_LIST], FILL_UI_PRIORITY, wx.EXPAND | wx.BOTTOM, DEFAULT_UI_MARGIN)
            layout[LAYOUT_SIZER].Add(layout[LAYOUT_EDIT], NO_FILL_UI_PRIORITY, wx.EXPAND | wx.BOTTOM, DEFAULT_UI_MARGIN)
            layout[LAYOUT_SIZER].Add(layout[LAYOUT_ADD], NO_FILL_UI_PRIORITY, wx.EXPAND | wx.BOTTOM, DEFAULT_UI_MARGIN)
            layout[LAYOUT_SIZER].Add(layout[LAYOUT_REMOVE], NO_FILL_UI_PRIORITY, wx.EXPAND | wx.BOTTOM, DEFAULT_UI_MARGIN)
            main_layout.Add(layout[LAYOUT_SIZER], FILL_UI_PRIORITY, wx.EXPAND | wx.ALL, DEFAULT_UI_MARGIN)

            self.layouts[section] = layout

        # setup control section of main window
        self.output_location_ctrl_box = wx.TextCtrl(self, value=str(self.output_location))
        self.output_location_ctrl_box.Disable()  # Not user editable
        self.output_location_ctrl = wx.DirPickerCtrl(self, path=str(self.output_location), style=wx.DIRP_DIR_MUST_EXIST)
        self.exporter_btns = {}

        # Setup export layout on the right
        export_layout = wx.BoxSizer(wx.VERTICAL)
        export_layout.Add(wx.StaticText(self, label="Output Location:"))
        export_layout.Add(self.output_location_ctrl_box, NO_FILL_UI_PRIORITY, wx.EXPAND | wx.ALL, DEFAULT_UI_MARGIN)
        export_layout.Add(self.output_location_ctrl, NO_FILL_UI_PRIORITY, wx.EXPAND | wx.ALL, DEFAULT_UI_MARGIN)
        export_layout.Add(wx.Window(self), FILL_UI_PRIORITY, wx.EXPAND | wx.ALL, DEFAULT_UI_MARGIN)

        # Setup an export button for each Exporter
        for exporter in EXPORTERS.keys():
            self.exporter_btns[exporter] = wx.Button(self, label=f'Export {exporter}')
            export_layout.Add(self.exporter_btns[exporter], NO_FILL_UI_PRIORITY, wx.EXPAND | wx.BOTTOM,
                              DEFAULT_UI_MARGIN)

        main_layout.Add(export_layout, FILL_UI_PRIORITY + 1, wx.EXPAND | wx.ALL, DEFAULT_UI_MARGIN)

        # Now add in the main layout
        self.SetSizeHints(MIN_WINDOW_WIDTH, MIN_WINDOW_HEIGHT)
        self.SetSizer(main_layout)

    def _get_selections(self, section_name: str) -> Dict[str, DescriptionItem]:
        """
        Gets the selection dictionary for the particular section_name

        :param section_name: The name to get the selection dictionary from
        :return: The selection dictionary
        """
        if section_name == MOD_LAYOUT:
            return dict((item.name, item) for item in self._db)
        else:
            # Assume current mod is not None in this case
            mod = self.current_mod

        # Get the selections for the current item selected
        if section_name == CMD_LAYOUT:
            item_strs = [self._build_listbox_str(c) for c in mod.commands]
            return dict((name, item) for name, item in zip(item_strs, mod.commands))
        elif section_name == TLM_LAYOUT:
            item_strs = [self._build_listbox_str(c) for c in mod.telemetry]
            return dict((name, item) for name, item in zip(item_strs, mod.telemetry))
        elif section_name == CAL_LAYOUT:
            item_strs = [self._build_listbox_str(c) for c in mod.calibration]
            return dict((name, item) for name, item in zip(item_strs, mod.calibration))
        else:
            raise ValueError(f'{section_name} is not a valid section')

    def _set_section_selection(self, section_name: str, value: Optional[str]):
        """
        Sets the named `section_name` to the `value`. If `value` is not in list, exception is raised, if value is
        None, UI is disabled for particular section.

        :param section_name: The name of the section to set the selection for.
        :param value: The value to set the section to.
        :raise ValueError: If the `value` is not found in the `section_name`.
        """
        # check for the value is None or in the section list
        if value is not None and value not in self._sections[section_name].selections:
            raise ValueError(f'{value} not in {section_name}')
        # Create items dict to get the current selection
        self._sections[section_name].current_selection = value
        if value:
            self.layouts[section_name][LAYOUT_LIST].SetStringSelection(value, True)
            self._sections[section_name].current_selection_item = self._get_selections(section_name)[value]
            enable_ui = True
        else:
            self.layouts[section_name][LAYOUT_LIST].SetSelection(NOT_FOUND)
            self._sections[section_name].current_selection_item = None
            enable_ui = False
        self.layouts[section_name][LAYOUT_REMOVE].Enable(enable_ui)
        self.layouts[section_name][LAYOUT_EDIT].Enable(enable_ui)

    def _clear_layout_section(self, section_name: str):
        """Clears out one of the named sections in the `self.layouts` attribute."""
        self.layouts[section_name][LAYOUT_LIST].Clear()
        self.layouts[section_name][LAYOUT_REMOVE].Enable(False)
        self.layouts[section_name][LAYOUT_EDIT].Enable(False)
        self._sections[section_name].selections = []
        self._sections[section_name].current_selection = None

    def _clear_module_lists(self):
        """Clears out any of the UI ListBoxes in the main UI window pertaining to current module selected."""
        for layout in [TLM_LAYOUT, CMD_LAYOUT, CAL_LAYOUT]:
            self._clear_layout_section(layout)

            # Set the add button to disabled for each layout section
            self.layouts[layout][LAYOUT_ADD].Enable(False)

    @staticmethod
    def _build_listbox_str(item: DescriptionItem) -> str:
        """
        Builds a listbox string for the item listbox in the UI.

        :param item: The item description to build the UI element from.
        :return: The underlying item string in the list box.
        """
        if isinstance(item, ModuleDescription):
            return f"{item.name}"
        elif isinstance(item, TelemetryDescription):
            return f"{item.index:03} - {item.name}"
        elif isinstance(item, CommandDescription):
            return f"{item.name}"
        elif isinstance(item, CalibrationDescription):
            return f"{item.index:03} - {item.name}"
        else:
            raise ValueError(f"item is not a valid instance of Module/Telemetry/Command/Calibration description")

    def _populate_section(self, section_name: str, items: List[str]):
        """
        Populates the section `section_name` with the items in `items`.

        :param section_name: The section to populate with items
        :param items: The items to populate the section with
        """
        self._sections[section_name].selections = items
        self._sections[section_name].current_selection = None

        self.layouts[section_name][LAYOUT_LIST].Set(items)
        self.layouts[section_name][LAYOUT_ADD].Enable(True)

    def _get_selected_item(self, section_name: str) -> Optional[Tuple[int, DescriptionItem]]:
        """
        Gets the selected item in the listbox for `section_name`. If no item is selected, this returns None.

        :param section_name: The section to get the selected item from.
        :return: The selected item instance or None
        """
        if section_name not in [CMD_LAYOUT, TLM_LAYOUT, CAL_LAYOUT, MOD_LAYOUT]:
            raise ValueError(f"{section_name} is not a valid section")

        idx = self.layouts[section_name][LAYOUT_LIST].GetSelection()

        # Check for the no-selection case
        if self.layouts[section_name][LAYOUT_LIST].GetSelection() == NOT_FOUND:
            return None

        selections = self._get_selections(section_name)
        return idx, selections[self.layouts[section_name][LAYOUT_LIST].GetStringSelection()]

    def _build_module_list(self, mod: ModuleDescription):
        """Builds the listing of the modules in the modules list box in the UI using `mod`"""
        self._clear_module_lists()

        # Build each section
        tlm, cmd, cal = [], [], []
        for t in mod.telemetry:
            tlm.append(self._build_listbox_str(t))
        for c in mod.commands:
            cmd.append(self._build_listbox_str(c))
        for c in mod.calibration:
            cal.append(self._build_listbox_str(c))

        # Populate the listboxes
        self._populate_section(CMD_LAYOUT, cmd)
        self._populate_section(TLM_LAYOUT, tlm)
        self._populate_section(CAL_LAYOUT, cal)

    def _on_location_ctrl_changed(self, e):
        """
        Called when the `Path` is changed in the UI. Updates the `output_location` property

        :param e: wxPython event
        """
        self.output_location = self.output_location_ctrl.Path
        self.output_location_ctrl_box.SetValue(str(self.output_location))
        print(self.output_location)

    def _build_export_btn_callback(self, fn: Callable[[Path, ModuleDb], None]) -> Callable[[wx.Event], Any]:
        """Handler to build an export callback for a button in wx."""
        def callback(e: wx.Event) -> Any:
            return fn(self.output_location, self.db)
        return callback

    def _on_mod_add(self, e):
        """
        Callback for the `Add Module...` button.

        :param e: wx event
        """

    def _on_mod_edit(self, e):
        """
        Callback for the `Edit Module...` button.

        :param e: wx event
        """

    def _on_mod_remove(self, e):
        """
        Callback for the `Remove Module...` button.

        :param e: wx event
        """
        idx, _ = self._get_selected_item(MOD_LAYOUT)
        self.db.pop(idx)
        self.layouts[MOD_LAYOUT][LAYOUT_LIST].Delete(idx)
        self.current_mod = None

    def _on_cmd_add(self, e):
        """
        Callback for the `Add Command...` button.

        :param e: wx event
        """

    def _on_cmd_edit(self, e):
        """
        Callback for the `Edit Command...` button.

        :param e: wx event
        """

    def _on_cmd_remove(self, e):
        """
        Callback for the `Remove Command...` button.

        :param e: wx event
        """
        idx, _ = self._get_selected_item(CMD_LAYOUT)
        self.current_mod.telemetry.pop(idx)
        self.layouts[CMD_LAYOUT][LAYOUT_LIST].Delete(idx)

    def _on_tlm_add(self, e):
        """
        Callback for the `Add Telemetry...` button.

        :param e: wx event
        """

    def _on_tlm_edit(self, e):
        """
        Callback for the `Edit Telemetry...` button.

        :param e: wx event
        """

    def _on_tlm_remove(self, e):
        """
        Callback for the `Remove Telemetry...` button.

        :param e: wx event
        """
        idx, _ = self._get_selected_item(TLM_LAYOUT)
        self.current_mod.telemetry.pop(idx)
        self.layouts[TLM_LAYOUT][LAYOUT_LIST].Delete(idx)

    def _on_cal_add(self, e):
        """
        Callback for the `Add Calibration...` button.

        :param e: wx event
        """

    def _on_cal_edit(self, e):
        """
        Callback for the `Edit Calibration...` button.

        :param e: wx event
        """

    def _on_cal_remove(self, e):
        """
        Callback for the `Remove Calibration...` button.

        :param e: wx event
        """
        # Remove the currently selected item
        idx, _ = self._get_selected_item(CAL_LAYOUT)
        self.current_mod.telemetry.pop(idx)
        self.layouts[CAL_LAYOUT][LAYOUT_LIST].Delete(idx)

    def _on_mod_selected(self, e):
        """
        Callback for when a module is selected in the UI.

        :param e: wx event
        """
        if self.layouts[MOD_LAYOUT][LAYOUT_LIST].GetSelection() == NOT_FOUND:
            self.current_mod = None
        else:
            self.current_mod = self._get_selected_item(MOD_LAYOUT)[1]

    def _on_tlm_selected(self, e):
        """
        Callback for when a telemetry item is selected in the UI.

        :param e: wx event
        """
        if self.layouts[TLM_LAYOUT][LAYOUT_LIST].GetSelection() == NOT_FOUND:
            self._set_section_selection(TLM_LAYOUT, None)
        else:
            self._set_section_selection(TLM_LAYOUT, self.layouts[TLM_LAYOUT][LAYOUT_LIST].GetStringSelection())

    def _on_cmd_selected(self, e):
        """
        Callback for when a command item is selected in the UI.

        :param e: wx event
        """
        if self.layouts[CMD_LAYOUT][LAYOUT_LIST].GetSelection() == NOT_FOUND:
            self._set_section_selection(CMD_LAYOUT, None)
        else:
            self._set_section_selection(CMD_LAYOUT, self.layouts[CMD_LAYOUT][LAYOUT_LIST].GetStringSelection())

    def _on_cal_selected(self, e):
        """
        Callback for when a calibration item is selected in the UI.

        :param e: wx event
        """
        if self.layouts[CAL_LAYOUT][LAYOUT_LIST].GetSelection() == NOT_FOUND:
            self._set_section_selection(CAL_LAYOUT, None)
        else:
            self._set_section_selection(CAL_LAYOUT, self.layouts[CAL_LAYOUT][LAYOUT_LIST].GetStringSelection())

    def bind(self):
        """Binds the UI elements to the relevant callbacks to finish UI"""
        # Bind picker control
        self.Bind(wx.EVT_DIRPICKER_CHANGED, self._on_location_ctrl_changed, self.output_location_ctrl)

        # Bind export buttons
        for exporter, fn in EXPORTERS.items():
            btn = self.exporter_btns[exporter]
            callback = self._build_export_btn_callback(fn)

            self.Bind(wx.EVT_BUTTON, callback, btn)

        # Bind each of the layout sets
        self.Bind(wx.EVT_BUTTON, self._on_mod_add, self.layouts[MOD_LAYOUT][LAYOUT_ADD])
        self.Bind(wx.EVT_BUTTON, self._on_mod_remove, self.layouts[MOD_LAYOUT][LAYOUT_REMOVE])
        self.Bind(wx.EVT_BUTTON, self._on_mod_edit, self.layouts[MOD_LAYOUT][LAYOUT_EDIT])
        self.Bind(wx.EVT_LISTBOX, self._on_mod_selected, self.layouts[MOD_LAYOUT][LAYOUT_LIST])

        self.Bind(wx.EVT_BUTTON, self._on_tlm_add, self.layouts[TLM_LAYOUT][LAYOUT_ADD])
        self.Bind(wx.EVT_BUTTON, self._on_tlm_remove, self.layouts[TLM_LAYOUT][LAYOUT_REMOVE])
        self.Bind(wx.EVT_BUTTON, self._on_tlm_edit, self.layouts[TLM_LAYOUT][LAYOUT_EDIT])
        self.Bind(wx.EVT_LISTBOX, self._on_tlm_selected, self.layouts[TLM_LAYOUT][LAYOUT_LIST])

        self.Bind(wx.EVT_BUTTON, self._on_cmd_add, self.layouts[CMD_LAYOUT][LAYOUT_ADD])
        self.Bind(wx.EVT_BUTTON, self._on_cmd_remove, self.layouts[CMD_LAYOUT][LAYOUT_REMOVE])
        self.Bind(wx.EVT_BUTTON, self._on_cmd_edit, self.layouts[CMD_LAYOUT][LAYOUT_EDIT])
        self.Bind(wx.EVT_LISTBOX, self._on_cmd_selected, self.layouts[CMD_LAYOUT][LAYOUT_LIST])

        self.Bind(wx.EVT_BUTTON, self._on_cal_add, self.layouts[CAL_LAYOUT][LAYOUT_ADD])
        self.Bind(wx.EVT_BUTTON, self._on_cal_remove, self.layouts[CAL_LAYOUT][LAYOUT_REMOVE])
        self.Bind(wx.EVT_BUTTON, self._on_cal_edit, self.layouts[CAL_LAYOUT][LAYOUT_EDIT])
        self.Bind(wx.EVT_LISTBOX, self._on_cal_selected, self.layouts[CAL_LAYOUT][LAYOUT_LIST])

    def build(self):
        """Populates the UI with the module database"""
        mods = [self._build_listbox_str(mod) for mod in self._db]
        self._populate_section(MOD_LAYOUT, mods)
