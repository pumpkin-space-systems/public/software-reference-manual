# coding: utf-8
# ##############################################################################
#  (C) Copyright 2020 Pumpkin, Inc. All Rights Reserved.                       #
#                                                                              #
#  This file may be distributed under the terms of the License                 #
#  Agreement provided with this software.                                      #
#                                                                              #
#  THIS FILE IS PROVIDED AS IS WITH NO WARRANTY OF ANY KIND,                   #
#  INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND                       #
#  FITNESS FOR A PARTICULAR PURPOSE.                                           #
# ##############################################################################
"""
Main UI panel that contains the main documentation window to select
commands/telemetry/calibration values for each of the modules loaded into the
documentation.db file.
"""
from typing import Any

import wx

from scripts.doc.types import TelemetryDescription, CommandDescription
from scripts.doc.ui.main import BG_COLOR, NO_FILL_UI_PRIORITY, DEFAULT_UI_MARGIN, FILL_UI_PRIORITY




class EditTelemetryWindow(wx.Frame):
    """
    The edit telemetry window of the documentation editor program.
    """

    def __init__(self, parent: Any, title: str, tlm: TelemetryDescription, *args, **kwargs):
        """
        Constructs GUI for MainWindow. Use `.show()` to make UI visible.

        :param parent: Parent window (or app)
        :param title: The title of the window
        :param tlm: The telemetry to edit
        :param args: Additional position arguments to pass to super constructor for wx
        :param kwargs: Additional kw args to pass to super constructor for wx
        """
        wx.Frame.__init__(self, parent, title=title, *args, **kwargs)

        self.tlm = tlm


class EditCommandWindow(wx.Frame):
    """
    The edit command window of the documentation editor program.
    """

    def __init__(self, parent: Any, title: str, cmd: CommandDescription, *args, **kwargs):
        """
        Constructs GUI for CommandEditWindow. Use `.show()` to make UI visible.

        :param parent: Parent window (or app)
        :param title: The title of the window
        :param cmd: The command to edit
        :param args: Additional position arguments to pass to super constructor for wx
        :param kwargs: Additional kw args to pass to super constructor for wx
        """
        wx.Frame.__init__(self, parent, title=title, *args, **kwargs)

        self.cmd = cmd

    def layout(self):
        """Lays out the UI for editing commands"""
        main_layout = wx.BoxSizer(wx.VERTICAL)
        self.SetBackgroundColour(BG_COLOR)

        # Command Name UI
        name_layout = wx.BoxSizer(wx.HORIZONTAL)
        name_label = wx.StaticText(label="Name:")
        self.name_box = wx.TextCtrl()
        name_layout.Add(name_label, NO_FILL_UI_PRIORITY, wx.ALIGN_RIGHT | wx.TOP | wx.BOTTOM,
                        DEFAULT_UI_MARGIN)
        name_layout.Add(self.name_box, FILL_UI_PRIORITY, wx.EXPAND, DEFAULT_UI_MARGIN)

        # Listing UI
        listing_layout = wx.BoxSizer(wx.HORIZONTAL)
        listing_label = wx.StaticText(label="Listing Name:")
        self.listing_box = wx.TextCtrl(style=wx.TE_LEFT)
        listing_layout.Add(name_label, NO_FILL_UI_PRIORITY, wx.ALIGN_RIGHT | wx.TOP | wx.BOTTOM,
                        DEFAULT_UI_MARGIN)
        name_layout.Add(self.name_box, FILL_UI_PRIORITY, wx.EXPAND, DEFAULT_UI_MARGIN)

        # Syntax UI
        syntax_layout = wx.BoxSizer(wx.HORIZONTAL)
        syntax_label = wx.StaticText(label="Syntax:")
        self.syntax_box = wx.TextCtrl(style=wx.TE_LEFT)
        syntax_layout.Add()
