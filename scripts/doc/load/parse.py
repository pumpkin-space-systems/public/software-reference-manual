# coding: utf-8
# ##############################################################################
#  (C) Copyright 2020 Pumpkin, Inc. All Rights Reserved.                       #
#                                                                              #
#  This file may be distributed under the terms of the License                 #
#  Agreement provided with this software.                                      #
#                                                                              #
#  THIS FILE IS PROVIDED AS IS WITH NO WARRANTY OF ANY KIND,                   #
#  INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND                       #
#  FITNESS FOR A PARTICULAR PURPOSE.                                           #
# ##############################################################################
"""
Contains the functionality to load in telemetry.db files into a
"""
import csv
import json
from io import StringIO
from typing import List, Tuple
from pathlib import Path
from dataclasses import asdict

from dacite import from_dict, Config
from putdig.common import import_bus_telemetry_definition
from pumpkin_supmcu.supmcu import SupMCUTelemetryDefinition, sizeof_supmcu_type, typeof_supmcu_fmt_char

from scripts.doc.types import TelemetryDescription, TelemetryType, MultiItemPart, MultiTelemetryItemDescription, \
    CommandDescription, \
    CalibrationDescription, ModuleDescription, Position, ModuleDb

HEX_TYPES = ['x', 'X', 'z', 'Z']
FMT_CHR_TO_TYPE_NAME = {
    'S': 'string',
    'c': 'char',
    'u': 'uint8_t',
    't': 'int8_t',
    's': 'uint16_t',
    'n': 'int16_t',
    'i': 'uint32_t',
    'd': 'int32_t',
    'l': 'uint64_t',
    'k': 'int64_t',
    'f': 'float',
    'F': 'double',
    'x': '8-bit hex (uint8_t)',
    'X': '8-bit hex (uint8_t)',
    'z': '16-bit hex (uint16_t)',
    'Z': '16-bit hex (uint16_t)',
}


def format_str_to_names(s: str) -> List[Tuple[str, str]]:
    """
    Converts the format string into a list of tuples containing a valid format identifier and
    the underlying c-type.

    :param s: The format string
    :return: The list of tuples of valid format character, underlying c-type
    """
    return [(c, FMT_CHR_TO_TYPE_NAME[c]) for c in s if c in FMT_CHR_TO_TYPE_NAME]


def import_telemetry_definition(t: SupMCUTelemetryDefinition) -> TelemetryDescription:
    """
    Import the SupMCUTelemetryDefinition, exporting out a TelemetryDescription with description fields empty.

    :param t: Telemetry definition from the pumpkin SupMCU Library
    :return: The representative telemetry definitions.
    """
    fmt_parts = format_str_to_names(t.format)
    if not fmt_parts:
        raise ValueError(f'{t.format} is an invalid format string')

    # Check parts for more than one format str part (multi), followed by hex (bitfield), then single
    if len(fmt_parts) > 1:
        tlm_type = TelemetryType.Multiple
        parts = []
        cur_byte = 0
        for c, fmt in fmt_parts:
            # get length, determine position in multi-field telemetry
            start = cur_byte
            end = cur_byte + sizeof_supmcu_type(typeof_supmcu_fmt_char(c)) - 1
            cur_byte = end + 1
            parts.append(MultiItemPart('', fmt, (start, end)))
        detail = MultiTelemetryItemDescription(parts)
    elif fmt_parts[0][0] in HEX_TYPES:
        tlm_type = TelemetryType.Bitfield
        c, fmt = fmt_parts[0]
        size = sizeof_supmcu_type(typeof_supmcu_fmt_char(c))
        # See FIXME in types
        # detail = BitfieldTelemetryDescription(fmt, [BitfieldItemPart('', (0, size - 1))])
        detail = MultiTelemetryItemDescription([MultiItemPart('', fmt, (0, size - 1))])
    else:
        tlm_type = TelemetryType.Single
        c, fmt = fmt_parts[0]
        # See FIXME in types
        # detail = SingleTelemetryItemDescription(fmt)
        detail = MultiTelemetryItemDescription([MultiItemPart('', fmt, (0, t.telemetry_length - 1))])

    # Now Create the telemetry item
    return TelemetryDescription(t.name, t.name, '', '', t.simulatable != 0, t.telemetry_length, tlm_type, detail)
    #return TelemetryDescription(t.idx, t.name, t.name, '', '', t.simulatable != 0, t.telemetry_length, tlm_type, detail)


def import_putdig_telemetry_definition(p: Path) -> List[ModuleDescription]:
    """
    Imports the `pumqry -e ...` dump from a Pumpkin FlatSat/SUPERNOVA/RS3 stack.
    This creates the module description with only the telemetry fields filled in.

    :param p: The path to the `pumqry -e` dump file.
    :return: The list of the ModuleDefinitions
    """
    module_defs = import_bus_telemetry_definition(p)

    # This means we we're given an empty dump ...
    if not module_defs:
        return []

    descriptions = []

    for d in module_defs:
        supmcu_def = d.definition
        module_desc = ModuleDescription(supmcu_def.name, f'{supmcu_def.name} (0x{supmcu_def.address:02X})', '', '',
                                        d.version, [], [], [])

        # Sort telemetry definitions by index
        tlm_defs = sorted(supmcu_def.module_telemetry.values(), key=lambda tlm: tlm.idx)
        for tlm in tlm_defs:
            module_desc.telemetry.append(import_telemetry_definition(tlm))

        descriptions.append(module_desc)

    # Import the SupMCU telemetry as a ModuleDescription as well, but fix the version/name
    supmcu_tlm = module_defs[0].definition.supmcu_telemetry
    supmcu_desc = ModuleDescription('SupMCU', 'Supervisor MCU', '', '', '', [], [], [])
    supmcu_defs = sorted(supmcu_tlm.values(), key=lambda tlm: tlm.idx)
    for tlm in supmcu_defs:
        supmcu_desc.telemetry.append(import_telemetry_definition(tlm))
    descriptions.append(supmcu_desc)

    return descriptions


def import_command_listing(command_listing: str) -> CommandDescription:
    """
    Import command listing in the format `<name> <syntax>`

    :param command_listing: The command listing
    :return: An empty command description with name and syntax filled out
    """
    parts = command_listing.split()
    name, syntax = parts[0], " ".join(parts[1:])
    return CommandDescription(name, name, syntax, "", "", "", "")


def import_calibration_listing(csv_string: str) -> CalibrationDescription:
    """
    Imports a row assuming index,name,description.

    :param csv_string: The single csv row
    :return: A calibration description
    """
    with StringIO(csv_string, newline='') as f:
        r = csv.reader(f)
        row = next(r)
        return CalibrationDescription(row[1], int(row[0]), row[2])


def load_module_db(path: Path) -> ModuleDb:
    """
    Loads the module definition file. This is simply a json file with multiple ModuleDescriptions
    encoded as entries into a JSON array.

    :param path: The file path to the module database
    :return: The list of the ModuleDescriptions
    """
    with path.open('r') as f:
        desc = json.load(f)

        return [from_dict(data_class=ModuleDescription,
                          data=d,
                          config=Config(type_hooks={
                              TelemetryType: TelemetryType,
                              Position: lambda l: (l[0], l[1])  # Fixes TypeMatchError for Position element
                          })) for d in desc]


def save_modules_db(db: ModuleDb, path: Path, format_output: bool = False):
    """
    Saves the module database `db` to `path` on the filesystem.

    :param db: The database to save to disk.
    :param path: The path to save file file to.
    :param format_output: Whether to format the JSON output or not.
    """
    with path.open('w') as f:
        json.dump([asdict(d) for d in db], f, indent=4 if format_output else None)
